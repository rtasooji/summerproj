"""
Created  by rtasooji on 8/1/2018
"""

import sys
import clr

sys.path.append('C:\\Program Files (x86)\\PIPC\\AF\\PublicAssemblies\\4.0\\')
clr.AddReference('OSIsoft.AFSDK')

from OSIsoft.AF import *
from OSIsoft.AF.Data import *
from OSIsoft.AF.Time import *
from OSIsoft.AF.Asset import *
from System import *


class PiToPy(object):

    def __init__(self):
        self.name = ""

    def get_parent_element(self, server, database, element):
        server = self._get_server(server)
        database = self._get_database(server, database)
        return self._get_element(database, element)

    def _get_server(self, server_name: str):
        """
        Get the server from PI System
        :param server_name: name of the server
        :return: server
        """
        pi_system = PISystems()
        asset_server = pi_system[server_name]
        return asset_server

    def _get_database(self, server, database_name):
        """
        get the database from server
        :param server:
        :param database_name:
        :return: return database
        """
        return server.Databases.get_Item(database_name)

    def _get_element(self, database, element_name):
        """

        :param database:
        :param element_name:
        :return:
        """
        return database.Elements.get_Item(element_name)

    def set_time(self, year, month, day, hour, minute, seconds):
        result = DateTime(year, month, day, hour, minute, seconds).ToUniversalTime()
        return AFTime(result)

    def time_span(self, hour, minute, seconds):
        time_span = TimeSpan(hour, minute, seconds)
        return AFTimeSpan(time_span)

    def time_range(self, start, end):
        return AFTimeRange(start, end)

    def send_data(self):
        # TODO: implement send and update
        pass
