"""
Created  by rtasooji on 8/3/2018
"""
from OSISummer.pi.connector import PiToPy

import pandas as pd
from fbprophet import Prophet
import PySide2.QtCore as QtCore


class DataSet(object):

    def __init__(self):
        self.pi = PiToPy()

    def get_element(self, server, database, element):
        print("Start retrieving data")

        return self.pi.get_parent_element(server, database, element)

    def get_data(self, top_parent, start, end, time_span=3) -> []:
        y = list()
        time = list()
        time_range = self.pi.time_range(start, end)
        interval = self.pi.time_span(0, 0, time_span)
        for attr in top_parent.Attributes:
            if attr.Name == "Value":
                values = attr.Data.InterpolatedValues(time_range,
                                                      interval,
                                                      None,
                                                      None,
                                                      True)
                self._insert_values(values, y, time)
        return y, time

    def _insert_values(self, values, y: [], time: []):
        for value in values:
            if value.IsGood:
                output = value.Value
                t = value.Timestamp.ToString()

                y.append(output)
                time.append(t)


class Forecast(object):

    class Signal(QtCore.QObject):
        signal = QtCore.Signal(list)

    def __init__(self):
        self.result = self.Signal()

    def prophet(self, y, time):
        p = Prophet()
        p.fit(self.make_input_dataframe(y, time))
        return p

    def future(self, prophet: Prophet, period=10, freq='T'):
        """
        :param prophet: fitted prophet object
        :param period: the peirod to forecast in future
        :param freq: the frequency check
        https://pandas.pydata.org/pandas-docs/stable/timeseries.html#timeseries-offset-aliases
        :return: future dataframe for prediction
        """
        return prophet.make_future_dataframe(periods=period, freq=freq)

    def forecast(self, prophet: Prophet, future):
        """
        :param prophet: fitted prophet object
        :param future:  the generated future dataframe
        :return: the forecast result dataframe
        """
        forecast = prophet.predict(future)
        result = [prophet, forecast]
        print("Value for yhat: " + str(type(forecast['yhat'])))
        print("Value for ds: "+ str(type(forecast['ds'])))
        ForecastHandling.insert_values(forecast['yhat'], forecast['ds'])
        ForecastHandling.report_value()
        self.result.signal.emit(result)
        print("predicted inside Forecast class")


    def make_input_dataframe(self, y, ds):
        """
        CHECK: Might cause problem the input time needs to work with their format
        can change the input to pandas date format, if needed
        :param y:
        :param ds: working format 'YYYY-M-Day H:MIN:SEC'
        :return:
        """
        result = {"ds": ds,
                  "y": y}
        df = pd.DataFrame(result, index=range(0, len(y)))
        return df


import smtplib
import json
from email.mime.text import MIMEText

class ForecastHandling(object):

    reported_threshold = 54
    forecast_result = []
    forecast_timestamp = []
    send_smtp = False
    @staticmethod
    def insert_values(inputs: [], timestamp: []):
        ForecastHandling.forecast_result = inputs
        ForecastHandling.forecast_timestamp = timestamp


    @staticmethod
    def report_value():
        result = {'value': "",
                  'time': ""}
        for i in range(len(ForecastHandling.forecast_result)):
            if ForecastHandling.forecast_result[i] < ForecastHandling.reported_threshold:
                result['value'] = ForecastHandling.forecast_result[i]
                result['time'] = ForecastHandling.forecast_timestamp[i].isoformat()
                ForecastHandling.reported_threshold -= 1
                ForecastHandling._setup_smtp(result, 'rtasooji@vt.edu')
                break

    @staticmethod
    def _setup_smtp(result, receiver):
        gmail_user = 'fhpinotification@gmail.com'
        gmail_password = 'HokieDVE'
        output = MIMEText(json.dumps(result))
        output['Subject'] = "Battery voltage below {}".format(ForecastHandling.reported_threshold)
        print(output)
        print(type(output))
        try:
            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            server.login(gmail_user, gmail_password)
            result = server.sendmail(from_addr=gmail_user, to_addrs=receiver, msg=output.as_string())
            print(result)
            server.quit()
        except Exception as e:
            print('Something went wrong in smtp...', e)




### for testing
# if __name__ == '__main__':
#     test_set = DataSet()
#     server = "osisoft"
#     database = "futurehaus"
#     element = "UFL\\UFL\\Vladimir\\device01"
#     start = "-8H"
#     end = "8/22/18 12:00:00 pm"
#     element = test_set.get_element(server, database, element)
#     y, time = test_set.get_data(element, start, end)
#
#     p = Forecast()
#     prophet = p.prophet(y, time)
#     future = p.future(prophet, period=1, freq='H')
#     predict = p.forecast(prophet, future)

# For testing forecast handling
# get the output example from the forecast as source and pass it to this
# change the value in some rows to 54
# on next interation change value to 54 and 53
# on next iteration change value to 52
# should send only one notification
# at 4 pm set the reported threshold value to 54


# import time
# if __name__ == '__main__':
#     y = list(range(55, 65))
#     timestamp = list(range(0, 10))
#     ForecastHandling.insert_values(y.copy(), timestamp.copy())
#     print(ForecastHandling.forecast_result)
#     print(ForecastHandling.forecast_timestamp)
#     counter = 0
#     while True:
#         if counter == 1:
#             y[0] -= 2
#             ForecastHandling.insert_values(y.copy(), timestamp.copy())
#             print(ForecastHandling.forecast_result)
#             print(ForecastHandling.forecast_timestamp)
#         time.sleep(1)
#         ForecastHandling.report_value()
#         print("threshold: " + str(ForecastHandling.reported_threshold))
#         counter += 1

