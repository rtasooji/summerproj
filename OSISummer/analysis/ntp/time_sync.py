"""
Created  by rtasooji on 6/18/2018
"""
import ntplib
from time import ctime, time, sleep, mktime
import datetime
import ctypes
from ctypes import util
import sys


class TimeSyncer(object):

    def __init__(self, server_name: str="time.windows.com"):
        """
        gets the ntp server \n
        example that can provide in a list to test:\n
        ['ntp.iitb.ac.in', 'time.nist.gov', 'time.windows.com', 'pool.ntp.org']\n
        :param server_name: server name default 'time.windows.com'
        """
        self.ntpClient = ntplib.NTPClient()
        self.response = self.ntpClient.request(host=server_name, version=3)
        self.time = datetime.datetime.utcfromtimestamp(self.response.orig_time)


    def set_sys_time_win(self):
        """
        Set the system time to the server time
        :return:
        """
        import win32api
        win32api.SetSystemTime(self.time.year, self.time.month,
                               self.time.weekday(), self.time.day,
                               self.time.hour, self.time.minute,
                               self.time.second,
                               self.time.microsecond//1000
                               )

    def get_sys_time_win(self):\
        return win32api.GetSystemTime()

    def get_time_info(self):
        """
        print
        :return:
        """
        print("Original time: %f" % self.response.orig_time)
        print("Original time stamp: %f" % self.response.orig_timestamp)
        print("Receive time: %f" % self.response.recv_time)
        print("Offset time: %f" % self.response.offset)
        print("delay time: %f" % self.response.delay)


    def set_sys_time_unix(self):
        time_tuple = (self.time.year,
                      self.time.month,
                      self.time.day,
                      self.time.hour,
                      self.time.minute,
                      self.time.second,
                      self.time.microsecond*1000)  # micro to nanoseconds
        CLOCK_REALTIME = 0
        class timespec(ctypes.Structure):
            _fields_ = [("tv_sec", ctypes.c_long),
                        ("tv_nsec", ctypes.c_long)]

        librt =  ctypes.CDLL(ctypes.util.find_library("rt"))

        ts = timespec()
        ts.tv_sec = int(mktime(
                        datetime.datetime(*time_tuple[:6]).timetuple()))
        ts.tv_nsec = time_tuple[6]

        librt.clock_settime(CLOCK_REALTIME, ctypes.byref(ts))


