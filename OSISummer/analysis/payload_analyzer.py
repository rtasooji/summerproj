"""
Created  by rtasooji on 6/27/2018
"""
import matplotlib.pyplot as plt


values = [1, 1001, 2001, 3001, 4001]


def make_files(values):
    file_list = []
    for i in values:
        file_loc = "C:\\Users\\rtasooji\\Desktop\\analysis files\\size_time\\"\
                    + str(i) + "\\payload_analyze.csv"
        file_list.append(file_loc)
    return file_list


def combine_files(files):
    all_lines = []
    for file in files:
        with open(file) as f:
            lines = f.readlines()
            for line in lines:
                all_lines.append(line)
    return all_lines


def parse_lines(lines):
    value_holder = dict()
    for line in lines:
        segment = line.split(sep=" ")
        curr_key = ""
        for i in range(len(segment)):
            while i < 6:
                if i % 2 == 0:
                    key, _ = segment[i].split(sep=":", maxsplit=1)
                    if key not in value_holder.keys():
                        value_holder[key] = list()
                        curr_key = key
                        break
                    else:
                        curr_key = key
                        break
                else:
                    value_holder[curr_key].append(float(segment[i]))
                    break
            if i == 6:
                key = segment[i] + segment[i+1].rsplit(":")[0]
                if key not in value_holder.keys():
                    value_holder[key] = list()
                    value_holder[key].append(
                            float(segment[i+2].rsplit()[0]))
                else:
                    value_holder[key].append(
                            float(segment[i+2].rsplit()[0]))
    return value_holder


def plot_values(values):
    my_fig = plt.figure()
    plt.title("Local network time interval")
    ax = my_fig.add_subplot(1, 1, 1)
    ax.set_ylabel("Number of messages in bulk, size for each message~104 byte",
                  size=8)
    ax.set_xlabel("time interval (seconds) before making the body for request "
                  "and after sending the reques to PI",
                  size=8)
    ax.scatter(values['timeintervals'], values['queueSizeN'], s=0.5)
    return ax


if __name__ == "__main__":
    file_list = make_files(values)
    all_lines = combine_files(file_list)
    values = parse_lines(all_lines)
    plot_values(values)
