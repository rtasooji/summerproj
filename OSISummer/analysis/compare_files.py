# Created by dark at 6/11/2018, 9:53 AM
# %% moduels
from analysis.analyzer import AnalyzeFile
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# %%
"""
['bmh', 'classic', 'dark_background', 'fast', 'fivethirtyeight', 'ggplot',
 'grayscale', 'seaborn-bright', 'seaborn-colorblind', 'seaborn-dark-palette',
 'seaborn-dark', 'seaborn-darkgrid', 'seaborn-deep', 'seaborn-muted',
 'seaborn-notebook', 'seaborn-paper', 'seaborn-pastel', 'seaborn-poster',
 'seaborn-talk', 'seaborn-ticks', 'seaborn-white', 'seaborn-whitegrid',
 'seaborn', 'Solarize_Light2', '_classic_test']
"""
plt.style.use('Solarize_Light2')

# %%
#file_dir = os.path.dirname(res.__file__) + "/longtest1/"
file_dir = "C:\\Users\\rtasooji\\Desktop\\analysis files\\longtest2\\"
publisher = AnalyzeFile(file_dir + "publisher_2.csv")
receiver = AnalyzeFile(file_dir + "receiver_2.csv")

publisher_time = publisher.unixToTime(publisher.unixTimes)
receiver_time = receiver.unixToTime(receiver.unixTimes)


value_pub_data = publisher.messageLogs
value_np = np.zeros(shape=(len(value_pub_data)))
for i in range(len(value_pub_data)):
    print(i)
    _, value = value_pub_data.iloc[i].split('_', 1)
    value_np[i] = value
value_pub = pd.DataFrame(data=value_np, dtype=np.int64)

value_rec_data = receiver.messageLogs
value_np = np.zeros(len(value_rec_data))
for i in range(len(value_rec_data)):
    _, value = value_rec_data.iloc[i].split('_', 1)
    value_np[i] = value
value_rec = pd.DataFrame(data=value_np, dtype=np.int64)



row_p, _ = publisher.get_shape()
row_r, _ = receiver.get_shape()


def print_info(mean, std, publish_time, receive_time):
    print("mean: " + str(mean))
    print("std: " + str(std))
    print("publisher time interval: %s sec " % (publish_time))
    print("receiver time interval: %s sec" % (receive_time))
    print("")
    pass


if row_p == row_r:
    value_time = zip(receiver.unixTimes, publisher.unixTimes)
    value_log = zip(receiver.messageLogs, publisher.messageLogs)

    latency = [x - y for x, y in zip(receiver.unixTimes, publisher.unixTimes)]
    print(latency)
    mean, std = publisher.calMean_std(latency)
    publisher_total_time = round((publisher.unixTimes.max() - publisher.unixTimes.min()), 5)
    receiver_total_time = round((receiver.unixTimes.max() - receiver.unixTimes.min()), 5)
    print_info(mean, std, publisher_total_time, receiver_total_time)
    plt.close()
    fig = plt.figure()

    col = 2
    row = 1
    ax = fig.add_subplot(row, col, 1)
    ax.title.set_text("No Dropped " +
                      "publishing time: " + str(publisher_total_time) +
                      "receiving time: " + str(receiver_total_time))
    ax.plot(value_pub.iloc[:, 0], latency, label="interval")
    ax.set_ylabel("time interval_second")
    ax.set_xlabel("payload Value")
    ax.legend()
    ax = fig.add_subplot(row, col, 2)
    ax.scatter(publisher.unixTimes, value_pub.iloc[:, 0],
               label="publisher", s=0.5, color="blue")
    ax.scatter(receiver.unixTimes, value_rec.iloc[:, 0],
               label="receiver", s=0.5, color="red")
    ax.set_xlabel("unix time")
    ax.set_ylabel("payload value", rotation='horizontal', size=0.1)
    ax.legend()
else:
    # need to work on it. Need focus
    # There are drops on the other file, file the one that there is no drops
    #remove them from the bigger one
    re_shaped = list()
    time_interval=list()
    indices = value_pub.isin(value_rec)
    missingIndex = list()
    for i in range(len(indices)):
        if indices.iloc[i, 0]:
            continue
        else:
            missingIndex.append(i)

    # bolean mask
    value_pub_copy = value_pub[~value_pub.index.isin(missingIndex)]
    pub_time = publisher.unixTimes.copy()
    pub_time = pub_time[~pub_time.index.isin(missingIndex)]
    latency = [x - y for x, y in zip(receiver.unixTimes, pub_time)]
    mean, std = publisher.calMean_std(latency)
    publisher_total_time = round((publisher.unixTimes.max() - publisher.unixTimes.min()), 3)
    receiver_total_time = round((receiver.unixTimes.max() - receiver.unixTimes.min()), 3)
    print_info(mean, std, publisher_total_time, receiver_total_time)
    plt.close()
    fig = plt.figure()

    col = 2
    row = 1
    ax = fig.add_subplot(row, col, 1)
    ax.title.set_fontsize(10)
    ax.title.set_text("Dropped " + str(len(missingIndex)) +
                      "publishing time: " + str(publisher_total_time) +
                      "receiving time: " + str(receiver_total_time))
    ax.plot(pub_time, latency, label="interval")
    ax.set_ylabel("time interval_second")
    ax.set_xlabel("payload Value")
    ax.legend()
    ax = fig.add_subplot(row, col, 2)
    ax.scatter(publisher.unixTimes, value_pub,
               label="publisher", s=0.5, color="blue")
    ax.scatter(receiver.unixTimes, value_rec,
               label="receiver", s=0.5, color="red")
    ax.set_xlabel("unix time")
    ax.set_ylabel("payload value")
    ax.legend()

plt.show()
