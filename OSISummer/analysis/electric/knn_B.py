"""
Created  by rtasooji on 7/25/2018
Using acsf2 database to predict the input device using knn algoritm.
This script only works on the local machine that has PI System installed on it.
"""

import pandas as pd
import numpy as np
import timeit
import itertools
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

import clr

sys.path.append('C:\\Program Files (x86)\\PIPC\\AF\\PublicAssemblies\\4.0\\')
clr.AddReference('OSIsoft.AFSDK')

from OSIsoft.AF import *
from OSIsoft.AF.Data import *
from OSIsoft.AF.Time import *
from OSIsoft.AF.Asset import *
from System import *


def get_ascf2() -> list():

    device_list = list()

    pi_system = PISystems()
    asset_server = pi_system["RTASOOJI7480"]

    test_db = asset_server.Databases.get_Item("test")

    top_parent = test_db.Elements.get_Item("UCSF2\\UFL")
    # CHECK: time interval has huge impact on performance
    s_date = DateTime(2018, 7, 24, 13, 11, 00).ToUniversalTime()
    e_date = DateTime(2018, 7, 24, 13, 20, 00).ToUniversalTime()

    s_time = AFTime(s_date)
    e_time = AFTime(e_date)
    time_range = AFTimeRange(s_time, e_time)

    # CHECK: The recorded database is at 10 seconds frequency
    time_span = TimeSpan(0, 0, 10)
    interval = AFTimeSpan(time_span)  # get values every 10 seconds

    for grp_device in top_parent.Elements:
        for device in grp_device.Elements:
            device_spec = {"grp": "", "name": "", "timeStamp": list(),
                           "ph": list(), "power": list(), "reactPower": list(),
                           "rmsCur": list(), "rmsVolt": list(), "freq": list()}

            device_spec["grp"] = grp_device.Name
            device_spec["name"] = device.Name
            for attr in device.Attributes:
                values = attr.Data.InterpolatedValues(time_range,
                                                      interval,
                                                      None,
                                                      None,
                                                      True)
                if attr.Name == "Power":
                    insert_values(values, device_spec, "power")
                if attr.Name == "RecTime":
                    insert_values(values, device_spec, "timeStamp")
                if attr.Name == "PhAngle":
                    insert_values(values, device_spec, "ph")
                if attr.Name == "ReacPower":
                    insert_values(values, device_spec, "reactPower")
                if attr.Name == "rmsCur":
                    insert_values(values, device_spec, "rmsCur")
                if attr.Name == "rmsVolt":
                    insert_values(values, device_spec, "rmsVolt")
                if attr.Name == "Freq":
                    insert_values(values, device_spec, "freq")

            device_list.append(device_spec.copy())
            device_spec.clear()
            # add copy of dic to list clear dic
    return device_list


def insert_values(values, input_dic, key):
    for value in values:

        result = value.Value
        if not value.IsGood:
            result = 0  # CHECK: better to use 'NaN' and then clean the data, will add processing time
        input_dic[key].append(result)


def check_array_size(db):
    """
    Call this if needed for testing purposes
    :param db: saved pickle list
    :return: None
    """

    total_power = 0
    total_ph = 0
    total_reactPower = 0
    total_rmsCur = 0
    total_rmsVolt = 0
    total_freq = 0
    total_timeStamp = 0

    for device in db:

        power = device["power"]
        ph = device["ph"]
        reac_power = device["reactPower"]
        rms_cur = device["rmsCur"]
        rms_volt = device["rmsVolt"]
        freq = device["freq"]
        time = device["timeStamp"]

        for value in power:
            total_power += 1
        for value in ph:
            total_ph += 1
        for value in reac_power:
            total_reactPower += 1
        for value in rms_cur:
            total_rmsCur += 1
        for value in rms_volt:
            total_rmsVolt += 1
        for value in freq:
            total_freq += 1
        for value in time:
            total_timeStamp += 1
    print("device grp: %s " %  device["grp"])
    print("device name: %s" % device["name"])
    print("total power points: %s" % total_power)
    print("total ph points: %s" % total_ph)
    print("total reactPowre points: %s" % total_reactPower)
    print("total rmsCur points: %s" % total_rmsCur)
    print("total rmsVolt points: %s" % total_rmsVolt)
    print("total freq points: %s" % total_freq)
    print("total timeStamp points: %s" % total_timeStamp)
    print("total points: {}".format(total_power + total_ph + total_reactPower +
                                    total_rmsCur + total_rmsVolt + total_freq +
                                    total_timeStamp))


def make_data_frame(db) -> pd.DataFrame():
    """
    Create a dataframe for scikit usign ndarray
    :param db: the database retrieved from pi stystem
    :return: pandas daraframe
    """
    total_power = np.ndarray(shape=(0, 0), dtype=np.float32)
    total_ph = np.ndarray(shape=(0, 0), dtype=np.float32)
    total_reactPower = np.ndarray(shape=(0, 0), dtype=np.float32)
    total_rmsCur = np.ndarray(shape=(0, 0), dtype=np.float32)
    total_rmsVolt = np.ndarray(shape=(0, 0), dtype=np.float32)
    total_freq = np.ndarray(shape=(0, 0), dtype=np.float32)

    total_timeStamp = np.ndarray(shape=(0, 0), dtype=str)

    total_grp = list()
    total_name = list()

    labels = ["grp", "name", "power", "ph", "reacPower", "rmsCur", "rmsVolt",
              "freq", "Timestamp"]
    df = pd.DataFrame(columns=labels)

    counter = 0
    for device in db:

        power = device["power"]
        total_power = np.append(total_power, power)

        ph = device["ph"]
        total_ph = np.append(total_ph, ph)

        reac_power = device["reactPower"]
        total_reactPower = np.append(total_reactPower, reac_power)

        rms_cur = device["rmsCur"]
        total_rmsCur = np.append(total_rmsCur, rms_cur)

        rms_volt = device["rmsVolt"]
        total_rmsVolt = np.append(total_rmsVolt, rms_volt)

        freq = device["freq"]
        total_freq = np.append(total_freq, freq)

        time = device["timeStamp"]
        total_timeStamp = np.append(total_timeStamp, time)

        for i in range(len(power)):
            total_grp.append(device['grp'])
            total_name.append(device['name'])



    df['power'] = total_power
    df['ph'] = total_ph
    df['reacPower'] = total_reactPower
    df['rmsCur'] = total_rmsCur
    df['rmsVolt'] = total_rmsVolt
    df['freq'] = total_freq

    df['Timestamp'] = total_timeStamp
    df['grp'] = total_grp
    df['name'] = total_name
    return df


def get_cmap(length, name='hsv'):
    """
    Generate colors to plot multi trends if needed
    :param length:
    :param name:
    :return:
    """
    return plt.cm.get_cmap(name, length)


def plot_values(column_name):
    """
    Plot raw data values from the database if needed
    :param column_name:
    :return:
    """
    if column_name == 'grp':
        column = 0
    elif column_name == 'name':
        column = 1

    fig, ax = plt.subplots()

    max_name = pd.DataFrame.max(features.iloc[:, column])
    min_name = pd.DataFrame.min(features.iloc[:, column])

    cmap = get_cmap(max_name - min_name)

    for label, grp in features.groupby(column_name):
        if column_name == 'grp':
            device_grp = le_grp.inverse_transform(label)
        else:
            device_grp = label
        ax.scatter(grp['rmsCur'], grp['rmsVolt'], label=device_grp,
                   c=cmap(label - min_name), s=.5)

        # grp.plot('power', 'reacPower', ax=ax, label=label) # nice

    if column_name == 'grp':
        plt.legend()
    plt.show()


def plot_confusion_matrix(cm, title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    To visualize the confusion matrix from sk-learn
    :param cm:
    :param title:
    :param cmap:
    :return:
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], 'd'),
                 horizontalalignment='center',
                 color='white' if cm[i, j > thresh] else 'black')

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('predicted label')


if __name__ == '__main__':
    # get the data from PI System
    x1 = timeit.default_timer()
    db = get_ascf2()
    x2 = timeit.default_timer()
    print(x2 - x1)
    """device_spec = {"grp": "", "name": "", "timeStamp": list(),
                    "ph": list(), "power": list(), "reactPower": list(),
                    "rmsCur": list(), "rmsVolt": list(), "freq": list()}
    """
    # check_array_size(db)  # total point per column: 324450, total point: 2271150
    # make the dataframe for analytics
    x1 = timeit.default_timer()
    features = make_data_frame(db)
    x2 = timeit.default_timer()
    print(x2 - x1)  # 6.3 seconds

    x1 = timeit.default_timer()
    le_grp = LabelEncoder()  # label categorical columns
    le_name = LabelEncoder()
    features.iloc[:, 0] = le_grp.fit_transform(features.iloc[:, 0])
    features.iloc[:, 1] = le_name.fit_transform(features.iloc[:, 1])

    # plot_values('name')  # plot categorical data if needed

    X_train, X_test, y_train, y_test = train_test_split(features.iloc[:, 2:8],
                                                        features.iloc[:, 0],
                                                        test_size=.4)

    neigh = KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree', leaf_size=50,
                                 n_jobs=-1)
    neigh.fit(X_train, y_train)

    y_pred = neigh.predict(X_test)
    x2 = timeit.default_timer()
    print(x2 - x1)

    parameters = neigh.get_params()

    cm = confusion_matrix(y_test, y_pred)

    plot_confusion_matrix(cm)
    plt.show()










