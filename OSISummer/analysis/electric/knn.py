"""
Created  by rtasooji on 7/25/2018
Using acsf2 database to predict the input device using knn algoritm.
This script only works on the local machine that has PI System installed on it.
"""

# get the data from PI System
# server: RTASOOJI7480
# database: test
# element: UCSF2\\UFL
"""device_spec = {"grp": "", "name": "", "timeStamp": list(),
                "ph": list(), "power": list(), "reactPower": list(),
                "rmsCur": list(), "rmsVolt": list(), "freq": list()}
"""
# check_array_size(db)  # total point per column: 324450, total point: 2271150

# analytic package
import pandas as pd
import numpy as np
import datetime
import timeit
import itertools
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score

# project package
from OSISummer.pi.connector import PiToPy


# TODO setting the start and end time should be called from context file
# The I/O access might increase the running time. which might not needed at this case
class DataSet(object):

    ascf_start = datetime.datetime(2018, 7, 24, 13, 11, 00)
    ascf_end = datetime.datetime(2018, 7, 24, 14, 15, 00)

    def __init__(self):

        self.pi = PiToPy()
        # CHECK: change based on the time
        self.ascf_interval = self.ascf_end - self.ascf_start

    def get_ascf2(self, server, database, element, start, end, time_span) -> []:
        print("start retrieving data")
        x1 = timeit.default_timer()
        device_list = list()
        top_parent = self.pi.get_parent_element(server, database, element)

        time_range = self.pi.time_range(start, end)
        for grp_device in top_parent.Elements:
            for device in grp_device.Elements:
                device_spec = {"grp": "", "name": "", "timeStamp": list(),
                               "ph": list(), "power": list(), "reactPower": list(),
                               "rmsCur": list(), "rmsVolt": list(), "freq": list()}

                device_spec["grp"] = grp_device.Name
                device_spec["name"] = device.Name
                for attr in device.Attributes:
                    values = attr.Data.InterpolatedValues(time_range,
                                                          time_span,
                                                          None,
                                                          None,
                                                          True)
                    if attr.Name == "Power":
                        self._insert_values(values, device_spec, "power")
                    if attr.Name == "RecTime":
                        self._insert_values(values, device_spec, "timeStamp")
                    if attr.Name == "PhAngle":
                        self._insert_values(values, device_spec, "ph")
                    if attr.Name == "ReacPower":
                        self._insert_values(values, device_spec, "reactPower")
                    if attr.Name == "rmsCur":
                        self._insert_values(values, device_spec, "rmsCur")
                    if attr.Name == "rmsVolt":
                        self._insert_values(values, device_spec, "rmsVolt")
                    if attr.Name == "Freq":
                        self._insert_values(values, device_spec, "freq")

                device_list.append(device_spec.copy())
                device_spec.clear()
                # add copy of dic to list clear dic
        x2 = timeit.default_timer()
        print("time retrieved from PI System in {} seconds".format(x2 - x1))
        return device_list

    @staticmethod
    def _insert_values(values, input_dic, key):
        for value in values:

            result = value.Value
            if not value.IsGood:
                result = 0  # CHECK: better to use 'NaN' and then clean the data, will add processing time
            input_dic[key].append(result)

    @staticmethod
    def check_array_size(db):
        """
        Call this if needed for testing purposes
        :param db: saved pickle list
        :return: None
        """

        total_power = 0
        total_ph = 0
        total_reactPower = 0
        total_rmsCur = 0
        total_rmsVolt = 0
        total_freq = 0
        total_timeStamp = 0

        for device in db:

            power = device["power"]
            ph = device["ph"]
            reac_power = device["reactPower"]
            rms_cur = device["rmsCur"]
            rms_volt = device["rmsVolt"]
            freq = device["freq"]
            time = device["timeStamp"]

            for value in power:
                total_power += 1
            for value in ph:
                total_ph += 1
            for value in reac_power:
                total_reactPower += 1
            for value in rms_cur:
                total_rmsCur += 1
            for value in rms_volt:
                total_rmsVolt += 1
            for value in freq:
                total_freq += 1
            for value in time:
                total_timeStamp += 1
        print("device grp: %s " %  device["grp"])
        print("device name: %s" % device["name"])
        print("total power points: %s" % total_power)
        print("total ph points: %s" % total_ph)
        print("total reactPowre points: %s" % total_reactPower)
        print("total rmsCur points: %s" % total_rmsCur)
        print("total rmsVolt points: %s" % total_rmsVolt)
        print("total freq points: %s" % total_freq)
        print("total timeStamp points: %s" % total_timeStamp)
        print("total points: {}".format(total_power + total_ph + total_reactPower +
                                        total_rmsCur + total_rmsVolt + total_freq +
                                        total_timeStamp))


    def make_data_frame(self, db) -> pd.DataFrame():
        """
        Create a dataframe for scikit usign ndarray
        :param db: the database retrieved from pi stystem
        :return: pandas daraframe
        """
        total_power = np.ndarray(shape=(0, 0), dtype=np.float32)
        total_ph = np.ndarray(shape=(0, 0), dtype=np.float32)
        total_reactPower = np.ndarray(shape=(0, 0), dtype=np.float32)
        total_rmsCur = np.ndarray(shape=(0, 0), dtype=np.float32)
        total_rmsVolt = np.ndarray(shape=(0, 0), dtype=np.float32)
        total_freq = np.ndarray(shape=(0, 0), dtype=np.float32)

        total_timeStamp = np.ndarray(shape=(0, 0), dtype=str)

        total_grp = list()
        total_name = list()

        labels = ["grp", "name", "power", "ph", "reacPower", "rmsCur", "rmsVolt",
                  "freq", "Timestamp"]
        df = pd.DataFrame(columns=labels)

        counter = 0
        for device in db:

            power = device["power"]
            total_power = np.append(total_power, power)

            ph = device["ph"]
            total_ph = np.append(total_ph, ph)

            reac_power = device["reactPower"]
            total_reactPower = np.append(total_reactPower, reac_power)

            rms_cur = device["rmsCur"]
            total_rmsCur = np.append(total_rmsCur, rms_cur)

            rms_volt = device["rmsVolt"]
            total_rmsVolt = np.append(total_rmsVolt, rms_volt)

            freq = device["freq"]
            total_freq = np.append(total_freq, freq)

            time = device["timeStamp"]
            total_timeStamp = np.append(total_timeStamp, time)

            for i in range(len(power)):
                total_grp.append(device['grp'])
                total_name.append(device['name'])

        df['power'] = total_power
        df['ph'] = total_ph
        df['reacPower'] = total_reactPower
        df['rmsCur'] = total_rmsCur
        df['rmsVolt'] = total_rmsVolt
        df['freq'] = total_freq

        df['Timestamp'] = total_timeStamp
        df['grp'] = total_grp
        df['name'] = total_name
        return df

# TODO: change the design to inherent Dataset and do analytics based on the dataset

class PredictDevice(object):
    def __init__(self, server_name, database, element):
        """

        :param server_name: The name of the PI System that has dataset PI Tags
        :param database:  The name of the database that stored the PI Tags
        :param element: The location of the top-parent of the database.\n
                        For example: "UCSF2\\UFL"
        """
        self.server = server_name
        self.database = database
        self.element_loc = element
        self._pi_dataset = DataSet()
        self.dataset = None
        self.dataframe = None
        self.le_grp = LabelEncoder()
        self.le_name = LabelEncoder()
        self.counter = 1
        self.dataset_interval = self._pi_dataset.ascf_interval

    def get_training_interval(self, start: datetime.datetime,
                              end: datetime.datetime):
        #CHECK: this needs to be changed based on the location of the dataset
        start_time = datetime.datetime(2018, 7, 24, 13, 11, 00)
        end_time = datetime.datetime(2018, 7, 24, 14, 15, 00)
        if start < start_time:
            print("no dataset available in starting date\n"
                  "Start time set to 2018/7/24 13:11:00")
            start_time = datetime.datetime(2018, 7, 24, 13, 11, 00)
        else:
            start_time = start

        if end > end_time:
            print("no dataset available at the end of date\n"
                  "End time set to 2018/7/24 14:15:00")
            end_time = datetime.datetime(2018, 7, 24, 14, 15, 00)
        else:
            end_time = end

        s_date = self._pi_dataset.pi.set_time(start_time.year,   start_time.month,
                                              start_time.day,    start_time.hour,
                                              start_time.minute, start_time.second)

        e_date = self._pi_dataset.pi.set_time(end_time.year,   end_time.month,
                                              end_time.day,    end_time.hour,
                                              end_time.minute, end_time.second)

        interval = self._pi_dataset.pi.time_span(0, 0, 10)

        db = self._pi_dataset.get_ascf2(self.server, self.database,
                                        self.element_loc, s_date, e_date, interval)
        return db

    def get_training_cumulative(self, interval):
        """
        get the dataset from PI system
        :return:
        """
        # CHECK: this needs to be changed based on the location of the dataset
        s_date = self._pi_dataset.pi.set_time(2018, 7, 24, 13, 11, 00)

        # CHECK: this needs to be changed based on the location of the dataset
        # set time interval
        date = datetime.datetime(2018, 7, 24, 13, 11, 00)
        delta = datetime.timedelta(seconds=self.counter * interval)
        date += delta
        print(date)
        e_date = self._pi_dataset.pi.set_time(date.year, date.month, date.day,
                                              date.hour, date.minute, date.second)

        # CHECK: The recorded database is at 10 seconds frequency
        interval = self._pi_dataset.pi.time_span(0, 0, 10)  # get values every 10 seconds

        x1 = timeit.default_timer()
        db = self._pi_dataset.get_ascf2(self.server, self.database,
                                        self.element_loc, s_date, e_date, interval)
        x2 = timeit.default_timer()
        print(x2 - x1)
        return db

    def get_dataframe(self, db):
        x1 = timeit.default_timer()
        features = self._pi_dataset.make_data_frame(db)
        x2 = timeit.default_timer()
        print("Creating Dataframe: {}".format(x2 - x1))  # 6.3 seconds
        return features

    def classify(self, dataset_features, input_feature):
        x1 = timeit.default_timer()
        dataset_features.iloc[:, 0] = self.le_grp.fit_transform(dataset_features.iloc[:, 0])
        dataset_features.iloc[:, 1] = self.le_name.fit_transform(dataset_features.iloc[:, 1])

        # plot_values('name')  # plot categorical data if needed
        X_train = dataset_features.iloc[:, 2:8]
        y_train = dataset_features.iloc[:, 0]

        neigh = KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree', leaf_size=50,
                                     n_jobs=-1)
        neigh.fit(X_train, y_train)

        y_pred = neigh.predict(dataset_features)
        result = self.le_grp.inverse_transform(y_pred)
        x2 = timeit.default_timer()
        print(x2 - x1)

        return result

    def predict_device(self, input_data):
        data = self.get_training()
        dataframe = self.get_dataframe(data)
        return self.classify(dataframe, input_data)

    def test_classify(self, features):
        """
        test classification
        :param features:
        :return: y_test, y_pred
        """
        x1 = timeit.default_timer()

        features.iloc[:, 0] = self.le_grp.fit_transform(features.iloc[:, 0])
        features.iloc[:, 1] = self.le_name.fit_transform(features.iloc[:, 1])

        # plot_values('name')  # plot categorical data if needed

        X_train, X_test, y_train, y_test = train_test_split(features.iloc[:, 2:8],
                                                            features.iloc[:, 0],
                                                            test_size=.4)

        neigh = KNeighborsClassifier(n_neighbors=5, algorithm='kd_tree', leaf_size=50,
                                     n_jobs=-1)
        neigh.fit(X_train, y_train)

        y_pred = neigh.predict(X_test)
        x2 = timeit.default_timer()
        print("Process KNN: {}".format(x2 - x1))
        return y_test, y_pred

    @staticmethod
    def get_cohen_kappa_score(y_true, y_pred):
        return cohen_kappa_score(y_true, y_pred)


# TODO: Plotting any thing needs to move to its own class using backend for QT
#
#     @staticmethod
#     def get_cmap(length, name='hsv'):
#         """
#         Generate colors to plot multi trends if needed
#         :param length:
#         :param name:
#         :return:
#         """
#         return plt.cm.get_cmap(name, length)
#
#     def plot_values(self, column_name, features):
#         """
#         Plot raw data values from the database if needed
#         :param column_name:
#         :param features: dataframe
#         :return:
#         """
#         if column_name == 'grp':
#             column = 0
#         elif column_name == 'name':
#             column = 1
#         else:
#             column = 0
#
#         fig, ax = plt.subplots()
#
#         features.iloc[:, 0] = self.le_grp.fit_transform(features.iloc[:, 0])
#         features.iloc[:, 1] = self.le_name.fit_transform(features.iloc[:, 1])
#
#         max_name = pd.DataFrame.max(features.iloc[:, column])
#         min_name = pd.DataFrame.min(features.iloc[:, column])
#
#         cmap = self.get_cmap(max_name - min_name)
#
#         for label, grp in features.groupby(column_name):
#             if column_name == 'grp':
#                 device_grp = self.le_grp.inverse_transform(label)
#             else:
#                 device_grp = label
#             ax.scatter(grp['rmsCur'], grp['rmsVolt'], label=device_grp,
#                        c=cmap(label - min_name), s=.5)
#
#             # grp.plot('power', 'reacPower', ax=ax, label=label) # nice
#
#         if column_name == 'grp':
#             plt.legend()
#         plt.show()
#
#
#
#
#     @staticmethod
#     def plot_confusion_matrix(cm, title='Confusion matrix',
#                               cmap=plt.cm.Blues):
#         """
#         To visualize the confusion matrix from sk-learn
#         :param cm:
#         :param title:
#         :param cmap:
#         :return:
#         """
#         plt.imshow(cm, interpolation='nearest', cmap=cmap)
#         plt.title(title)
#         plt.colorbar()
#
#         thresh = cm.max() / 2.
#         for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
#             plt.text(j, i, format(cm[i, j], 'd'),
#                      horizontalalignment='center',
#                      color='white' if cm[i, j > thresh] else 'black')
#
#         plt.tight_layout()
#         plt.ylabel('True label')
#         plt.xlabel('predicted label')











