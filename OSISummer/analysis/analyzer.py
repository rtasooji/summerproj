# -*- coding: utf-8 -*-
"""
Created on Tue May 29 08:40:07 2018

@author: rtasooji
"""
#%%
#moduels
import pandas as pd
import numpy as np
#%%


class AnalyzeFile(object):
    """The AnalizeFile class open csv file with pre-defined headers.\n
       pre-defined headers: Unix Time, current Time, log message\n
       Attributes:
           inputFile: csv file with pre-defined format
    """

    def __init__(self, inputfile):
        self.fileName = inputfile
        self.data = pd.read_csv(inputfile,
                                sep = ",",
                                names = ["Unix Time",
                                         "Current Time",
                                         "Logged Message"])
        self.unixTimes = self.data.iloc[:, 0]
        self.times = self.data.iloc[:, 1]
        self.messageLogs = self.data.iloc[:, 2]

    def get_shape(self):
        row, col = self.data.shape
        return row, col

    def get_unix_interval(self):
        len_input = len(self.unixTimes)
        if len_input % 2 == 0:
            output_result = self.calInterval(self.unixTimes,len_input)
        else: #ignore the last input
            output_result = self.calInterval(self.unixTimes,len_input - 1)
        return output_result

    def calMean_std(self, list_input):
        mean = np.mean(list_input)
        std = np.std(list_input)
        return mean, std

    @staticmethod
    def unixToTime(input_list):
        return pd.to_datetime(input_list, unit='s')

    @staticmethod
    def calInterval(inputList, endPoint):
        prevValue = None
        currValue = None
        intervalResult = list()
        counter = 0
        while (counter < endPoint):
            if counter % 2 == 0:
                prevValue = inputList[counter]
            else:
                currValue = inputList[counter]
                intervalResult.append(currValue - prevValue)
            counter += 1
        return intervalResult

#%%


