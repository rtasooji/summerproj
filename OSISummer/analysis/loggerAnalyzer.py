from analysis.analyzer import AnalyzeFile
from OSISummer.mqttpub.parsers.status import EmpaticaStatus
from timeit import default_timer as timer

start = timer()
print(type(EmpaticaStatus.on.value))
end = timer()
print(end - start)
file_analyzer = AnalyzeFile("timeLogger.txt ")
row, col = file_analyzer.get_shape()
interval = file_analyzer.calInterval(file_analyzer.unixTimes, row)
print("max time: " + str(max(interval)))
print("min time: " + str(min(interval)))
print(file_analyzer.calMean_std(interval))

"""
import timeit
value = timeit.timeit(stmt="'max time:{0}'.format(max(interval))",
                      setup="interval = [1,2,3,4,5,6,7,8,9,10];",
                      number=1000000)
value2 = timeit.timeit(stmt="'max time:' + str(max(interval))",
                      setup="interval = [1,2,3,4,5,6,7,8,9,10];",
                      number=1000000)
value3 = timeit.timeit(stmt="'max time:%f'%(max(interval))",
                      setup="interval = [1,2,3,4,5,6,7,8,9,10];",
                      number=1000000)
value4 = timeit.timeit(stmt="'max time: '.join(str(max(interval)))",
                      setup="interval = [1,2,3,4,5,6,7,8,9,10];",
                      number=1000000)
"""