"""
Created  by rtasooji on 7/30/2018
"""
#python packages
from abc import ABCMeta, abstractmethod
# project packages
from OSISummer.mqttReceiver.mqttQueue import MqttQueue


class MqttMessage(metaclass=ABCMeta):
    def __init__(self, topic: str):
        super(MqttMessage, self).__init__()
        self.topic_message = topic
        self.topic_list = []
        self.payload_queue = MqttQueue()

    @abstractmethod
    def set_topics(self):
        pass

    @abstractmethod
    def insert_values(self, payload):
        pass


class Messages(MqttMessage):
    def __init__(self, *args, **kwargs):
        super(Messages, self).__init__(*args, **kwargs)
        self.set_topics()

    def set_topics(self):
        topics = self.topic_message.split("/")
        for i in topics:
            self.topic_list.append(i)

    def insert_values(self, payload):
        """
        needs to be set based on the device features
        the features also needs to be added as message type in omf message
        :return:
        """
        self.payload_queue.put(payload)




