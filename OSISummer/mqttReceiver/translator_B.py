"""
Create mqtt client and connect to mqtt server.
trigger on_connect after connecting to mqtt server.
Triggers on_message callback upon receiving message from mqtt server
"""
from OSISummer.mqttReceiver.mqttQueue import MqttQueue
import datetime
import time  # for debugging purpose
from mqttReceiver.queueParser import Parser
from rx import Observable
from omf.messages.all import Messages
from omf.omfSender import OMFSender


class Convert(object):

    version = "1.0"

    def __init__(self, sender: OMFSender):
        """
        Creates mqtt client\n
        :param client_name: unique name\n
        :param ip_address: string\n
        :param port: int\n
        :param topic: follows MQTT topic format\n
        :param qos: quality of service int, default to one\n
        :param clean_ses: clean session default False\n
        """

        self.received_container = MqttQueue()
        self.received_1 = MqttQueue()
        self.received_2 = MqttQueue()
        self.received_3 = MqttQueue()
        self.received_points = MqttQueue()
        self._topics = set()
        self._assets = set()
        self.timer = time.time()
        self.sender = sender
        self._sent = True  # track if the message is sent before closing the receiver

        # CHECK: timedelta needs to be changed based on the network and data flow.
        # Best way to find the timedelta is to run for some time and see
        # what is the network latency and use that for best performance
        #
        # Below regression model provides estimation for choosing time interval:
        #   time in second = 0.022 + 0.000042 * C
        #   which C is total number of messages that are expected to be received
        #   This model highly depends on the local network speed and CPU clock speed
        #   The coefficients (42 µs) required time to process the message
        #   The intercept (22 ms) is the time required to send a message

        self._obs = Observable.interval(datetime.timedelta(seconds=0.15))

        #  counter is the size of the bulk sending through relay
        #   CHECK: this value depends on the data flow
        #   Use the regression model
        self._obs = self._obs.subscribe(lambda x: self.send_messages(counter=700))

    def send_type(self):
        """
        Send the type omf body to pi system.
        This is the starting point of the receiving and sending.
        If there are any chagnes in the format this message needs to be changed.
        To see the format check create_type method in Messages package
        :return:
        """
        # send the type body
        respond = self.sender.postMessage("type", Messages.create_type())
        # this code is received if PI system received the message.
        # It will raise exception if it is not.
        if respond.status_code != 204:
            print("something is wrong!")
            raise RESPONSEERROR("message didn't received by relay, received code: {}"
                                .format(respond.status_code))

    def send_messages(self, counter=100):
        """
        The order of sending messages are important for PI system to receive
        :param counter: number of messages in the bulk
        :return:
        """

        self.check_size(self.received_container, counter)  # create container
        self.check_size(self.received_1, counter)  # create asset data
        self.check_size(self.received_2, counter)  # create asset link

        self.check_size(self.received_3, counter)  # create container link
        
        self.check_size(self.received_points, counter)  # create pi tags

    def check_size(self, queue_type, counter_time=100):
        """
        Empty the queue based on the counter_time
        :param queue_type: the type of message on header
        :param counter_time: number of messages in the bulk
        :return:
        """
        counter = 0
        value_holder = list()
        if not queue_type.empty():
            self._sent = False
            # t0 = time.time()
            curr_queue = queue_type.get()  # gets the left in the queue
            # get the message type from the first value and store it
            msg_type = curr_queue[0]  # message type
            method_num = curr_queue[1]  # method to call
            values = list(curr_queue[2:])
            msg_body = Messages.call_method(method_num, values)[0]
            value_holder.append(msg_body)
            # deciding the size of the bulk sending
            while counter < counter_time:
                if not queue_type.empty():
                    # print("size of the queue: ", queue_type.curr_size)
                    # t0 = time.time()
                    curr_queue = queue_type.get()  # gets the left in the queue
                    method_num = curr_queue[1]  # method to call
                    values = list(curr_queue[2:])
                    msg_body = Messages.call_method(method_num, values)[0]
                    value_holder.append(msg_body)
                    counter += 1
                else:
                    break
            self.sender.postMessage(msg_type, value_holder, action='create')  # send
            self._sent = True
            # t1 = time.time()
            # print("time intervals: {}".format(t1 - t0))
        else:
            pass
            # print("Nothing in the queue, waiting...")

    # check size and empty_queue interact with each other
    def empty_queue(self, queue: MqttQueue):
        """
        Empty the queue on exit
        :param queue:
        :return:
        """
        if not queue.empty():
            curr_queue = queue.get()
            value_holder = list()
            msg_type = curr_queue[0]  # message type
            while not queue.empty():
                method_num = curr_queue[1]  # method to call
                values = list(curr_queue[2:])
                msg_body = Messages.call_method(method_num, values)[0]
                value_holder.append(msg_body)
            self.sender.postMessage(msg_type, value_holder, action='create')

    def validate_message(self, input_topic, input_payloads):
        """
        Place each message in the corresponding queue type
        :param input_topic: topic from publisher
        :param input_payloads: payload from publisher
        :return:
        """
        # convert the raw data to json format
        msg_topic = input_topic
        payloads = Parser.parse_mqtt(input_payloads)
        # TODO:
        # in case adding catch the info, make dictionary of queues from file in
        # constructor.
        if msg_topic not in self._topics:
            elements = msg_topic.split('/')
            # Add the message topic to the set
            self._topics.add(msg_topic)

            # Make the container, the order for calling methods is important.
            # The tuple format is as follow:
            #[0]: type of message in OMF format
            #[1]: The number of the method that will be called in
            #     the Messages class in omf
            #[2:]: The required parameters for the method
            self.received_container.put(("container", 0,
                                         msg_topic + '_container'))

            # Make the hierarchical elements for PI AF server
            for i in range(len(elements)):
                if i == 0:
                    topic_element = elements[i]
                    asset_index = elements[i]
                    # Ensure unique index for each asset
                    source_index = "_ROOT"  # needed for link message
                    target_index = asset_index  # needed for link message
                else:
                    topic_element = elements[i]
                    prev_asset_index = asset_index
                    asset_index = prev_asset_index + "/" + elements[i]
                    source_index = prev_asset_index  # needed for link message
                    target_index = asset_index  # needed for link message

                # Make sure each asset is created only once
                if asset_index not in self._assets:
                    self._assets.add(asset_index)
                    # create_asset_data, 1
                    self.received_1.put(("data", 1, asset_index, topic_element))
                    # Create and send link between assets
                    self.received_2.put(("data", 2, source_index, target_index))
                else:
                    continue

            # Create and send link between container and assets
            # To fully understand OMF message go to README.md file.
            self.received_3.put(("data", 3, msg_topic,
                                 msg_topic + '_container'))
            self.received_points.put(("data", 4, msg_topic + '_container',
                                     payloads['value'],
                                     payloads['time'],
                                     payloads['status'],
                                     payloads['desc']))
        else:
            self.received_points.put(("data", 4, msg_topic + '_container',
                                      payloads['value'],
                                      payloads['time'],
                                      payloads['status'],
                                      payloads['desc']))

    def close(self):
        # TODO:
        # This does not include other queues, such as containers or __links.
        # Current logic is: If there is no data body type, there is no need
        # to update PI system.
        # If catching system added to current implementation, this needs to be
        # fixed.
        # Some note about empty. empty does not close the interval thread.
        # The thread is running in the background.
        # To fully close the interval call dispose() methods.
        # Calling dispose will cause problem in GUI.
        # Fix the problem if it needed.
        self._obs.empty()
        while True:
            # Keep subscribing to the check_size method and empty the queue
            if not self.received_points.empty():
                continue
            else:
                # Do not close suddenly after queue is empty and make sure
                # the data is sent to PI system.
                if self._sent:
                    break
                else:
                    continue
