"""
Created  by rtasooji on 6/19/2018
for tracking the size of the queue, not reliable because of race condition
"""
from queue import Queue
import sys


class MqttQueue(Queue):

    def __init__(self):
        """
        To roughly track the size. Be aware of possible race
        conditions.\n
        """
        super(MqttQueue, self).__init__()
        self.curr_size = 0
        self.observed = False

    def _put(self, item):
        self.curr_size += sys.getsizeof(item)
        self.queue.append(item)

    def _get(self):
        item = self.queue.popleft()
        self.curr_size -= sys.getsizeof(item)
        return item
