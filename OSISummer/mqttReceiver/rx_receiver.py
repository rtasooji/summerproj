"""
Create mqtt client and connect to mqtt server.
trigger on_connect after connecting to mqtt server.
Triggers on_message callback upon receiving message from mqtt server
"""
import paho.mqtt.client as mqtt
import datetime
import time  # for debugging purpose

import mqttReceiver.queueParser as parser
from mqttReceiver.mqttQueue import MqttQueue
from omf.messages.all import Messages
from omf.omfSender import OMFSender

from rx import Observable


class MqttClient(object):

    version = "1.0"

    def __init__(self, sender: OMFSender, client_name: str, ip_address: str,
                 port: int, topic: str, user="", password="", qos: int = None,
                 clean_ses: bool = False):
        """
        Creates mqtt client\n
        :param client_name: unique name\n
        :param ip_address: string\n
        :param port: int\n
        :param topic: follows MQTT topic format\n
        :param qos: quality of service int, default to one\n
        :param clean_ses: clean session default False\n
        """

        if qos is None:
            self.qos_value = 1
        else:
            self.qos_value = qos
        self.connected = False
        self.topic = topic
        self.user_name = user
        self.client = mqtt.Client(client_name, clean_session=clean_ses)
        self.client.username_pw_set(user, password)
        self.client.on_connect = self.on_connect
        self.client.connect(ip_address, port)
        self.client.on_subscribe = self.on_subscribe
        self.client.subscribe(topic)
        self.client.on_message = self.on_message
        self.received_container = MqttQueue()
        self.received_1 = MqttQueue()
        self.received_2 = MqttQueue()
        self.received_3 = MqttQueue()
        self.received_points = MqttQueue()
        self._topics = set()
        self._assets = set()
        self.timer = time.time()
        self.sender = sender
        self._sent = True  # track if the message is sent before closing the receiver

        # CHECK: timedelta needs to be changed based on the network and data flow.
        # Best way to find the timedelta is to run for some time and see
        # what is the network latency and use that for best performance
        #
        # Below regression model provides estimation for choosing time interval:
        #   time in second = 0.022 + 0.000042 * C
        #   which C is total number of messages that are expected to be received
        #   This model highly depends on the local network speed and CPU clock speed
        #   The coefficients (42 µs) required time to process the message
        #   The intercept (22 ms) is the time required to send a message

        self._obs = Observable.interval(datetime.timedelta(seconds=0.15))

    def __enter__(self):
        """
        Start the mqtt client loop and return self, allows implementing objects
        which can be used easily with the 'with' statement.
        :return: self
        """
        self.client.loop_start()
        #  counter is the size of the bulk sending through relay
        #   CHECK: this value depends on the data flow
        #   Use the regression model
        self._obs.subscribe(lambda x: self.send_messages(counter=700))
        return self

    def __exit__(self, type, value, tb):
        """
        On exit make sure every messages in each queue are going to get empty
        before closing the connection.
        :param type: type
        :param value: value
        :param tb: tb
        """
        # TODO:
        # This does not include other queues, such as containers or __links.
        # Current logic is: If there is no data body type, there is no need
        # to update PI system.
        # If catching system added to current implementation, this needs to be
        # fixed.
        while True:
            # Keep subscribing to the check_size method and empty the queue
            if not self.received_points.empty():
                continue
            else:
                # Do not close suddenly after queue is empty and make sure
                # the data is sent to PI system.
                if self._sent:
                    break
                else:
                    continue
        self.client.loop_stop()
        self.client.disconnect()

    def on_subscribe(self, client, userdata, mid, granted_qos):
        print("subscribed to {}".format(mid))

    def on_connect(self, client, userdata, flags, rc):
        """
        Function: on_connect call back
        :param client: MQTT client instance
        :param userdata: private user data
        :param flags: response falgs sent by MQTT server
        :param rc: result code
        """
        if rc == 0:
            self.connected = True
        else:
            print("Connection failed")
            self.client.loop_stop()

        print("on_connect:Connected with flags:{0},result code:{1}".format(flags,
              rc))

    def send_messages(self, counter=100):
        """
        The order of sending messages are important for PI system to receive
        :param counter: number of messages in the bulk
        :return:
        """

        self.check_size(self.received_container, counter)  # create container
        self.check_size(self.received_1, counter)  # create asset data
        self.check_size(self.received_2, counter)  # create asset link

        self.check_size(self.received_3, counter)  # create container link

        self.check_size(self.received_points, counter)  # create pi tags

    def check_size(self, queue_type, counter_time=100):
        """
        Empty the queue based on the counter_time
        :param queue_type: the type of message on header
        :param counter_time: number of messages in the bulk
        :return:
        """
        counter = 0
        value_holder = list()
        if not queue_type.empty():
            self._sent = False
            # t0 = time.time()
            curr_queue = queue_type.get()  # gets the left in the queue
            # get the message type from the first value and store it
            msg_type = curr_queue[0]  # message type
            method_num = curr_queue[1]  # method to call
            values = list(curr_queue[2:])
            msg_body = Messages.call_method(method_num, values)[0]
            value_holder.append(msg_body)
            # deciding the size of the bulk sending
            while counter < counter_time:
                if not queue_type.empty():
                    # print("size of the queue: ", queue_type.curr_size)
                    # t0 = time.time()
                    curr_queue = queue_type.get()  # gets the left in the queue
                    method_num = curr_queue[1]  # method to call
                    values = list(curr_queue[2:])
                    msg_body = Messages.call_method(method_num, values)[0]
                    value_holder.append(msg_body)
                    counter += 1
                else:
                    break
            self.sender.postMessage(msg_type, value_holder, action='create')  # send
            self._sent = True
            # t1 = time.time()
            # print("time intervals: {}".format(t1 - t0))
        else:
            pass
            # print("Nothing in the queue, waiting...")

    # check size and empty_queue interact with each other
    def empty_queue(self, queue: MqttQueue):
        """
        Empty the queue on exit
        :param queue:
        :return:
        """
        if not queue.empty():
            curr_queue = queue.get()
            value_holder = list()
            msg_type = curr_queue[0]  # message type
            while not queue.empty():
                method_num = curr_queue[1]  # method to call
                values = list(curr_queue[2:])
                msg_body = Messages.call_method(method_num, values)[0]
                value_holder.append(msg_body)
            response = self.sender.postMessage(msg_type, value_holder, action='create')
            print(response.status_code)


    def on_message(self, client, userdata, message):
        """
        This is the on_message call back
        :param client: mqtt client
        :param userdata: user data
        :param message: mqtt message
        """
        try:
            msg_topic = message.topic
            rec_msg = message.payload
            # CHECK:rec_topic is redundant but might change in future lets separate it
            if msg_topic is not None:
                self.validate_message(msg_topic, rec_msg)
            self.timer = time.time()
            print("message received.")
        except Exception:
            client.disconnect()
            client.loop_stop()
            print("Exception happened!")
            raise Exception

    def validate_message(self, input_topic, input_payloads):
        """
        Place each message in the corresponding queue type
        :param input_topic:
        :param input_payloads:
        :return:
        """
        messages = parser.parse_input(input_topic, input_payloads)
        for msg in messages:
            msg_topic = msg[0]
            print(input_topic)
            payloads = msg[1]
            if type(payloads['value']) is str:
                container_type = 1
                type_id = 2
            else:
                container_type = 0
                type_id = 1

            self.received_container.put(("container", 0,
                                         msg_topic + '_container', container_type))

            # TODO:
            # in case adding catch the info, make dictionary of queues from file in
            # constructor.

            if msg_topic not in self._topics:
                elements = msg_topic.split('/')
                # Add the message topic to the set
                self._topics.add(msg_topic)

                # Make the container, the order for calling methods is important.
                # The tuple format is as follow:
                #[0]: type of message in OMF format
                #[1]: The number of the method that will be called in
                #     the Messages class in omf
                #[2:]: The required parameters for the method

                # Make the hierarchical elements for PI AF server
                for i in range(len(elements)):
                    if i == 0:
                        topic_element = elements[i]
                        asset_index = elements[i]
                        # Ensure unique index for each asset
                        source_index = "_ROOT"  # needed for link message
                        target_index = asset_index  # needed for link message
                    else:
                        topic_element = elements[i]
                        prev_asset_index = asset_index
                        asset_index = prev_asset_index + "/" + elements[i]
                        source_index = prev_asset_index  # needed for link message
                        target_index = asset_index  # needed for link message

                    # Make sure each asset is created only once
                    if asset_index not in self._assets:
                        self._assets.add(asset_index)
                        # create_asset_data, 1
                        if i == len(elements) - 1:
                            self.received_1.put(("data", 1, asset_index, topic_element, type_id))
                            self.received_2.put(("data", 2, source_index, target_index, type_id))

                        else:
                            self.received_1.put(("data", 1, asset_index, topic_element, 0))
                            self.received_2.put(("data", 2, source_index, target_index, 0))
                    else:
                        continue

                # Create and send link between container and assets
                self.received_3.put(("data", 3, msg_topic,
                                     msg_topic + '_container', type_id))

                self.received_points.put(("data", 4, msg_topic + '_container',
                                          payloads['value'],
                                          payloads['time']))

            else:
                self.received_points.put(("data", 4, msg_topic + '_container',
                                          payloads['value'],
                                          payloads['time']))







