"""
Created  by rtasooji on 6/19/2018
"""
import json
import datetime
from abc import ABCMeta, abstractmethod
import re

def parse_input(topic, payload):

    if topic == "MATE3s":
        return MATE3Parser.parse_mqtt(topic, payload)
    elif topic == "ComBox":
        return ComBox.parse_mqtt(topic, payload)
    else:
        out_msg = dict()
        msg = payload.decode('utf-8')
        p = re.compile('\d+(\.\d+)?')
        # if payload is digit convert it to float
        if p.match(msg):
            out_msg['Value'] = float(msg)
        else:
            msg = convert_string(msg.lower())
            out_msg['Value'] = msg

        return Parser.parse_mqtt(topic, out_msg)


def convert_string(msg):
    if msg == "on" or msg == "open" or msg == "up":
        return 1
    elif msg == "off" or msg == "close" or msg == "down":
        return 0
    elif "stop":
        return 2


class AbstractParser(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def parse_mqtt(topic, payload):
        """
        :param topic:
        :param payload:
        :return:
        """
        pass


class Parser(AbstractParser):

    @staticmethod
    def parse_mqtt(topic, payload):
        """
        Parse received message in the subscriber's queue
        :param topic: message topic
        :param payload: payload received from receiver
        :return: elements[topics], payload[data, time, status, description]
        """
        try:

            received_value = Parser._parse_json(topic, payload)

        except ValueError:
            print("Can not parse the value")
            return None
        return received_value

    @staticmethod
    def _parse_json(topic, message):
        """
        This needs to be changed based on the mqtt publisher\n
        :param payload: json message received on mqtt message payload
        :return: list of values
        """
        data = message['Value']
        time = Parser.get_time()
        payload = Parser.make_std_payload(time, data)
        output = (topic, payload)
        result = list()
        result.append(output)
        return result

    @staticmethod
    def make_std_payload(time, data):
        return {"time": time,
                "value": data}

    @classmethod
    def get_payload(cls, topic, payload):
        return "Received mqtt message:\ntopic: {0}\npayload: {1}\n" \
               "time: {2}".format(topic, payload, datetime.datetime.now())

    @staticmethod
    def get_time():
        """
        get current time. For debugging purposes
        :return:
        """
        return datetime.datetime.utcnow().isoformat() + 'Z'
        # return (datetime.datetime.utcnow() + datetime.timedelta(
        #     days=0)).isoformat() + 'Z'


class MATE3Parser(Parser):
    @staticmethod
    def parse_mqtt(topic, payload):
        try:
            payload_json = json.loads(payload)
            received_value = MATE3Parser._parse_json(topic, payload_json)

        except ValueError:
            print("The data is not a valid JSON, return None")
            return None
        return received_value

    @staticmethod
    def _parse_json(topic, json_mqtt):
        """
        This needs to be changed based on the mqtt publisher\n
        :param json_mqtt: json message received on mqtt message payload
        :return: list of values
        """
        result = list()
        time = datetime.datetime.utcnow().isoformat()+'Z'

        measured_voltage = 0.0
        sell_voltage = 0.0
        inverter = 0.0
        dc_voltage = 0.0
        split_inverter = 0.0
        split_sell = 0.0
        shunt_a = 0.0
        shunt_b = 0.0
        shunt_c = 0.0
        battery_volt = 0.0
        battery_curr = 0.0

        for value in json_mqtt:
            if 'name' in value:
                if value['name'] == 'OutBack_Measured_System_Voltage':
                    measured_voltage = value['value']
                    MATE3Parser._make_message(result, topic + "/Sys_volt", time, measured_voltage)
                elif value['name'] == 'OB_Set_Sell_Voltage':
                    sell_voltage = value['value']
                    MATE3Parser._make_message(result, topic + "/Sell_volt", time, sell_voltage)
                elif value['name'] == 'OB_Set_Radian_Inverter_Sell_Current_Limit':
                    inverter = value['value']
                    MATE3Parser._make_message(result, topic + "/inverter_sell", time, inverter)
                elif value['name'] == 'I_DC_Voltage':
                    dc_voltage = value['value']
                    MATE3Parser._make_message(result, topic + "/dc_volt", time, dc_voltage)
                elif value['name'] == 'GS_Split_Inverter_Operating_Mode':
                    split_inverter = value['value']
                    MATE3Parser._make_message(result, topic + "/split_inv", time, split_inverter)
                elif value['name'] == 'GS_Split_Sell_kW':
                    split_sell = value['value']
                    MATE3Parser._make_message(result, topic + "/split_sell", time, split_sell)
                elif value['name'] == 'FN_Shunt_A_Current':
                    shunt_a = value['value']
                    MATE3Parser._make_message(result, topic + "/shunt_a", time, shunt_a)
                elif value['name'] == 'FN_Shunt_B_Current':
                    shunt_b = value['value']
                    MATE3Parser._make_message(result, topic + "/shunt_b", time, shunt_b)
                elif value['name'] == 'FN_Shunt_C_Current':
                    shunt_c = value['value']
                    MATE3Parser._make_message(result, topic + "/shunt_c", time, shunt_c)
                elif value['name'] == 'FN_Battery_Voltage':
                    battery_volt = value['value']
                    MATE3Parser._make_message(result, topic + "/battery_volt", time, battery_volt)
                elif value['name'] == 'FN_Battery_Current':
                    battery_curr = value['value']
                    MATE3Parser._make_message(result, topic + "/battery_curr", time, battery_curr)
        return result

    @staticmethod
    def _make_message(output: [], topic, time, value):
        payload = MATE3Parser.make_std_payload(time, value)
        curr_topic = topic
        data = (curr_topic, payload)
        output.append(data)

    @staticmethod
    def get_timestamp(input: list):
        for value in input:
            if value['name'] == 'Outback_Year':
                year = value['value']
            if value['name'] == 'Outback_Month':
                month = value['value']
            if value['name'] == 'Outback_Day':
                day = value['value']
            if value['name'] == 'Outback_Hour':
                hour = value['value']
            if value['name'] == 'Outback_Minute':
                minute = value['value']
            if value['name'] == 'Outback_Second':
                second = value['value']
        date = datetime.datetime(year=year, month=month, day=day, hour=hour,
                                 minute=minute, second=second).isoformat() + 'Z'
        return str(date)


class ComBox(Parser):
    @staticmethod
    def parse_mqtt(topic, payload):
        try:
            payload_json = json.loads(payload)
            received_value = ComBox._parse_json(topic, payload_json)

        except ValueError:
            print("The data is not a valid JSON, return None")
            return None
        return received_value

    @staticmethod
    def _parse_json(topic, json_mqtt):
        """
        This needs to be changed based on the mqtt publisher\n
        :param json_mqtt: json message received on mqtt message payload
        :return: list of values
        """
        result = list()
        time = Parser.get_time()

        harvest_pow = 0.0
        charging_pow = 0.0
        charging_curr = 0.0
        pv_power = 0.0
        battery_curr = 0.0
        battery_pow = 0.0
        battery_volt = 0.0
        battery_temp = 0.0
        battery_curr_net = 0.0
        battery_pow_net = 0.0
        avg_pv_volt = 0.0
        total_pv_curr = 0.0

        for value in json_mqtt:
            if 'name' in value:
                if value['name'] == 'PV Harvest Power':
                    harvest_pow = value['value']
                    ComBox._make_message(result, topic + "/harvest_pow", time, harvest_pow)
                elif value['name'] == 'DC Charging Power':
                    charging_pow = value['value']
                    ComBox._make_message(result, topic + "/charging_pow", time, charging_pow)
                elif value['name'] == 'DC Charging Current':
                    charging_curr = value['value']
                    ComBox._make_message(result, topic + "/charging_curr", time, charging_curr)
                elif value['name'] == 'MPPT PV Power':
                    pv_power = value['value']
                    ComBox._make_message(result, topic + "/pv_power", time, pv_power)

                elif value['name'] == 'MPPT Battery Current':
                    battery_curr = value['value']
                    ComBox._make_message(result, topic + "/battery_curr", time, battery_curr)
                elif value['name'] == 'MPPT Battery Power':
                    battery_pow = value['value']
                    ComBox._make_message(result, topic + "/battery_pow", time, battery_pow)
                elif value['name'] == 'Battery Voltage':
                    battery_volt = value['value']
                    ComBox._make_message(result, topic + "/battery_volt", time, battery_volt)
                elif value['name'] == 'Battery Temperature':
                    battery_temp = value['value']
                    ComBox._make_message(result, topic + "/battery_temp", time, battery_temp)
                elif value['name'] == 'Battery Current Net':
                    battery_curr_net = value['value']
                    ComBox._make_message(result, topic + "/battery_curr_net", time, battery_curr_net)
                elif value['name'] == 'Battery Power Net':
                    battery_pow_net = value['value']
                    ComBox._make_message(result, topic + "/battery_pow_net", time, battery_pow_net)
                elif value['name'] == 'Average PV Voltage':
                    avg_pv_volt = value['value']
                    ComBox._make_message(result, topic + "/avg_pv_volt", time, avg_pv_volt)
                elif value['name'] == 'Total PV Current':
                    total_pv_curr = value['value']
                    ComBox._make_message(result, topic + "/total_pv_curr", time, total_pv_curr)

        return result

    @staticmethod
    def _make_message(output: [], topic, time, value):
        payload = ComBox.make_std_payload(time, value)
        curr_topic = topic
        data = (curr_topic, payload)
        output.append(data)
