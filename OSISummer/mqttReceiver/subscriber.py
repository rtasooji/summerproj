"""
Create mqtt client and connect to mqtt server.
trigger on_connect after connecting to mqtt server.
Triggers on_message callback upon receiving message from mqtt server
"""
import paho.mqtt.client as mqtt
import PySide2.QtCore as QtCore


class MqttSub(object):
    version = "1.0"

    class Signal(QtCore.QObject):
        message = QtCore.Signal(list)

    def __init__(self, client_name: str, ip_address: str,
                 port: int, topic: str, user="", password="", qos: int = None,
                 clean_ses: bool = False):
        """
        Creates mqtt client\n
        :param client_name: unique name\n
        :param ip_address: string\n
        :param port: int\n
        :param topic: follows MQTT topic format\n
        :param qos: quality of service int, default to one\n
        :param clean_ses: clean session default False\n
        """
        self.signal = self.Signal()
        if qos is None:
            self.qos_value = 1
        else:
            self.qos_value = qos
        self.connected = False
        self.topic = topic
        self.user_name = user
        self.client = mqtt.Client(client_name, clean_session=clean_ses)
        self.client.username_pw_set(user, password)
        self.client.on_connect = self.on_connect
        self.client.connect(ip_address, port)
        self.client.on_subscribe = self.on_subscribe
        self.client.subscribe(topic)
        self.client.on_message = self.on_message



    def __enter__(self):
        """
        Start the mqtt client loop and return self, allows implementing objects
        which can be used easily with the 'with' statement.
        :return: self
        """
        self.client.loop_start()
        return self

    def __exit__(self, type, value, tb):
        """
        On exit make sure every messages in each queue are going to get empty
        before closing the connection.
        :param type: type
        :param value: value
        :param tb: tb
        """

        self.client.loop_stop()
        self.client.disconnect()
        self.signal.blockSignals(True)

    def on_subscribe(self, client, userdata, mid, granted_qos):
        print("subscribed to {}".format(mid))

    def on_connect(self, client, userdata, flags, rc):
        """
        Function: on_connect call back
        :param client: MQTT client instance
        :param userdata: private user data
        :param flags: response falgs sent by MQTT server
        :param rc: result code
        """
        if rc == 0:
            self.connected = True
        else:
            print("Connection failed")
            self.client.loop_stop()

        print("on_connect:Connected with flags:{0},result code:{1}".format(flags,
              rc))

    def on_message(self, client, userdata, message):
        """
        This is the on_message call back
        :param client: mqtt client
        :param userdata: user data
        :param message: mqtt message
        """
        try:
            rec_topic = message.topic
            rec_msg = message.payload
            # CHECK:rec_topic is redundant but might change in future lets separate it

            result = [rec_topic, rec_msg]
            # send to main thread
            self.signal.message.emit(result)
        except Exception as e:
            client.disconnect()
            client.loop_stop()
            print("Exception happened!")
            print("Message{}".format(e))
            raise Exception
        return rec_topic, rec_msg







