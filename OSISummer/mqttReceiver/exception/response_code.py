"""
Created  by rtasooji on 6/27/2018
"""
class RESPONSEERROR(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = ""

    def __str__(self):
        """
        This method is being called with the exception is being raised
        :return:
        """
        if self.message:
            return "message {}".format(self.message)
        else:
            return "message"