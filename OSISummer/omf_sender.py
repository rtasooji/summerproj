# Created by dark at 6/22/2018, 5:48 PM
"""
This script will create a listener for receiving payloads from mqtt borker.
Before running the code check "TO DO" lists. some of the time stamps might
commented out or changed for testing purposes.
"""
import time
import json
from mqttReceiver.translator import Convert
from omf.omfSender import OMFSender
from omf.messages.all import Messages
import mqttReceiver.resources as rs
import os
from mqttReceiver.exception.response_code import RESPONSEERROR


# The OMFSender is needed to send messages to the PI system.


class SendOMF(object):
    def __init__(self):

        self._user_inputs = self._get_values()
        self.sender = self._make_sender(self._user_inputs)

        self.sender = Convert(self.sender)
        self.sender.send_type()

    @staticmethod
    def _get_values() -> dict:
        """
        Get values from the context.json file in the resources folder in
         mqttReceive package
        :return: relay and broker inputs
        """
        usr_input = dict()
        res_path = os.path.dirname(rs.__file__)
        try:
            with open(os.path.join(res_path, "context.json")) as f:
                usr_input = json.load(f)
        except FileExistsError:
            print("file not found!")
        return usr_input

    def _make_sender(self, user_inputs: dict) -> OMFSender:
        """
        Creates a sender object that will be used to send the bulk of data inside
        mqtt receiver
        :param user_inputs: producer token and ingress url generated using PI data
        collection manager
        :return:
        """
        producer_token = user_inputs['relay']['producer_token']
        ingress_url = user_inputs['relay']['Ingress_url']
        return OMFSender(ingress_url=ingress_url, producer_token=producer_token)






def run():
    inputs = get_values()
    make_sender(inputs)
    send_type()
    start_listener(inputs)


if __name__ == '__main__':
    run()

