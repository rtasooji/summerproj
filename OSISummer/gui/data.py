"""
Created  by rtasooji on 8/6/2018
"""
# python packages
import pandas as pd
# project packages
from OSISummer.gui.main import MyMenu


import PySide2.QtCore as QtCore


class DataHolder(QtCore.QObject):

    plot_data = list()
    predict_data = list()

    def __init__(self):
        super(DataHolder, self).__init__()
        MyMenu.input_data.connect(print)

    def print(self, input:dict):
        print("********")
        print(input)
        print("we are in data")