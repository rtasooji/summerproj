import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore


class TopicResult(QtCore.QObject):
    signal = QtCore.Signal(str)

    def __init__(self):
        super(TopicResult, self).__init__()


class TopicList(QtWidgets.QListWidget):

    def __init__(self, parent=None, items=list()):
        super(TopicList, self).__init__()
        self.setParent(parent)
        self.init_UI(items)

    def init_UI(self, items):
        # make the list
        if len(items) > 0:
            for i in items:
                QtWidgets.QListWidgetItem(str(i), self)

            self.setCurrentRow(0)
        else:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setText("No Topic Received!")
            msg_box.exec_()


class TopicSelect(QtWidgets.QWidget):

    def __init__(self):
        super(TopicSelect, self).__init__()
        self.result = TopicResult()

    def init_UI(self, topic):

        self.lists = TopicList(parent=self, items=topic)
        self.lists.itemDoubleClicked.connect(self.get_value_d)
        button = QtWidgets.QPushButton('&Select', self)

        button.clicked.connect(self.get_value)
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.lists)
        layout.addWidget(button)

    def get_value_d(self, item):
        self.result.signal.emit(item.text())
        self.close()

    def get_value(self):
        self.result.signal.emit(self.lists.currentItem().text())
        self.close()
