"""
Created  by rtasooji on 7/31/2018
"""
import time
import json
from OSISummer.mqttReceiver.subscriber import MqttSub
import mqttReceiver.resources as rs
import os
import uuid
import PySide2.QtCore as QtCore


class MqttSub(QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)
        inputs = self.get_values()
        self.subscriber = self.make_listener(inputs)

    def run(self, *args, **kwargs):
        self.subscriber.client.loop_start()
        self.start_listener(self.subscriber)

    def __del__(self):
        self.subscriber.client.loop_stop()
        self.subscriber.client.disconnect()
        self.exit()

    @classmethod
    def get_values(cls) -> dict:
        """
        Get values from the context.json file in the resources folder in
         mqttReceive package
        :return: relay and broker inputs
        """
        usr_input = dict()
        res_path = os.path.dirname(rs.__file__)
        try:
            with open(os.path.join(res_path, "context.json")) as f:
                usr_input = json.load(f)
        except FileExistsError:
            print("file not found!")
        return usr_input

    def make_listener(self, user_inputs: dict):
        """
        Creates mqtt subscriber and send messages to the PI system
        :param user_inputs: **kwargs
        :return:
        """
        # the sender is required by the receiver to send the queue
        # set the values for starting mqtt subscriber
        client_name = str(uuid.uuid4())
        ip_address = user_inputs['broker']['ip_address']
        port_num = user_inputs['broker']['port_num']
        topic_name = user_inputs['broker']['topic_name']
        user_name = user_inputs['broker']['user_name']
        password = user_inputs['broker']['password']
        waiting_time = user_inputs['broker']['waiting_time']
        # start subscriber
        return MqttSub(client_name,
                       ip_address,
                       port_num,
                       topic_name,
                       user=user_name,
                       password=password,
                       qos=1,
                       clean_ses=False)

    @staticmethod
    def start_listener(f):

        try:
            while True:
                if f.connected is False:
                    time.sleep(0.5)
                    print("not connected")
                else:
                    time.sleep(0.000005)
        except KeyboardInterrupt:
            print("No message received in %s second. Closing the "
                  "subscriber." % waiting_time)

