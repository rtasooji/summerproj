"""
Created  by rtasooji on 8/1/2018
"""
import PySide2.QtCore as QtCore
import datetime
from rx import Observable

from OSISummer.analysis.electric.knn import PredictDevice


class KnnThread(QtCore.QThread):
    def __init__(self, server, database, element_location, delta=10):
        QtCore.QThread.__init__(self)

        self.start_time = None
        self._obs = Observable.interval(datetime.timedelta(seconds=delta))
        self.device_feature = {"grp": "", "name": "", "timeStamp": list(),
                               "ph": list(), "power": list(), "reactPower": list(),
                               "rmsCur": list(), "rmsVolt": list(), "freq": list()}
        self._predict_device = PredictDevice(server, database, element_location)

    def __del__(self):
        self.exit()

    # when the plug status is on store time
    # every 10 sec predict device
    # after 7 minute the device needs to located to its group
    def run(self):
        self.start_time = datetime.datetime.now()
        # subscribe to function with fixed time interval
        self._obs.subscribe(lambda x: self.predictor(self._predict_device,
                                                     self.device_feature))

    # TODO: the input_device for the object needs to get updated
    def predictor(self, input_pre: PredictDevice, input_device):
        time_interval = datetime.datetime.now() - self.start_time
        while time_interval <= input_pre.dataset_interval:
            dataset = input_pre.get_training_cumulative(10)
            dataframe = input_pre.get_dataframe(dataset)
            print(dataframe)
            # TODO: on real device check this
            # result = input_pre.predict_device()
            #input_pre.plot_values('grp', dataframe)

            input_pre.counter += 1
            print(self._predict_device)