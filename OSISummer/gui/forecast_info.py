"""
Created  by rtasooji on 8/3/2018
"""
import sys
import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore


class MessageResult(QtCore.QObject):
    signal = QtCore.Signal(dict)

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        super().__init__()


class ForecastInfo(QtWidgets.QWidget):
    def __init__(self):
        super(ForecastInfo, self).__init__()
        top_grp = QtWidgets.QGroupBox("&Server Info", self)
        self.result = MessageResult()

    # inputs
        # server input
        server_label = QtWidgets.QLabel("Server:", top_grp)
        self.server_line = QtWidgets.QLineEdit(top_grp)
        self.server_line.setText("osisoft")
        self.server_line.setPlaceholderText("Server name")
        self.server_line.setFocus()

        # database input
        database_label = QtWidgets.QLabel("Database:", top_grp)
        self.database_line = QtWidgets.QLineEdit(top_grp)
        self.database_line.setText("futurehaus")
        self.database_line.setPlaceholderText("database name")

        # element input
        element_label = QtWidgets.QLabel("Element:", top_grp)
        self.element_line = QtWidgets.QLineEdit(top_grp)
        self.element_line.setText("omf\\MATE3s\\battery_volt")
        self.element_line.setPlaceholderText("Element location")

        # start time
        t_start = QtWidgets.QLabel("Start time:", top_grp)
        self.t_start_line = QtWidgets.QLineEdit(top_grp)
        self.t_start_line.setText("-10D")
        self.t_start_line.setPlaceholderText("start time for fitting data")

        # end time
        e_time = QtWidgets.QLabel("End Time:", top_grp)
        self.e_time_line = QtWidgets.QLineEdit(top_grp)
        self.e_time_line.setText("8/29/2018 12:00:00 pm")
        self.e_time_line.setPlaceholderText("end time for fitting data")

        # forecast period
        forecast = QtWidgets.QLabel("Forecast Period:", top_grp)
        self.forecast = QtWidgets.QLineEdit(top_grp)
        self.forecast.setText("1")
        self.forecast.setPlaceholderText("Set the duration to predict ")

        # forecast frequency
        freq = QtWidgets.QLabel("Forecast frequency:", top_grp)
        self.freq = QtWidgets.QLineEdit(top_grp)
        self.freq.setText("H")
        self.freq.setPlaceholderText("Check pandas date range")

        # buttons
        ok_but = QtWidgets.QPushButton("Ok", top_grp)

        cancel_but = QtWidgets.QPushButton("Cancel", top_grp)

        # connect signals
        cancel_but.clicked.connect(self.cancel)
        ok_but.clicked.connect(self.ok)

        # grid layout
        grid_layout = QtWidgets.QGridLayout(top_grp)
        grid_layout.addWidget(server_label, 0, 0)
        grid_layout.addWidget(self.server_line,  0, 1)

        grid_layout.addWidget(database_label, 1, 0)
        grid_layout.addWidget(self.database_line,  1, 1)

        grid_layout.addWidget(element_label, 2, 0)
        grid_layout.addWidget(self.element_line,  2, 1)

        grid_layout.addWidget(t_start, 3, 0)
        grid_layout.addWidget(self.t_start_line, 3, 1)

        grid_layout.addWidget(e_time, 4, 0)
        grid_layout.addWidget(self.e_time_line, 4, 1)

        grid_layout.addWidget(forecast, 5, 0)
        grid_layout.addWidget(self.forecast, 5, 1)

        grid_layout.addWidget(freq, 6, 0)
        grid_layout.addWidget(self.freq, 6, 1)

        grid_layout.addWidget(ok_but, 7, 0)
        grid_layout.addWidget(cancel_but,  7, 1)

        top_grp.setLayout(grid_layout)

        self.setFocus()
        self.resize(top_grp.sizeHint())

    def cancel(self):
        self.close()

    def ok(self):
        server = self.server_line.text()
        database = self.database_line.text()
        element = self.element_line.text()
        start = self.t_start_line.text()
        end = self.e_time_line.text()
        period = self.forecast.text()
        freq = self.freq.text()

        result = {'server': server,
                  'database': database,
                  'element': element,
                  'start': start,
                  'end': end,
                  'period': period,
                  'freq': freq
                  }
        self.result.signal.emit(result)
        self.close()

# if __name__ == '__main__':
#     app = QtWidgets.QApplication(sys.argv)
#     win = ServerInfo()
#     win.show()
#     sys.exit(app.exec_())