"""
Created  by rtasooji on 8/6/2018
"""
import mqttReceiver.resources as rs
import os
import uuid
import time
import json


import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore

"""
client_name = str(uuid.uuid4())
ip_address = user_inputs['broker']['ip_address']
port_num = user_inputs['broker']['port_num']
topic_name = user_inputs['broker']['topic_name']
user_name = user_inputs['broker']['user_name']
password = user_inputs['broker']['password']
waiting_time = user_inputs['broker']['waiting_time']
"""




class ReceiverInfo(QtWidgets.QWidget):

    class Signal(QtCore.QObject):
        signal = QtCore.Signal(dict)

        def __init__(self):
            # Initialize the PunchingBag as a QObject
            super().__init__()

    def __init__(self, parent=None):
        super(ReceiverInfo, self).__init__()
        self.setParent(parent)
        self.result = self.Signal()
    # Relay Info
        user_inputs = self.get_default_values()

        relay_grp = QtWidgets.QGroupBox("&Relay Info", self)

        # echoMode=QtWidgets.QLineEdit.Password)
        # producer token
        relay_label = QtWidgets.QLabel("Producer Token:", relay_grp)
        self.relay_prod_line = QtWidgets.QLineEdit(relay_grp)
        self.relay_prod_line.setPlaceholderText("Get this from PI Data Collection Manager")
        self.relay_prod_line.setText(user_inputs["relay"]["producer_token"])
        self.relay_prod_line.setFixedWidth(700)

        # Ingress URL
        ingress_label = QtWidgets.QLabel("Ingress URL:", relay_grp)
        self.ingress_prod_line = QtWidgets.QLineEdit(relay_grp)
        self.ingress_prod_line.setPlaceholderText("Get this from PI Data Collection Manager")
        self.ingress_prod_line.setText(user_inputs["relay"]["Ingress_url"])
        self.ingress_prod_line.setFixedWidth(700)

        # grid layout
        relay_grp_layout = QtWidgets.QGridLayout(relay_grp)
        relay_grp_layout.addWidget(relay_label, 0, 0)
        relay_grp_layout.addWidget(self.relay_prod_line, 0, 1)
        relay_grp_layout.addWidget(ingress_label, 1, 0)
        relay_grp_layout.addWidget(self.ingress_prod_line, 1, 1)

        relay_grp.setLayout(relay_grp_layout)

    # broker info

        # stored values
        client_name = str(uuid.uuid4())
        ip_address = user_inputs['broker']['ip_address']
        port_num = user_inputs['broker']['port_num']
        topic_name = user_inputs['broker']['topic_name']
        user_name = user_inputs['broker']['user_name']
        password = user_inputs['broker']['password']
        waiting_time = user_inputs['broker']['waiting_time']

        broker_grp = QtWidgets.QGroupBox("&Broker Info", self)

        # IP address
        ip_label = QtWidgets.QLabel("Broker IP:", broker_grp)
        self.ip_line = QtWidgets.QLineEdit(broker_grp)
        self.ip_line.setPlaceholderText("Broker IP")
        self.ip_line.setText(ip_address)

        # port number
        port_label = QtWidgets.QLabel("Port:", broker_grp)
        self.port_line = QtWidgets.QLineEdit(broker_grp)
        self.port_line.setPlaceholderText("Broker Port")
        self.port_line.setText(str(port_num))

        # topic number
        topic_label = QtWidgets.QLabel("Topic:", broker_grp)
        self.topic_line = QtWidgets.QLineEdit(broker_grp)
        self.topic_line.setPlaceholderText("Subscribe Topic")
        self.topic_line.setText(topic_name)

        # user name
        user_label = QtWidgets.QLabel("User name:", broker_grp)
        self.user_line = QtWidgets.QLineEdit(broker_grp)
        self.user_line.setPlaceholderText("User name")
        self.user_line.setText(user_name)

        # password
        pass_label = QtWidgets.QLabel("Password:", broker_grp)
        self.pass_line = QtWidgets.QLineEdit(broker_grp,
                                             echoMode=QtWidgets.QLineEdit.Password)
        self.pass_line.setPlaceholderText("Password")
        self.pass_line.setText(password)

        # client name
        client_label = QtWidgets.QLabel("Client Name:", broker_grp)
        self.client_line = QtWidgets.QLineEdit(broker_grp)
        self.client_line.setPlaceholderText("Client name")
        self.client_line.setText(client_name)

        # grid layout
        broker_grp_layout = QtWidgets.QGridLayout(broker_grp)
        broker_grp_layout.addWidget(ip_label, 0, 0)
        broker_grp_layout.addWidget(self.ip_line, 0, 1)

        broker_grp_layout.addWidget(port_label, 1, 0)
        broker_grp_layout.addWidget(self.port_line, 1, 1)

        broker_grp_layout.addWidget(topic_label, 2, 0)
        broker_grp_layout.addWidget(self.topic_line, 2, 1)

        broker_grp_layout.addWidget(user_label, 3, 0)
        broker_grp_layout.addWidget(self.user_line, 3, 1)

        broker_grp_layout.addWidget(pass_label, 4, 0)
        broker_grp_layout.addWidget(self.pass_line, 4, 1)

        broker_grp_layout.addWidget(client_label, 5, 0)
        broker_grp_layout.addWidget(self.client_line, 5, 1)

        relay_grp.setLayout(broker_grp_layout)

    # top layout
        parent_layout = QtWidgets.QVBoxLayout(self)
        parent_layout.addWidget(relay_grp)
        parent_layout.addWidget(broker_grp)

        self.setLayout(parent_layout)
        self.resize(parent_layout.sizeHint())


    def get_default_values(self):
        usr_input = dict()
        res_path = os.path.dirname(rs.__file__)
        try:
            with open(os.path.join(res_path, "context.json")) as f:
                usr_input = json.load(f)
        except FileExistsError:
            print("file not found!")
        return usr_input

    def get_values(self):
        output = {'relay':  {'producer_token': self.relay_prod_line.text(),
                             'Ingress_url': self.ingress_prod_line.text()
                             },
                  'broker': {'ip_address': self.ip_line.text(),
                             'port_num': int(self.port_line.text()),
                             'topic_name': self.topic_line.text(),
                             'user_name': self.user_line.text(),
                             'password': self.pass_line.text(),
                             'client_name': self.client_line.text()
                             }
                  }
        self.result.signal.emit(output)
