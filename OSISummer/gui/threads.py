"""
Created  by rtasooji on 8/4/2018
"""
import time
import json
import datetime
from rx import Observable, Observer

from OSISummer.mqttReceiver.subscriber import MqttSub
from OSISummer.analysis.electric.knn import PredictDevice
from OSISummer.analysis.forecast import Forecast, DataSet, ForecastHandling
import mqttReceiver.resources as rs
import os
import PySide2.QtCore as QtCore


class MqttThread(QtCore.QThread):
    def __init__(self, input_value: dict):
        QtCore.QThread.__init__(self)
        self.inputs = input_value
        self.loop = True
        self.subscriber = self.make_listener(self.inputs)

    def run(self, *args, **kwargs):
        self.subscriber.client.loop_start()
        self.start_listener(self.subscriber)

    def __del__(self):
        self.loop = False
        self.subscriber.client.loop_stop()
        self.subscriber.client.disconnect()
        self.terminate()




    @classmethod
    def get_values(cls) -> dict:
        """
        Get values from the context.json file in the resources folder in
         mqttReceive package
        :return: relay and broker inputs
        """
        usr_input = dict()
        res_path = os.path.dirname(rs.__file__)
        try:
            with open(os.path.join(res_path, "context.json")) as f:
                usr_input = json.load(f)
        except FileExistsError:
            print("file not found!")
        return usr_input

    def make_listener(self, user_inputs: dict):
        """
        Creates mqtt subscriber and send messages to the PI system
        :param user_inputs: **kwargs
        :return:
        """
        # the sender is required by the receiver to send the queue
        # set the values for starting mqtt subscriber
        client_name = user_inputs['broker']['client_name']
        ip_address = user_inputs['broker']['ip_address']
        port_num = user_inputs['broker']['port_num']
        topic_name = user_inputs['broker']['topic_name']
        user_name = user_inputs['broker']['user_name']
        password = user_inputs['broker']['password']
        # waiting_time = user_inputs['broker']['waiting_time']
        # start subscriber
        return MqttSub(client_name,
                       ip_address,
                       port_num,
                       topic_name,
                       user=user_name,
                       password=password,
                       qos=1,
                       clean_ses=False)

    def start_listener(self, f):

        try:
            while self.loop:
                if f.connected is False:
                    time.sleep(0.5)
                    print("not connected")
                else:
                    time.sleep(0.000005)
        except KeyboardInterrupt:
            print("No message received in %s second. Closing the "
                  "subscriber." % waiting_time)


class KnnThread(QtCore.QThread):

    class KnnObserver(Observer):
        def __init__(self, server, database, element_location):
            self.device_feature = {"grp": "", "name": "", "timeStamp": list(),
                                   "ph": list(), "power": list(), "reactPower": list(),
                                   "rmsCur": list(), "rmsVolt": list(), "freq": list()}
            self._predict_device = PredictDevice(server, database, element_location)

        def on_next(self, value):
            print("here in on_next %s" % value)
            # self.predictor(self._predict_device, self.device_feature)
        def on_completed(self):
            self.dispose()
        def on_error(self, error):
            pass

        # TODO: the input_device for the object needs to get updated
        def predictor(self, input_pre: PredictDevice, input_device):
            time_interval = datetime.datetime.now() - self.start_time
            while time_interval <= input_pre.dataset_interval:
                dataset = input_pre.get_training_cumulative(10)
                dataframe = input_pre.get_dataframe(dataset)
                #print(dataframe)
                # TODO: on real device check this
                # result = input_pre.predict_device()
                # input_pre.plot_values('grp', dataframe)

                input_pre.counter += 1
                #print(self._predict_device)

    def __init__(self, server, database, element_location, delta=10):
        QtCore.QThread.__init__(self)
        self.server = server
        self.database = database
        self.element_loc = element_location
        self.device_feature = {"grp": "", "name": "", "timeStamp": list(),
                               "ph": list(), "power": list(), "reactPower": list(),
                               "rmsCur": list(), "rmsVolt": list(), "freq": list()}
        self._predict_device = PredictDevice(server, database, element_location)
        self.start_time = None
        self._obs = Observable.interval(datetime.timedelta(seconds=delta))
        self.observer = self.KnnObserver(server, database, element_location)

    def __del__(self):
        self._obs.dispose()
        self._obs = None
        self._predict_device = None
        self.wait()
        self.exit()

    # when the plug status is on store time
    # every 10 sec predict device
    # after 7 minute the device needs to located to its group
    def run(self):
        self.start_time = datetime.datetime.now()

        # subscribe to function with fixed time interval
        self._obs = self._obs.subscribe(lambda x: self.predictor(self._predict_device,
                                        self.device_feature))

    # TODO: the input_device for the object needs to get updated
    def predictor(self, input_pre: PredictDevice, input_device):
        time_interval = datetime.datetime.now() - self.start_time
        while time_interval <= input_pre.dataset_interval:
            dataset = input_pre.get_training_cumulative(10)
            dataframe = input_pre.get_dataframe(dataset)
            # print(dataframe)
            # TODO: on real device check this
            # result = input_pre.predict_device()
            # input_pre.plot_values('grp', dataframe)

            input_pre.counter += 1
            # print(self._predict_device)


class ForeCastThread(QtCore.QThread):

    def __init__(self, y, timestamp):

        self.y = y
        self.timestamp = timestamp
        self.prophet = Forecast(self.y, self.timestamp)
        super(ForeCastThread, self).__init__()

    def run(self, *args, **kwargs):
        self.make_forecast()

    def __del__(self):

        self.exit()

    def make_forecast(self):
        p = self.prophet.prophet()
        future = self.prophet.future(p, period=5, freq='S')
        self.prophet.forecast(p, future)


class PeriodicForeCastThread(QtCore.QThread):

    # the y and timestamp are retrieved from PI system
    def __init__(self, server, database, element, start, end, period, freq, delta=600):
        self.prophet = Forecast()
        self.s_time = start
        self.end = end
        self.data = DataSet()
        self.element = self.data.get_element(server, database, element)
        self.period = period
        self.freq = freq

        self._obs = Observable.interval(datetime.timedelta(seconds=delta))
        super(PeriodicForeCastThread, self).__init__()

    def __del__(self):
        self._obs.dispose()
        self._obs = None
        self.wait()
        self.exit()

    def run(self, *args, **kwargs):
        self.forecast()
        self._obs = self._obs.subscribe(lambda x: self.forecast())

    def forecast(self):
        # check send_smtp, if it is False, check time, if time is greater than
        # 4 am make send_smtp to True
        # if send_smtp is True and time is greater than 7 pm set send_smtp to False

        # if not ForecastHandling.send_smtp and
        y, timestamp = self.data.get_data(self.element, self.s_time, self.end)
        p = self.prophet.prophet(y, timestamp)

        future = self.prophet.future(p, self.period, self.freq)

        # this will signal prophet and dataframe to be displayed
        self.prophet.forecast(p, future)
