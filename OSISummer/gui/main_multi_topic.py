"""
Created  by rtasooji on 7/30/2018
"""
# Python packages
import sys
sys.path.append('C:\\Users\\Administrator\\Desktop\\python\\futurehaus')
import time


# project packages
from OSISummer.omf_sender import SendOMF
import OSISummer.gui.threads as thread
from OSISummer.gui.server_info import ServerInfo
from OSISummer.gui.receiver_info import ReceiverInfo
from OSISummer.gui.forecast_info import ForecastInfo
from OSISummer.gui.plot_widget import PlotWindow
from OSISummer.gui.plots import ForeCastCanvas
from OSISummer.gui.topic_list import TopicSelect
import OSISummer.mqttReceiver.mqttMessage as mqttMessage
import OSISummer.mqttReceiver.queueParser as parser

import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore
from PySide2.QtGui import QIcon, QFont


class InputData(object):
    plot_data = dict()
    plot_time = dict()

    forecast_data = dict()
    forecast_time = dict()

    predict_data = dict()


class MyMenu(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.messages = dict()
        self.user_input = dict()
        self.get_thread = None
        self.knn_thread = None
        self.forecast_thread = None
        self.per_forecast_thread = None
        self.server_info = ServerInfo()
        self.forecast_info = ForecastInfo()
        self.receiver_view = ReceiverInfo(self)

        self.window_list = list()
        self.send_omg_act = QtWidgets.QAction('&Store OMF in PI', self, checkable=True)
        self.omf_sender = None
        self.send_omf = True

        self.main_widget = QtWidgets.QWidget(self)

        self.initUI()

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.add_topics)
        self.timer.start(2000)  # update input arrays every 2 seconds

        self.memory_cleaner_timer = QtCore.QTimer(self)
        self.memory_cleaner_timer.timeout.connect(self.clear_memeory)
        self.memory_cleaner_timer.start(600000)  # clean the memory every 10 min 600000 milsec

        self.main_widget.setFocus()
        self.setCentralWidget(self.receiver_view)

        self.show()


    def initUI(self):

        QtWidgets.QToolTip.setFont(QFont('SansSerif', 10))

        self.statusBar = self.statusBar()
        self.statusBar.showMessage('Ready')

    # Menu actions
        # mqtt subscriber button
        run_listner_act = QtWidgets.QAction(QIcon('web.png'), '&Run', self)
        run_listner_act.setShortcut('Ctrl+S')
        run_listner_act.setStatusTip('Start MQTT Subscriber')
        run_listner_act.triggered.connect(self.mqtt_subscribe)

        # sendOMF button
        self.send_omg_act.setChecked(self.send_omf)
        self.send_omg_act.setStatusTip('Convert MQTT TO OMF and send to PI System')
        self.send_omg_act.triggered.connect(self.call_send_omf)

        # close button
        exit_act = QtWidgets.QAction('&Quit', self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip('Quit Application')
        exit_act.triggered.connect(self.closeEvent)

    # Analytics Action
        # predict electric device
        knn_act = QtWidgets.QAction('&Predict Device', self)
        knn_act.setStatusTip('Using ASC-F2 database predict the device every 10 seconds')
        knn_act.triggered.connect(self.get_server_info)

        # showplot
        showplot_act = QtWidgets.QAction('&Show plot', self)
        showplot_act.setStatusTip('Plot input')
        showplot_act.triggered.connect(self.select_topic)

        # forecast
        forecast_act = QtWidgets.QAction('&Forecast', self)
        forecast_act.setStatusTip('Forecast')
        forecast_act.triggered.connect(self.forecast)

        # periodic_forecast
        periodic_forecast_act = QtWidgets.QAction('&Periodic Forecast', self)
        periodic_forecast_act.setStatusTip('Forecast periodically')
        periodic_forecast_act.triggered.connect(self.get_forecast_info)



    # # Main window buttons
    #
    #     btn = QtWidgets.QPushButton('Iframe', self)
    #     btn.setToolTip("show web based content")
    #     btn.resize(btn.sizeHint())
    #     btn.clicked.connect(self.show_web)
    #     btn.move(50, 50)


    # Menu bar tabs
        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu('&File')

        analytic_menu = menu_bar.addMenu('&Analysis')

    # add action to menu bar
        # file tab
        file_menu.addAction(run_listner_act)
        file_menu.addAction(self.send_omg_act)
        file_menu.addAction(exit_act)

        # analysis tab
        analytic_menu.addAction(knn_act)
        analytic_menu.addAction(forecast_act)
        analytic_menu.addAction(showplot_act)
        analytic_menu.addAction(periodic_forecast_act)

        # connect signals
        self.receiver_view.result.signal.connect(self.get_value)


        # set window size
        self.resize(self.receiver_view.sizeHint())

    def get_value(self, inputs: dict):
        self.user_input = inputs

    def show_web(self):
        browser = IFrameWindow("http://google.com")
        browser.load(browser.url)
        browser.setGeometry(QtCore.QRect(100, 100, 400, 400))
        browser.show()
        self.window_list.append(browser)

    def select_topic(self):
        topic_list = TopicSelect()
        topic_list.init_UI(self.messages.keys())
        topic_list.show()
        topic_list.result.signal.connect(self.show_plot)
        self.window_list.append(topic_list)

    def show_plot(self, value):
        sc = PlotWindow(InputData.plot_data[value], InputData.plot_time[value], value)
        # TODO: fix empty_plot
        sc.result.signal.connect(self.empty_plot)
        sc.show()
        self.window_list.append(sc)

    def empty_plot(self, topic):
        InputData.plot_data[topic].clear()
        InputData.plot_time[topic].clear()

    def clear_memeory(self):
        for value in InputData.plot_data.values():
            value.clear()
        for value in InputData.plot_time.values():
            value.clear()


    def add_topics(self):
        # TODO: Explore ways to deal with analyzing multiple topics

        for k, v in self.messages.items():
            print(k)
            while not v.payload_queue.empty():
                payload = v.payload_queue.get()
                if k not in InputData.plot_data.keys():
                    # create empty list
                    InputData.plot_data[k] = []
                    InputData.plot_time[k] = []
                    InputData.forecast_data[k] = []
                    InputData.forecast_time[k] = []

                    # append value to the list
                    InputData.plot_data[k].append(payload['value'])
                    InputData.plot_time[k].append(payload['time'])
                    InputData.forecast_data[k].append(payload['value'])
                    InputData.forecast_time[k].append(payload['time'])
                else:
                    # append value to the list
                    InputData.plot_data[k].append(payload['value'])
                    InputData.plot_time[k].append(payload['time'])

                    InputData.forecast_data[k].append(payload['value'])
                    InputData.forecast_time[k].append(payload['time'])




    def call_send_omf(self, event):
        if self.send_omf:
            reply = QtWidgets.QMessageBox.question(self, 'Message',
                                                   "Stop storing data?",
                                                   QtWidgets.QMessageBox.Yes |
                                                   QtWidgets.QMessageBox.No,
                                                   QtWidgets.QMessageBox.No)
            if reply == QtWidgets.QMessageBox.Yes:
                self.send_omg_act.setChecked(False)
                # stop receiving point and close sender
                self.send_omf = False
                if self.omf_sender is not None:
                    self.omf_sender.sender.close()
            else:
                self.send_omg_act.setChecked(True)
        else:
            self.send_omf = True

    def mqtt_subscribe(self):
        if self.send_omg_act.isChecked():
            # create and send type message to omg
            self.omf_sender = SendOMF()

        if self.get_thread is None:
            self.receiver_view.get_values()
            thread_start = time.time()
            while not self.user_input:
                if time.time() - thread_start > 3:
                    break
                continue
            self.get_thread = thread.MqttThread(self.user_input)

            self.connect(self.get_thread.subscriber.signal,
                         QtCore.SIGNAL('message(PyObject)'), self.mqtt_to_omf)

            self.connect(self.get_thread.subscriber.signal,
                         QtCore.SIGNAL('message(PyObject)'), self.show_message)

            self.connect(self.get_thread, QtCore.SIGNAL("finished()"), self.done)
            self.get_thread.start()
        else:
            self.setStatusTip("The thread is running")
            pass

    def get_server_info(self):
        if self.knn_thread is None:
            self.server_info.show()
            self.server_info.result.signal.connect(self.knn_analytics)
        else:
            reply = QtWidgets.QMessageBox.question(self, 'Message',
                                         "KNN is running, stop the process?",
                                                   QtWidgets.QMessageBox.Yes |
                                                   QtWidgets.QMessageBox.No,
                                                   QtWidgets.QMessageBox.No)
            if reply == QtWidgets.QMessageBox.Yes:
                self.knn_thread.__del__()
                self.knn_thread = None
            else:
                pass

    def knn_analytics(self, server_info):
        """
        "pop up dialog with default value  and make KnnThread base on that"
        :return:
        """
        server = "RTASOOJI7480"
        database = "test"
        element = "UCSF2\\UFL"
        print("thread start")
        if self.knn_thread is None:
            self.knn_thread = thread.KnnThread(server_info[0], server_info[1],
                                               server_info[2])
            # connect the signals if needed
            self.knn_thread.start()
        else:
            self.setStatusTip("knn is running")
            pass

    def get_forecast_info(self):
        if self.per_forecast_thread is None:
            self.forecast_info.show()
            self.forecast_info.result.signal.connect(self.periodic_forecast)
        else:
            reply = QtWidgets.QMessageBox.question(self, 'Message',
                                         "Forecast is running, stop the process?",
                                                   QtWidgets.QMessageBox.Yes |
                                                   QtWidgets.QMessageBox.No,
                                                   QtWidgets.QMessageBox.No)
            if reply == QtWidgets.QMessageBox.Yes:
                self.forecast_thread.__del__()
                self.forecast_thread = None
            else:
                pass

    def periodic_forecast(self, result: dict):
        if self.per_forecast_thread is None:
            print(result)
            self.per_forecast_thread = thread.PeriodicForeCastThread(server=result['server'], database=result['database'],
                                                                     element=result['element'], start=result['start'],
                                                                     end=result['end'], period=int(result['period']),
                                                                     freq=result['freq'])
            self.per_forecast_thread.prophet.result.signal.connect(self.show_forecast)
            self.per_forecast_thread.start()

        else:
            self.setStatusTip("forecasting is running")

    def forecast(self):
        print("start forecasting")
        if self.forecast_thread is None:
            self.forecast_thread = thread.ForeCastThread(InputData.forecast_data,
                                                         InputData.forecast_time)
            self.forecast_thread.prophet.result.signal.connect(self.show_forecast)
            self.forecast_thread.start()
            # self.connect(self.forecast_thread.prophet.result.signal,
            #              QtCore.SIGNAL('message(PyObject)'), self.show_forecast)
        else:
            self.setStatusTip("forecasting is running")

    def show_forecast(self, result: list):
        fc = ForeCastCanvas(prophet=result[0], df=result[1])
        fc.show()
        self.window_list.append(fc)
        self.forecast_thread.__del__()
        self.forecast_thread = None

    def show_message(self, message: []):
        messages = parser.parse_input(message[0], message[1])
        for msg in messages:
            topic = msg[0]
            payload = msg[1]
            if topic not in self.messages.keys():
                message = mqttMessage.Messages(topic)
                message.insert_values(payload)
                self.messages[topic] = message
            else:
                message = self.messages[topic]
                message.insert_values(payload)
                self.messages[topic] = message

            print("receiving message")

    def mqtt_to_omf(self, message: []):
        # if receiving value and send OMF is checked send the message to OMF
        if self.send_omf:
            if self.omf_sender is not None:
                messages = parser.parse_input(message[0], message[1])
                for msg in messages:
                    self.omf_sender.sender.validate_message(msg[0], msg[1])
        # show received value:

    def done(self):
        self.get_thread.wait()
        QtWidgets.QMessageBox.information(self, "Done!", "All threads are stopped!")

    def closeEvent(self, event):
        for i in range(len(self.window_list)):
            if isinstance(self.window_list[i], QtWidgets.QWidget) or \
               isinstance(self.window_list[i], QtWidgets.QMainWindow):
                try:
                    self.window_list[i].close()
                except:
                    print("window already deleted")
                finally:
                    self.window_list[i] = None
        self.window_list.clear()
        print(type(event))
        print("closing window")
        if self.get_thread is not None:
            self.disconnect(self.get_thread.subscriber.signal,
                         QtCore.SIGNAL('message(PyObject)'), self.mqtt_to_omf)
            self.disconnect(self.get_thread.subscriber.signal,
                         QtCore.SIGNAL('message(PyObject)'), self.show_message)
            self.get_thread.__del__()
            # TODO:
            # This does not include other queues, such as containers or __links.
            # Current logic is: If there is no data body type, there is no need
            # to update PI system.
            # If catching system added to current implementation, this needs to be
            # fixed.
            if self.omf_sender is not None:
                self.omf_sender.sender.close()

        self.close()
        print("window closed")


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    win = MyMenu()
    sys.exit(app.exec_())

