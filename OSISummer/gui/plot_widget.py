"""
Created  by rtasooji on 8/6/2018
"""
import sys
import numpy as np
from fbprophet import Prophet
import pandas as pd
from OSISummer.gui.plots import (SequenceCanvas, HistCanvas, NormalPbPlot,
                                 ForeCastCanvas, BoxPlotCanvas)
import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore


class TopicName(QtCore.QObject):
    signal = QtCore.Signal(str)

    def __init__(self):
        super(TopicName, self).__init__()


class PlotWindow(QtWidgets.QMainWindow):

    def __init__(self, value, time, topic):
        super().__init__()
        self.data = value
        self.time = time
        self.topic = topic
        self.result = TopicName()

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Plotting incoming data")

        self.file_menu = QtWidgets.QMenu('&File', self)

        self.file_menu.addAction('&Quit', self.fileQuit)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)

        self.grp_1 = QtWidgets.QGroupBox(self.main_widget)
        self.grp_2 = QtWidgets.QGroupBox(self.main_widget)
        self.grp_3 = QtWidgets.QGroupBox(self.main_widget)

        box_layout_top = QtWidgets.QHBoxLayout(self.grp_1)
        box_layout_bot = QtWidgets.QHBoxLayout(self.grp_2)
        box_layout_clr = QtWidgets.QHBoxLayout(self.grp_3)

        sc = SequenceCanvas(parent=self.grp_1, input_x=self.time, input_y=self.data,
                            width=5, height=5, dpi=100)
        hc = HistCanvas(parent=self.grp_1, input_x=self.data,  bins=10,
                        width=5, height=5, dpi=100)
        box_layout_top.addWidget(sc)
        box_layout_top.addWidget(hc)

        dc = BoxPlotCanvas(x_input=self.data, parent=self.grp_2,
                           width=5, height=5, dpi=100)
        npc = NormalPbPlot(x_input=self.data)

        clear_btn = QtWidgets.QPushButton("Clear plots", self.grp_3)
        clear_btn.setToolTip("Clear the data in plots")
        clear_btn.resize(clear_btn.sizeHint())
        clear_btn.clicked.connect(self.clear_plot)

        box_layout_bot.addWidget(dc)
        box_layout_bot.addWidget(npc)
        box_layout_clr.addWidget(clear_btn)

        top_layout = QtWidgets.QVBoxLayout(self.main_widget)
        top_layout.addWidget(self.grp_1)
        top_layout.addWidget(self.grp_2)
        top_layout.addWidget(self.grp_3)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox().about(self, "About",
                            """
                            Plots gets updated every 5 seconds.
                            """)

    def clear_plot(self):
        self.result.signal.emit(self.topic)
