## Learnig Notes:

*   make the class of signal as a sub class of the widget that needs to 
    emit signal

*   use QtCore.SIGNAL('PyObject') to make communication 

*   Use the sub class signal to emit and connect between widgets

*   between threads QObject format works

*   If it is not possible to type on the main window, use setCentralWidget and
    use the widger that needs keyboard input. 
    
    