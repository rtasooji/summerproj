"""
from :
https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html
changed to PySide2
axex class:
https://matplotlib.org/api/axes_api.html#axis-labels-title-and-legend
"""
from __future__ import unicode_literals
from scipy.stats import probplot
import pandas as pd
from fbprophet import Prophet
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (
        FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar)

from PySide2.QtWidgets import QSizePolicy
from PySide2.QtCore import Qt, QTimer


class MplCanvas(FigureCanvas):
    """This is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100, t=3000):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)

        self.compute_initial_figure()

        super().__init__(figure=self.fig)

        self.setParent(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_figure)
        self.timer.start(t)

    def compute_initial_figure(self):
        pass

    def update_figure(self):
        pass


class SequenceCanvas(MplCanvas):
    def __init__(self, input_x, input_y, *args, **kwargs):
        self.x = input_x
        self.y = input_y
        self.ready = False
        self.check_size(len(self.x), 2)
        super().__init__(*args, **kwargs)

    def compute_initial_figure(self):
        if self.ready:
            self.axes.set_title("Sequence")
            self.calculate_ticker()
            self.axes.tick_params(axis='x', rotation=90)
            self.axes.plot(self.x, self.y)

    def update_figure(self):
        self.axes.cla()
        self.check_size(len(self.x), 2)
        self.compute_initial_figure()
        self.draw()

    def calculate_ticker(self):
        if self.ready:
            steps = int(len(self.x)/5)
            tick = np.arange(0, len(self.x), steps)
            self.axes.set_xticks(ticks=tick)

    def check_size(self, size, threshold=2):
        if size > threshold:
            self.ready = True
        else:
            self.ready = False


class NormalPbPlot(MplCanvas):
    def __init__(self, x_input, *args, **kwargs):
        self.x = x_input
        self.ready = False
        self.check_size(len(self.x), 2)
        super().__init__(*args, **kwargs)

    def compute_initial_figure(self):
        if self.ready:
            probplot(self.x, plot=self.axes)

    def update_figure(self):
        self.axes.clear()
        self.check_size(len(self.x), 2)
        self.compute_initial_figure()
        self.draw()

    def check_size(self, size, threshold=2):
        if size > threshold:
            self.ready = True
        else:
            self.ready = False


class HistCanvas(MplCanvas):
    def __init__(self, input_x, bins, *args, **kwargs):
        self.x = input_x
        self.bins = bins
        super().__init__(*args, **kwargs)

    def compute_initial_figure(self):
        self.axes.set_title("Histogram")
        self.axes.hist(self.x, self.bins)

    def update_figure(self):
        self.axes.cla()
        self.compute_initial_figure()
        self.draw()


class LagCanvas(MplCanvas):
    def __init__(self):
        super().__init__()


class BoxPlotCanvas(MplCanvas):
    def __init__(self, x_input, *args, **kwargs):
        self.x = x_input
        self.ready = False
        super().__init__(*args, **kwargs)

    def compute_initial_figure(self):
        if self.ready:
            self.axes.set_title("BoxPlot")
            self.axes.boxplot(self.x)

    def update_figure(self):
        self.axes.cla()
        self.axes.set_title("BoxPlot")
        self.axes.boxplot(self.x)
        self.draw()


class ForeCastCanvas(MplCanvas):
    def __init__(self, df: pd.DataFrame, prophet: Prophet, *args, **kwargs):
        self.dataframe = df
        self.prophet = prophet
        super().__init__(*args, **kwargs)

    def compute_initial_figure(self):
        """
              def plot(
                m, fcst, ax=None, uncertainty=True, plot_cap=True, xlabel='ds', ylabel='y',
        )
        :return:
        """

        self.fig = Figure(facecolor='w', figsize=(10, 6))
        self.axes = self.fig.add_subplot(111)

        fcst_t = self.dataframe['ds'].dt.to_pydatetime()
        self.axes.plot(self.prophet.history['ds'].dt.to_pydatetime(),
                       self.prophet.history['y'], 'k.')
        self.axes.plot(fcst_t, self.dataframe['yhat'], ls='-', c='#0072B2')
        if 'cap' in self.dataframe:
            self.axes.plot(fcst_t, self.dataframe['cap'], ls='--', c='k')
        if self.prophet.logistic_floor and 'floor' in self.dataframe:
            self.axes.plot(fcst_t, self.dataframe['floor'], ls='--', c='k')
        self.axes.grid(True, which='major', c='gray', ls='-', lw=1, alpha=0.2)
        self.axes.set_xlabel('ds')
        self.axes.set_ylabel('y')

