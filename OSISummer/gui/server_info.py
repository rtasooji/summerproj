"""
Created  by rtasooji on 8/3/2018
"""
import sys
import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore


class MessageResult(QtCore.QObject):
    signal = QtCore.Signal(list)

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        super().__init__()


class ServerInfo(QtWidgets.QWidget):
    def __init__(self):
        super(ServerInfo, self).__init__()
        top_grp = QtWidgets.QGroupBox("&Server Info", self)
        self.result = MessageResult()

    # inputs
        # server input
        server_label = QtWidgets.QLabel("Server:", top_grp)
        self.server_line = QtWidgets.QLineEdit(top_grp)
        self.server_line.setText("futurehaus")
        self.server_line.setPlaceholderText("Server name")
        self.server_line.setFocus()

        # database input
        database_label = QtWidgets.QLabel("Database:", top_grp)
        self.database_line = QtWidgets.QLineEdit(top_grp)
        self.database_line.setText("futurehaus")
        self.database_line.setPlaceholderText("database name")

        # element input
        element_label = QtWidgets.QLabel("Element:", top_grp)
        self.element_line = QtWidgets.QLineEdit(top_grp)
        self.element_line.setText("database\\UFL")
        self.element_line.setPlaceholderText("Element location")

        # buttons
        ok_but = QtWidgets.QPushButton("Ok", top_grp)

        cancel_but = QtWidgets.QPushButton("Cancel", top_grp)

        # connect signals
        cancel_but.clicked.connect(self.cancel)
        ok_but.clicked.connect(self.ok)

        # grid layout
        grid_layout = QtWidgets.QGridLayout(top_grp)
        grid_layout.addWidget(server_label, 0, 0)
        grid_layout.addWidget(self.server_line,  0, 1)

        grid_layout.addWidget(database_label, 1, 0)
        grid_layout.addWidget(self.database_line,  1, 1)

        grid_layout.addWidget(element_label, 2, 0)
        grid_layout.addWidget(self.element_line,  2, 1)

        grid_layout.addWidget(ok_but, 3, 0)
        grid_layout.addWidget(cancel_but,  3, 1)

        top_grp.setLayout(grid_layout)

        self.setFocus()
        self.resize(top_grp.sizeHint())



    def cancel(self):
        self.close()

    def ok(self):
        server = self.server_line.text()
        database = self.database_line.text()
        element = self.element_line.text()
        result = [server, database, element]
        self.result.signal.emit(result)
        self.close()



        # QGroupBxox
        # Qlabel
        # qlineedit
        # qgridlayout
        #


# server = "RTASOOJI7480"
# database = "test"
# element = "UCSF2\\UFL"

# if __name__ == '__main__':
#     app = QtWidgets.QApplication(sys.argv)
#     win = ServerInfo()
#     win.show()
#     sys.exit(app.exec_())
