import unittest
import json
from OSISummer.mqttReceiver.mqttMessage import Messages
import datetime


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.message = Messages(topic="OSI/test")

    def test_messages(self):
        value = 10
        time = str(datetime.datetime.utcnow())
        desc = "test"
        status = "On"
        values = {'Timestamp': time,
                  'Value': value,
                  'Description': desc,
                  'Status': status}
        payload = json.dumps(values)
        self.message.insert_values(payload)
        payloads = self.message.payload_queue.get()
        time = payloads['time']
        self.assertEqual([value, time, status, desc],
                         [payloads['value'], payloads['time'],
                          payloads['status'], payloads['desc']])


if __name__ == '__main__':
    unittest.main()
