import unittest
from mqttReceiver.mqttQueue import MqttQueue
from omf.messages.all import Messages
import sys


class MyTestCase(unittest.TestCase):



    def test_MQTTQueue(self):
        q1 = MqttQueue()
        size1 = sys.getsizeof(" ")  # 50 bytes
        print(size1)
        q1.put(" ")
        q1.put(" ")
        q1.put(" ")
        q1.put(" ")
        q1.put(" ")
        self.assertEqual(size1 * 5, q1.curr_size)
        q1.get()
        self.assertEqual(size1 * 4, q1.curr_size)
        q1.put_nowait(" ")
        self.assertEqual(size1 * 5, q1.curr_size)
        while not q1.empty():
            q1.get_nowait()
        q1.put_nowait("1")
        q1.put_nowait("2")
        q1.put_nowait("3")
        q1_list = list()
        while not q1.empty():
            q1_list.append(q1.get_nowait())
        self.assertEqual(["1", "2", "3"], q1_list)
        q1.put_nowait((1, "value", 30, 40, 50))  # (method to call, type, data to send)
        v1 = q1.get_nowait()
        self.assertEqual(1, v1[0])
        self.assertEqual("value", v1[1])
        self.assertEqual(30, v1[2])
        self.assertEqual((30, 40, 50), v1[2:])

    def test_Messages(self):
        q2 = MqttQueue()
        q2.put(("container", 0, "topic"))  # create_container
        q2.put(('data', 1, "asset", "element"))  # create_asset_data
        q2.put(('data', 2, "source", "target"))  # create_asset_link
        q2.put(("data", 3, "topic1", "topic2"))  # create_asset_link
        q2.put(("data", 4, "topic",
                "data", "time",
                "status", "desc"))  # data container
        curr_queue = q2.get()
        output = Messages.call_method(curr_queue[1], curr_queue[2:])
        expected = [{
            "id": "topic",
            "typeid": "SecondDynamicType"
        }]
        self.assertEqual(expected, output)
        curr_queue = q2.get()
        output = Messages.call_method(curr_queue[1], curr_queue[2:])
        expected = [{
            "typeid": "FirstStaticType",
            "values": [{
                "index": "asset",
                "name": "element"
            }]
        }]
        self.assertEqual(expected, output)
        curr_queue = q2.get()
        output = Messages.call_method(curr_queue[1], curr_queue[2:])
        expected = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": "source"
                },
                "target": {
                    "typeid": "FirstStaticType",
                    "index": "target"
                }
            }]
        }]
        self.assertEqual(expected, output)

        curr_queue = q2.get()
        output = Messages.call_method(curr_queue[1], curr_queue[2:])
        expected = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": "topic1"
                },
                "target": {
                    "containerid": "topic2"
                }
            }]
        }]
        self.assertEqual(expected, output)

        curr_queue = q2.get()
        output = Messages.call_method(curr_queue[1], curr_queue[2:])
        expected = [
            {
                "containerid": "topic",
                "values": [
                    {
                        "timestamp": "time",  # needs to be in datetime#
                        "NumberProperty1": "data",
                        "Property2": "time",
                        "StringEnum": "status",
                        "Description": "desc"
                    }
                ]
            }
        ]
        self.assertEqual(expected, output)

        curr_list = list(curr_queue[2:])
        curr_list[2] = "10"
        output = Messages.call_method(curr_queue[1], curr_list)
        expected = [
            {
                "containerid": "topic",
                "values": [
                    {
                        "timestamp": "10",  # needs to be in datetime#
                        "NumberProperty1": "data",
                        "Property2": "10",
                        "StringEnum": "status",
                        "Description": "desc"
                    }
                ]
            }
        ]
        self.assertEqual(expected, output)


if __name__ == '__main__':
    unittest.main()
