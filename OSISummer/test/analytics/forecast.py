"""
Created  by rtasooji on 8/5/2018
"""
import sys
import PySide2.QtWidgets as QtWidgets
from OSISummer.gui.plots import ForeCastCanvas

from fbprophet import Prophet
import pandas as pd


class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.do_prophet()



    def do_prophet(self):
        df = pd.read_csv(
            "C:\\Users\\rtasooji\\Desktop\\python\\facebookProphet\\prophet-master\\examples\\example_wp_log_peyton_manning.csv")
        prophet = Prophet(daily_seasonality=True)
        prophet.fit(df)  # takes 1 to 5 seconds
        future = prophet.make_future_dataframe(periods=365, freq='D')
        forecast = prophet.predict(future)
        fc = ForeCastCanvas(forecast, prophet)
        fc.show()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    win = MyWindow()
    sys.exit(app.exec_())