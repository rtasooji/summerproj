import unittest
from OSISummer.analysis.electric.knn import PredictDevice, DataSet
import datetime


class KnnTestCase(unittest.TestCase):

    def setUp(self):
        # server: RTASOOJI7480
        # database: test
        # element: UCSF2\\UFL
        self.knn_test = PredictDevice("RTASOOJI7480", "test", "UCSF2\\UFL")
        self.start = DataSet.ascf_start
        self.end = DataSet.ascf_end

    def test_knn_all_dataset(self):

        dataset = self.knn_test.get_training_interval(self.start, self.end)
        dataframe = self.knn_test.get_dataframe(dataset)
        y_true, y_pred = self.knn_test.test_classify(dataframe)
        # It is a score of how much homogeneity, or consensus,
        # there is in the ratings given by various judges.
        cohen_score = PredictDevice.get_cohen_kappa_score(y_true, y_pred)
        print("cohen kappa score: {}".format(cohen_score))
        self.assertTrue(0.75 <= cohen_score <= 0.80)

    @staticmethod
    def change_end_time(start_time, delta_time: datetime.datetime.minute):
        delta = datetime.timedelta(minutes=delta_time)
        return start_time + delta

if __name__ == '__main__':
    unittest.main()
