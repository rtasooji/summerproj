"""
Created  by rtasooji on 8/6/2018
"""
import sys

import PySide2.QtWidgets as QtWidgets
import PySide2.QtCore as QtCore

from OSISummer.gui.receiver_info import ReceiverInfo


class TestReceiverInfo(QtWidgets.QMainWindow):
    def __init__(self):
        super(TestReceiverInfo, self).__init__()

        self.initUI()

        self.show()


    def initUI(self):
        rec_view = ReceiverInfo(self)

        self.resize(rec_view.sizeHint())




    def closeEvent(self, *args, **kwargs):
        self.file_quit()

    def file_quit(self):
        self.close()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    win = TestReceiverInfo()
    sys.exit(app.exec_())

