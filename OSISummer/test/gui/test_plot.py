"""
Created  by rtasooji on 8/3/2018
"""
import sys
import numpy as np
from fbprophet import Prophet
import pandas as pd
from OSISummer.gui.plots import (SequenceCanvas, HistCanvas, NormalPbPlot,
                                 DynamicMplCanvas, ForeCastCanvas)
import PySide2.QtWidgets as QtWidgets
from PySide2.QtCore import Qt


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowTitle("Application main window")

        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction('&Prophet', self.do_prophet)
        self.file_menu.addAction('&Quit', self.fileQuit)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)

        self.grp_1 = QtWidgets.QGroupBox(self.main_widget)
        self.grp_2 = QtWidgets.QGroupBox(self.main_widget)

        box_layout_top = QtWidgets.QHBoxLayout(self.grp_1)
        box_layout_bot = QtWidgets.QHBoxLayout(self.grp_2)

        sc = SequenceCanvas(parent=self.grp_1, input_x=range(10), input_y=normal_set,
                            width=5, height=5, dpi=100)
        hc = HistCanvas(parent=self.grp_1, input_x=normal_set,  bins=10,
                            width=5, height=5, dpi=100)
        box_layout_top.addWidget(sc)
        box_layout_top.addWidget(hc)

        dc = DynamicMplCanvas(parent=self.grp_2, width=5, height=5, dpi=100, t=1000)
        npc = NormalPbPlot(x_input=normal_set)

        box_layout_bot.addWidget(dc)
        box_layout_bot.addWidget(npc)

        top_layout = QtWidgets.QVBoxLayout(self.main_widget)
        top_layout.addWidget(self.grp_1)
        top_layout.addWidget(self.grp_2)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        self.statusBar().showMessage("All hail matplot!", 2000)

    def do_prophet(self):
        df = pd.read_csv(
            "C:\\Users\\rtasooji\\Desktop\\python\\facebookProphet\\prophet-master\\examples\\example_wp_log_peyton_manning.csv")
        prophet = Prophet(daily_seasonality=True)
        prophet.fit(df)  # takes 1 to 5 seconds
        future = prophet.make_future_dataframe(periods=365, freq='D')
        forecast = prophet.predict(future)
        fc = ForeCastCanvas(forecast, prophet)
        fc.show()


    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox().about(self, "About",
                            """
                            Test embedding  matplotlib in PySide2
                            """)


if __name__ == '__main__':
    normal_set = np.random.normal(0, 1, 10)
    print(normal_set)
    app = QtWidgets.QApplication(sys.argv)
    win = ApplicationWindow()
    win.show()
    sys.exit(app.exec_())

