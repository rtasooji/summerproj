import unittest
from omf.omfSender import OMFSender
from omf.messages.all import Messages
from mqttReceiver.mqttQueue import MqttQueue
import timeit
import time
import numpy as np
import datetime
import sys


class MyTestCase(unittest.TestCase):

    def setUp(self):

        PRODUCER_TOKEN = "uid=f64332c6-d8f7-4eea-9388-ca888c2318be&crt=20180608192433517&sig=2xN7oKrlUFap6QbEcZFqwtHEbiKzlZGQoK4ALsBQwdQ="

        INGRESS_URL = "https://rtasooji7480.osisoft.int:5460/ingress/messages"
        self.sender = OMFSender(producer_token=PRODUCER_TOKEN,
                           ingress_url=INGRESS_URL)

        self.writer = open("payload_analyze.csv", 'a')


    def tearDown(self):
        pass

    def send_type_sender(self):
        """
        testing sending type body\n
        :return:
        """
        # the type of the message needs to match with the id

        response = self.sender.postMessage("type", Messages.create_type(),
                                           action="create")

        self.assertEqual(204, response.status_code)
        self.assertEqual("", response.text)

    def send_data_container_with_bulking(self, break_point):
        """
        Testing bulking to an array and send an array.\n
        Found with this format it is possible to send 18885 data body with
        compression.\n
        :return:
        """
        counter = 4907
        while counter <= break_point:
            try:
                queue = MqttQueue()
                self.create_container(queue, number=counter, freq=100000)
            #  self.create_container(queue, 19000) # this value will cause payload error
                print("counter %s" % counter)
                print("size of the queue: ", queue.curr_size)
                t0 = timeit.default_timer()
                curr_queue = queue.get()  # gets the left in the queue
                msg_type = curr_queue[0]  # message type
                method_num = curr_queue[1]  # method to call
                values = list(curr_queue[2:])
                msg_body = Messages.call_method(method_num, values)[0]
                value_holder = list()
                value_holder.append(msg_body)
                while not queue.empty():
                    curr_queue = queue.get()  # gets the left in the queue
                    method_num = curr_queue[1]  # method to call
                    values = list(curr_queue[2:])
                    values[2] = self.get_time()
                    msg_body = Messages.call_method(method_num, values)[0]
                    value_holder.append(msg_body.copy())
                    msg_body.clear()
                self.sender.postMessage(msg_type, value_holder, action='create')  # send
                t1 = timeit.default_timer() - t0
                self.writer.write("time intervals: {}\n".format(t1))
                counter += 1
            except Exception:
                print("exception happened in test!")
                break
        self.writer.close()

    def send_container(self):
        """
        testing sending container body\n
        :return:
        """
        queue = MqttQueue()
        queue.put(("container", 0, "_ROOT/F_HAUS/Empatica2/test1"))  # This requires _ROOT/

        curr_queue = queue.get()
        msg_type = curr_queue[0]  # message type
        method_num = curr_queue[1]  # method to call
        values = list(curr_queue[2:])  # values for the method
        msg_body = Messages.call_method(method_num, values)  # make msg_body
        response = self.sender.postMessage(msg_type, msg_body, action='create')  # send
        self.assertEqual(204, response.status_code)

    def test_something(self):
        self.send_type_sender()
        self.send_container()
        self.send_data_container_with_bulking(5000)

    def create_container(self, queue, number=10, freq=100):
        """
        helper for creating queue of messages.
        :param queue: input queue
        :param number: total number of points in the queue
        :param number: sleep time between making data
        :return:
        """
        mean_data_size = list()
        for i in range(number):
            time.sleep(1/freq)
            value_tuple = ("data", 4, "F_HAUS/Empatica2/test1",
                           i, self.get_time(), "On", "TestCase")
            mean_data_size.append(sys.getsizeof(value_tuple))
            queue.put(value_tuple)  # this does not need _ROOT/
        self.writer.write("queueSizeN: %f " % queue.qsize())
        self.writer.write("data_size: %f " % np.mean(mean_data_size))
        self.writer.write("queueSizeB: %f " % queue.curr_size)


    @staticmethod
    def get_time():
        """
        helper for receiving current time.
        :return:
        """
        return datetime.datetime.utcnow().isoformat() + 'Z'
        # return (datetime.datetime.utcnow() + datetime.timedelta(
        #     days=0)).isoformat() + 'Z'


if __name__ == '__main__':
    unittest.main()
