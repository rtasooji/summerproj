import unittest
from OSISummer.omf.omfSender import OMFSender
from OSISummer.omf.omfSender import OMFSender
from OSISummer.mqttReceiver.queueParser import MATE3Parser
from OSISummer.mqttReceiver.translator import Convert
import json
import time


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.sender = OMFSender(producer_token= "uid=a5c7981f-1460-489e-9ea9-502d97300e31&crt=20180902190214666&sig=IZ/GB3Lswfn52MCvayLV18Yg17/jxL6cbpgFwdDFhdA=",
                                ingress_url="https://osisoft:5462/ingress/messages")
        self.topics = "Futurehaus/unitTest/inverter"

    def test_send_mat3(self):
        self.send_omf()

    def send_omf(self):
        converter = Convert(self.sender)
        message = [{"modbus_address": 40461, "name": "Outback_Year", "units": "", "value": 2018},
                   {"modbus_address": 40462, "name": "Outback_Month", "units": "", "value": 8},
                   {"modbus_address": 40463, "name": "Outback_Day", "units": "", "value": 25},
                   {"modbus_address": 40464, "name": "Outback_Hour", "units": "", "value": 15},
                   {"modbus_address": 40465, "name": "Outback_Minute", "units": "", "value": 16},
                   {"modbus_address": 40466, "name": "Outback_Second", "units": "", "value": 55},
                   {"modbus_address": 40475, "name": "OutBack_Measured_System_Voltage", "units": "V", "value": 54},
                   {"modbus_address": 40502, "name": "OB_Set_Sell_Voltage", "units": "V", "value": 48},
                   {"modbus_address": 40503, "name": "OB_Set_Radian_Inverter_Sell_Current_Limit", "units": "A",
                    "value": 9},
                   {"modbus_address": 40549, "name": "I_DC_Voltage", "units": "V", "value": 53.6},
                   {"modbus_address": 41517, "name": "GS_Split_Inverter_Operating_Mode", "units": "", "value": 0},
                   {"modbus_address": 41552, "name": "GS_Split_Sell_kW", "units": "kW", "value": 2.3000000000000003},
                   {"modbus_address": 41655, "name": "FN_Shunt_A_Current", "units": "A", "value": -44.6},
                   {"modbus_address": 41656, "name": "FN_Shunt_B_Current", "units": "A", "value": 0},
                   {"modbus_address": 41657, "name": "FN_Shunt_C_Current", "units": "A", "value": -46.900000000000006},
                   {"modbus_address": 41658, "name": "FN_Battery_Voltage", "units": "V", "value": 54},
                   {"modbus_address": 41659, "name": "FN_Battery_Current", "units": "A", "value": -91.5}]

        json_input = json.dumps(message)
        messages = MATE3Parser.parse_mqtt(self.topics, json_input)
        converter.send_type()
        for msg in messages:
            converter.validate_message(msg[0], msg[1])
        time.sleep(0.3)
        while converter.received_container.curr_size > 0:
            time.sleep(0.3)
            continue


if __name__ == '__main__':
    unittest.main()
