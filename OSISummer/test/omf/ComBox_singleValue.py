import unittest
from OSISummer.omf.omfSender import OMFSender
from OSISummer.mqttReceiver.queueParser import ComBox
from OSISummer.mqttReceiver.translator import Convert
import json
import time

class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.sender = OMFSender(producer_token= "uid=794ae676-62ca-447f-bf4d-2ece2c288501&crt=20180902202651353&sig=0UyDjWOM+a2TIgCe9tdJKCrxuxQDgEl+wK9Yocp0Nr4=",
                                ingress_url="https://osisoft:5462/ingress/messages")
        self.topics = "Futurehaus/unitTest/charger"

    def test_send_mat3(self):
        self.send_omf()

    def send_omf(self):
        converter = Convert(self.sender)
        message = [{"modbus_address": 68,"name": "PV Harvest Power","units":"W","value":7000},
                   {"modbus_address": 70,"name": "DC Charging Power","units":"W","value":0},
                   {"modbus_address": 72,"name": "DC Charging Current","units":"A","value":0},
                   {"modbus_address": 140,"name": "MPPT PV Power","units":"W","value":0},
                   {"modbus_address": 142,"name": "MPPT Battery Current","units":"A","value":0},
                   {"modbus_address": 144,"name": "MPPT Battery Power","units":"W","value":0},
                   {"modbus_address": 152,"name": "Battery Voltage","units":"V","value":0},
                   {"modbus_address": 154,"name": "Battery Temperature","units":"deg C","value":-273},
                   {"modbus_address": 156,"name": "Battery Current Net","units":"A","value":0},
                   {"modbus_address": 344,"name": "Battery Power Net","units":"W","value":0},
                   {"modbus_address": 354,"name": "Average PV Voltage","units":"V","value":46.27},
                   {"modbus_address": 356,"name": "Total PV Current","units":"A","value": 20}]

        json_input = json.dumps(message)
        messages = ComBox.parse_mqtt(self.topics, json_input)
        converter.send_type()
        for msg in messages:
            converter.validate_message(msg[0], msg[1])
        time.sleep(0.3)
        while converter.received_container.curr_size > 0:
            time.sleep(0.3)
            continue
if __name__ == '__main__':
    unittest.main()
