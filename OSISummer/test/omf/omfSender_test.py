"""
Created  by rtasooji on 6/20/2018
"""
import unittest
from OSISummer.omf.omfSender import OMFSender
from OSISummer.omf.messages.all import Messages
from OSISummer.mqttReceiver.mqttQueue import MqttQueue
import datetime
import time
import json
import sys
import numpy as np
import asyncio
from rx import Observable

class OMFSenderTestCase(unittest.TestCase):


    # def test_normal_sending(self):
    #     self.send_type_sender()
    #     self.send_container()
    #     self.send_data_container_no_bulking()
    #
    # def test_bulking(self):
    #     """
    #     First type and then container needs to be sent then it is possible to
    #     send data bodies.
    #     :return:
    #     """
    #     self.send_type_sender()  # essential at the begging to know the type
    #     self.check_direct_post  # essential to set the container
    #     self.send_data_container_with_bulking()

    def setUp(self):
        producer_token = "uid=cba0cace-3629-4ae8-b721-d7036eae2f6e&crt=20180823212849960&sig=pIGjOrrQ5UAWx09/sfp/5DoWrtZ0Z05tzafqDtvUWFc="
        ingress_url = "https://osisoft:5462/ingress/messages"
        self.topics = "test/unitest"
        self.sending_list = list()
        self.sender = OMFSender(producer_token=producer_token, ingress_url=ingress_url)

    def test_empty_queue(self):
        self.send_type_sender()
        self.send_container()
        self.send_asset_data()
        self.send_container_link()
        self.send_data_container_no_bulking()
        #self.empty_array_every_second(10, 1, 100, .5)


    def check_size(self, queue, sender, counter_time=100):
        counter = 0
        value_holder = list()
        print("size of the queue: ", queue.curr_size)
        t0 = time.time()
        if not queue.empty():
            curr_queue = queue.get()  # gets the left in the queue
            msg_type = curr_queue[0]  # message type
            method_num = curr_queue[1]  # method to call
            values = list(curr_queue[2:])
            data = values[1]
            msg_body = Messages.call_method(method_num, values)[0]
            value_holder.append(msg_body)
            while counter < counter_time:
                if not queue.empty():
                    print("size of the queue: ", queue.curr_size)
                    t0 = time.time()
                    curr_queue = queue.get()  # gets the left in the queue
                    msg_type = curr_queue[0]  # message type
                    method_num = curr_queue[1]  # method to call
                    values = list(curr_queue[2:])
                    msg_body = Messages.call_method(method_num, values)[0]
                    value_holder = list()
                    value_holder.append(msg_body)
                    while not queue.empty():
                        curr_queue = queue.get()  # gets the left in the queue
                        method_num = curr_queue[1]  # method to call
                        values = list(curr_queue[2:])
                        msg_body = Messages.call_method(method_num, values)[0]
                        value_holder.append(msg_body.copy())
                        msg_body.clear()
                else:
                    break
            print("size of the array: %f" % self.get_size(value_holder))
            response = sender.postMessage(msg_type, value_holder, action='create')  # send
            t1 = time.time()
            print("time intervals: {}".format(t1 - t0))
            self.assertEqual(204, response.status_code)

    def empty_array_every_second(self, num_points, freq, num_bulk, delta_send):

        queue = MqttQueue()
        counter = num_bulk
        obs = Observable.interval(datetime.timedelta(seconds=delta_send)
                                  ).subscribe(lambda x: self.check_size(
                                              queue,
                                              self.sender,
                                              counter_time=counter))
        self.create_container(queue, num_points, freq=freq)

    def check_direct_post(self):
        msg_topic = "_ROOT/" + self.topics

        msg_body = Messages.call_method(0, (msg_topic, ))
        response = self.sender.postMessage("container", msg_body, action="create")
        self.assertEqual(204, response.status_code)

    def send_type_sender(self):
        """
        testing sending type body\n
        :return:
        """
        # the type of the message needs to match with the id
        response = self.sender.postMessage("type", Messages.create_type(),
                                      action="create")

        self.assertEqual(204, response.status_code)
        self.assertEqual("", response.text)

    def send_container(self):
        """
        testing sending container body\n
        :return:
        """

        queue = MqttQueue()
        queue.put(("container", 0, "_ROOT/" + self.topics))  # This requires _ROOT/

        curr_queue = queue.get()
        msg_type = curr_queue[0]  # message type
        method_num = curr_queue[1]  # method to call
        values = list(curr_queue[2:])  # values for the method
        msg_body = Messages.call_method(method_num, values)  # make msg_body
        response = self.sender.postMessage(msg_type, msg_body, action='create')  # send
        self.assertEqual(204, response.status_code)
        print("container sent.")

    def send_asset_data(self):
        elements = self.topics.split('/')  # list of elements
        for i in range(len(elements)):
            if i == 0:
                topic_element = elements[i]
                asset_index = elements[i]
                source_index = "_ROOT"
                target_index = asset_index
            else:
                topic_element = elements[i]
                prev_asset_index = asset_index
                asset_index = prev_asset_index + "/" + elements[i]
                source_index = prev_asset_index
                target_index = asset_index
            curr_msg = ("data", 1, asset_index, topic_element)
            msg_body = Messages.call_method(curr_msg[1], curr_msg[2:])
            response = self.sender.postMessage(curr_msg[0], msg_body)  # send asset data
            self.assertEqual(204, response.status_code)
            curr_msg = ('data', 2, source_index, target_index)
            msg_body = Messages.call_method(curr_msg[1], curr_msg[2:])
            response = self.sender.postMessage(curr_msg[0], msg_body)  # send asset link
            self.assertEqual(204, response.status_code)

    def send_container_link(self):
        curr_msg = ('data', 3,  self.topics, "_ROOT/" + self.topics)
        msg_body = Messages.call_method(curr_msg[1], curr_msg[2:])
        response = self.sender.postMessage(curr_msg[0], msg_body)
        self.assertEqual(204, response.status_code)

    def send_data_container_no_bulking(self):
        """
        send data container every message takes around 0.7 seconds through local
        network.\n
        Sending this requires sending type and container definition message
        :return:
        """

        queue = MqttQueue()
        self.create_container(queue, 100)
        print(queue.curr_size)

        while not queue.empty():
            t0 = time.time()
            curr_queue = queue.get()  # gets the left in the queue
            msg_type = curr_queue[0]  # message type
            method_num = curr_queue[1]  # method to call
            values = list(curr_queue[2:])  # values for the method
            msg_body = Messages.call_method(method_num, values)  # make msg_body
            response = self.sender.postMessage(msg_type, msg_body, action='create')  # send
            print(response.status_code)
            t1 = time.time()
            print("time intervals: {}".format(t1-t0))


    def send_data_container_with_bulking(self):
        """
        Testing bulking to an array and send an array.\n
        Found with this format it is possible to send 18885 data body with
        compression.\n
        :return:
        """
        queue = MqttQueue()

        self.create_container(queue, number=18885, freq=10000)
        #  self.create_container(queue, 19000) # this value will cause payload error
        print("size of the queue: ", queue.curr_size)
        t0 = time.time()
        curr_queue = queue.get()  # gets the left in the queue
        msg_type = curr_queue[0]  # message type
        method_num = curr_queue[1]  # method to call
        values = list(curr_queue[2:])
        self.assertEqual(0, values[1])
        msg_body = Messages.call_method(method_num, values)[0]
        value_holder = list()
        value_holder.append(msg_body)
        while not queue.empty():
            curr_queue = queue.get()  # gets the left in the queue
            method_num = curr_queue[1]  # method to call
            values = list(curr_queue[2:])
            msg_body = Messages.call_method(method_num, values)[0]
            value_holder.append(msg_body.copy())
            msg_body.clear()
        print("size of the array: %f" % self.get_size(value_holder))
        response = self.sender.postMessage(msg_type, value_holder, action='create')  # send
        t1 = time.time()
        print("time intervals: {}".format(t1-t0))
        self.assertEqual(204, response.status_code)

    @staticmethod
    def get_time():
        """
        helper for receiving current time.
        :return:
        """
        return (datetime.datetime.utcnow() + datetime.timedelta(
            days=0)).isoformat() + 'Z'


    def create_container(self, queue, number=10, freq=100):
        """
        helper for creating queue of messages.
        :param queue: input queue
        :param number: total number of points in the queue
        :param number: sleep time between making data
        :return:
        """
        mean_data_size = list()
        for i in range(number):
            time.sleep(1/freq)
            value_tuple = ("data", 4, "_ROOT/" + self.topics,
                           i, self.get_time(), "On", "TestCase")
            mean_data_size.append(self.get_size(value_tuple))
            queue.put(value_tuple)  # this does not need _ROOT/
        print("data_size: %f" % np.mean(mean_data_size))
        print("queueSize: %f" % queue.curr_size)

    def get_size(self, obj, seen=None):
        """
        The recursive approach for getting the size of an object.\n
        Found the method online:
        https://goshippo.com/blog/measure-real-size-any-python-object/
        :param obj:
        :param seen:
        :return:
        """
        """Recursively finds size of objects"""
        size = sys.getsizeof(obj)
        if seen is None:
            seen = set()
        obj_id = id(obj)
        if obj_id in seen:
            return 0
        # Important mark as seen *before* entering recursion to gracefully handle
        # self-referential objects
        seen.add(obj_id)
        if isinstance(obj, dict):
            size += sum([self.get_size(v, seen) for v in obj.values()])
            size += sum([self.get_size(k, seen) for k in obj.keys()])
        elif hasattr(obj, '__dict__'):
            size += self.get_size(obj.__dict__, seen)
        elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
            size += sum([self.get_size(i, seen) for i in obj])
        return size


if __name__ == '__main__':

    unittest.main()
