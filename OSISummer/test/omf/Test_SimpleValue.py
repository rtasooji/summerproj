import unittest
from OSISummer.omf.omfSender import OMFSender
from OSISummer.mqttReceiver.queueParser import Parser
from OSISummer.mqttReceiver.translator import Convert
import json
import time


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.sender = OMFSender(producer_token= "uid=794ae676-62ca-447f-bf4d-2ece2c288501&crt=20180902202651353&sig=0UyDjWOM+a2TIgCe9tdJKCrxuxQDgEl+wK9Yocp0Nr4=",
                                ingress_url="https://osisoft:5462/ingress/messages")
        self.num_topic = "Futurehaus/unitTest/numValue"
        self.str_topic = "Futurehaus/unitTest/strValue"

    def test_send_mat3(self):
        self.send_num_omf()
        self.send_str_omf()

    def send_num_omf(self):
        converter = Convert(self.sender)
        payload = dict()
        value = 7000
        payload['Value'] = value
        messages = Parser.parse_mqtt(self.num_topic, payload)
        converter.send_type()
        for msg in messages:
            converter.validate_message(msg[0], msg[1])
        time.sleep(0.3)
        while converter.received_container.curr_size > 0:
            time.sleep(0.3)
            continue

    def send_str_omf(self):
        converter = Convert(self.sender)
        payload = dict()
        value = "string value"
        payload['Value'] = value
        messages = Parser.parse_mqtt(self.str_topic, payload)
        converter.send_type()
        for msg in messages:
            converter.validate_message(msg[0], msg[1])
        time.sleep(0.3)
        while converter.received_container.curr_size > 0:
            time.sleep(0.3)
            continue


if __name__ == '__main__':
    unittest.main()
