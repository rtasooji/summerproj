# Created by dark at 8/24/2018, 1:35 PM
import psycopg2 as postgres
import json
import datetime
import pandas as pd
"""
Outback_Year
Outback_Month
Outback_Day
Outback_Hour
Outback_Minute
Outback_Second
OutBack_Measured_System_Voltage
OB_Set_Sell_Voltage
OB_Set_Radian_Inverter_Sell_Current_Limit
I_DC_Voltage
GS_Split_Inverter_Operating_Mode
GS_Split_Sell_kW
FN_Shunt_A_Current
FN_Shunt_B_Current
FN_Shunt_C_Current
FN_Battery_Voltage
FN_Battery_Current
"""

def get_timestamp(input: list):
    for value in input:
        if value['name'] == 'Outback_Year':
            year = value['value']
        if value['name'] == 'Outback_Month':
            month = value['value']
        if value['name'] == 'Outback_Day':
            day = value['value']
        if value['name'] == 'Outback_Hour':
            hour = value['value']
        if value['name'] == 'Outback_Minute':
            minute = value['value']
        if value['name'] == 'Outback_Second':
            second = value['value']
    date = datetime.datetime(year=year, month=month, day=day, hour=hour,
                             minute=minute, second=second)
    return date
try:

    labels = ['Stored_Time', 'Sent_Time', 'Measured_System_Voltage',
              'OB_Set_Sell_Voltage', 'OB_Set_Radian_Inverter_Sell_Current_Limit',
              'I_DC_Voltage', 'GS_Split_Inverter_Operating_Mode', 'GS_Split_Sell_kW',
              'FN_Shunt_A_Current', 'FN_Shunt_B_Current', 'FN_Shunt_C_Current',
              'FN_Battery_Voltage', 'FN_Battery_Current']

    output = []
    message = []

    conn = postgres.connect("dbname=ElectricConsumption user=postgres password=aghareza")
    cur = conn.cursor()

    cur.execute("DECLARE handle SCROLL CURSOR FOR SELECT stringvalue, ts FROM reporting_raw_data"
                " WHERE stringvalue IS NOT NULL AND device_id=0;")
    cur.execute("FETCH 100 FROM handle")

    for record in cur:
        stored_dict = dict()

        measured_voltage = 0.0
        sell_voltage = 0.0
        inverter = 0.0
        dc_voltage = 0.0
        split_inverter = 0.0
        split_sell = 0.0
        shunt_a = 0.0
        shunt_b = 0.0
        shunt_c = 0.0
        battery_volt = 0.0
        battery_curr = 0.0

        result = json.loads(record[0])
        for value in result:
            if 'name' in value:
                if value['name'] == 'OutBack_Measured_System_Voltage':
                    measured_voltage = value['value']
                elif value['name'] == 'OB_Set_Sell_Voltage':
                    sell_voltage = value['value']
                elif value['name'] == 'OB_Set_Radian_Inverter_Sell_Current_Limit':
                    inverter = value['value']
                elif value['name'] == 'I_DC_Voltage':
                    dc_voltage = value['value']
                elif value['name'] == 'GS_Split_Inverter_Operating_Mode':
                    split_inverter = value['value']
                elif value['name'] == 'GS_Split_Sell_kW':
                    split_sell = value['value']
                elif value['name'] == 'FN_Shunt_A_Current':
                    shunt_a = value['value']
                elif value['name'] == 'FN_Shunt_B_Current':
                    shunt_b = value['value']
                elif value['name'] == 'FN_Shunt_C_Current':
                    shunt_c = value['value']
                elif value['name'] == 'FN_Battery_Voltage':
                    battery_volt = value['value']
                elif value['name'] == 'FN_Battery_Current':
                    battery_curr = value['value']

        message.extend([record[1], get_timestamp(result),
                        measured_voltage, sell_voltage, inverter,
                        dc_voltage, split_inverter, split_sell, shunt_a,
                        shunt_b, shunt_c, battery_volt, battery_curr])
        output.append(message.copy())
        message.clear()

    data = pd.DataFrame.from_records(data=output, columns=labels)
    data.index.name = 'index'
    data.to_csv(path_or_buf='test.csv')
except (Exception, postgres.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()
        print("Database connection closed.")