# Created by dark at 6/17/2018, 7:44 PM
from threading import Thread
from OSISummer.publisher_sim.pub_W_top import MakePublishers
class ThreadPublisher(Thread):

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None):
        super(ThreadPublisher, self).__init__(group=group, target=target,
                                       name=name)
        Thread.__init__(self)
        self.args = args

        self.kwargs = kwargs

    def run(self):
        pub = MakePublishers(self.args[0], write=self.args[1])
        pub.make_publisher(self.args[2], self.args[3])
