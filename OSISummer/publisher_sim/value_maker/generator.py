# Created by tasooji at 6/17/2018, 6:23 PM
import time


class IntGenerator(object):

    def __init__(self, value: int,  frequency: int):
        """
        Integer generator that increase the value by one based on input frequency\n
        :param value: starting value
        :param frequency: frequency for increasing value by one
        """
        self._currValue = value
        self.freq = frequency

    def __iter__(self):
        return self

    def __next__(self):
        self._currValue += 1
        time.sleep(1/self.freq)
        return self._currValue
