# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 10:03:15 2018

@author: rtasooji
"""
import getopt
import sys
import time
import datetime
import OSISummer.mqttpub.resources as rs
import os
from OSISummer.mqttpub.parsers.messageReader import GSRReader
from OSISummer.mqttpub.sender import Mqttsender


# %%
def data_generator(input_data, freq, endpoint):
    for i in range(endpoint):
        time.sleep(1 / freq)
        yield input_data.iloc[i, ]  # FIXME: the type of data in class is not consistence
# %%


def get_time():
    return (datetime.datetime.utcnow() + datetime.timedelta(
        days=-15)).isoformat() + 'Z'


# broker_address = "10.32.52.150"
broker_address = "127.0.0.1"
port_value = 1883
data_loc = os.path.dirname(rs.__file__)
file_name = "Test_GSR.csv"
user = ""
password = ""
client = "test"
freq = 1
topic = "F_HAUS/"
waiting_time = 5.0  # second to stop the connection to relay
counter = 0  # TODO: find better way to remove counter


# FIXME: Try adding the date_generator in MessageReader.
def main(argv):
    global broker_address
    global port_value
    global file_name
    global user
    global password
    global client
    global freq
    global topic
    try:
        opts, args = getopt.getopt(argv, "d:c:f:ht:",
                                   ["data=", "client=",
                                    "freq=", "help", "topic="])

    except getopt.GetoptError:
        print("input error")
        sys.exit(2)  # command line sysntax error code

    for opt, arg in opts:
        if opt == "-d":
            file_name = arg
        elif opt == "-c":
            client = arg
        elif opt == "-f":
            freq = int(arg)
        elif opt == "-t":
            print(arg)
            topic = topic.__add__(arg)
            print(topic)
        elif opt == "-h":
            print("Options:\n"
                  "-h -help\tShow this screen\n"
                  "-d -data\tFile location\n"
                  "-c -client\tClient name\n"
                  "-f -freq\tTransfer frequency\n"
                  )
        with open(os.path.join(data_loc, "context.json")) as f:
            for line in f:
                line = line.rstrip()
                key, value = line.split("=", 1)
                if key == "ip":
                    broker_address = value
                elif key == "port":
                    port_value = int(value)
                elif key == "user":
                    user = value
                elif key == "pass":
                    password = value

if __name__ == "__main__":
    main(sys.argv[1:])  # sys.arg[0] name of the script

print("Topic: " + topic)
print(broker_address)
print(port_value)
print(user)
print(password)
gsr_data = GSRReader(os.path.join(data_loc, file_name))
currTime = time.time()


with Mqttsender(broker_address, port_value, client, user=user,
                password=password) as sender:
    values = data_generator(gsr_data.rawData, freq, gsr_data.numRow)
    for i in values:

        print(get_time(), "Input Time")
        while True:
            if sender.connected is False:
                if time.time() > currTime + waiting_time:
                    raise ConnectionError("Connection failed!")
            else:
                sender.publishMessage(topic,
                                      gsr_data.json_creator(get_time(),
                                                            float(i),
                                                            "Data imported from {0}".format(
                                                                gsr_data.filename),
                                                            gsr_data.status.on.value
                                                            )
                                      , 1)
                counter += 1
                print("sent: ", str(i))
                break
