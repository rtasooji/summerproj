"""
Created on Fri Jun  1 10:03:15 2018

@author: rtasooji
Create specified number of publisher with unique input range for each instance
and connect and send the data with defined frequency
TODO:
    at the start get the server time, set time and use them for each client
    for get time

    make unique UUID for each total number of instanced objects
    use the unique format for each instance
        number of instance _ generated value

"""
import time
import datetime
import OSISummer.mqttpub.resources as rs
import os
from OSISummer.mqttpub.sender import Mqttsender
from OSISummer.mqttpub.outputs.writer import WriteLog
from publisher_sim.value_maker import IntGenerator
import json
import uuid


class MakePublishers(object):

    def __init__(self, publisher_name: str, write: bool = False):

        self.name = publisher_name
        with open(os.path.join(os.path.dirname(rs.__file__),
                               "context.json")) as f:
            for line in f:
                line = line.rstrip()
                key, value = line.split("=", 1)
                if key == "ip":
                    self.broker_address = value
                elif key == "port":
                    self.port = int(value)
                elif key == "user":
                    self.user = value
                elif key == "pass":
                    self.password = value
        self.client = str(uuid.uuid4())
        self.topic = "F_HAUS/IoT/" + self.name
        self.clean_session = False
        if write:
            self.writer = WriteLog("publisher_" + self.name + ".csv")
        else:
            self.writer = None

    @staticmethod
    def get_time():
        return (datetime.datetime.utcnow() + datetime.timedelta(
            days=0)).isoformat() + 'Z'

    @staticmethod
    def json_creator(time_stamp, value, disc, status):
        return json.dumps({'Timestamp': str(time_stamp),
                           'Value': value,
                           'Description': disc,
                           'Status': status
                           })

    def make_publisher(self, value, freq):
        value_generator = IntGenerator(value, freq)
        sent_value = self.name + "_"
        with Mqttsender(self.broker_address, self.port, self.client,
                        user=self.user, password=self.password,
                        clr_ses=self.clean_session) as sender:
            print(self.get_time(), "Input Time")
            while True:
                sent_value = ''.join(sent_value.split("_", 1)[0])
                sent_value += "_" + str(value_generator.__next__())
                if sender.connected is False:
                    if time.time() > currTime + waiting_time:
                        raise ConnectionError("Connection failed!")
                else:
                    sender.publishMessage(self.topic,
                                          self.json_creator(self.get_time(),
                                                                str(sent_value),
                                                                "publisher {} with value {}".format(
                                                                    self.name, sent_value),
                                                                "ON"
                                                            )
                                          , 1)
                    if self.writer is not None:
                        self.writer.write(sent_value)
                    print("sent: ", sent_value)















