"""
Created  by rtasooji on 6/20/2018

Create specified number of publisher with unique input range for each instance
and connect and send the data with defined frequency
TODO:
    at the start get the server time, set time and use them for each client
    for get time

    make unique UUID for each total number of instanced objects
    use the unique format for each instance
        number of instance _ generated value

"""
import time
import datetime
import OSISummer.mqttpub.resources as rs
import os
from OSISummer.mqttpub.sender import Mqttsender
from OSISummer.mqttpub.outputs.writer import WriteLog
from OSISummer.publisher_sim.value_maker.generator import IntGenerator
import json
import uuid
import random


class MakePublishers(object):

    def __init__(self, publisher_name: str, write: bool = False):

        self.name = publisher_name
        with open(os.path.join(os.path.dirname(rs.__file__),
                               "context.json")) as f:
            user_inputs = json.load(f)
            self.broker_address = user_inputs['broker']['ip_address']
            self.port_value = user_inputs['broker']['port_num']
            self.user = user_inputs['broker']['user_name']
            self.password = user_inputs['broker']['password']
        self.client = str(uuid.uuid4())
        self.topics = list()

        self.topic = "OSI/test1/" + self.name  #CHECK: hard coded for now
        self.generate_topic(self.topic, 1, 1)  #CHECK: hard coded for now

        self.clean_session = False
        self.write = write
        self.writers = dict()
    @staticmethod
    def get_time():
        return (datetime.datetime.utcnow() + datetime.timedelta(
            days=0)).isoformat() + 'Z'

    @staticmethod
    def json_creator(time_stamp, value, disc, status):
        return json.dumps({'Timestamp': str(time_stamp),
                           'Value': value,
                           'Description': disc,
                           'Status': status
                           })

    def generate_topic(self, topic_name, numbers, value):
        """
        "F_HAUS/IoT/" + self.name
        :param topic_name:
        :param numbers:
        :param value:
        :return:
        """
        for i in range(numbers):
            topic_name += "/" + str(value)
            value += 1
            self.topics.append(topic_name)
            topic_name, _ = topic_name.rsplit("/", 1)

    def make_publisher(self, value, freq):
        value_generator = IntGenerator(value, freq)
        with Mqttsender(self.broker_address, self.port_value, self.client,
                        user=self.user, password=self.password,
                        clr_ses=self.clean_session) as sender:
            print(self.get_time(), "Input Time")
            while True:
                sent_value = value_generator.__next__()
                for i in self.topics:
                    time.sleep(random.randint(1, 10) * 0.001)
                    sender.publishMessage(i,
                                          self.json_creator(self.get_time(),
                                                                 sent_value,
                                                                "publisher {} "
                                                                "with value {}"
                                                                .format(
                                                                    self.name,
                                                                    sent_value),
                                                                "ON"
                                                            )
                                          , 1)
                    file_name = i.replace("/", "_")
                    # TODO:
                    # this method is slow and it is only needs to be used
                    # for debugging purposes, remove this in release version.
                    if self.write:
                        if file_name not in self.writers.keys():
                            writer = WriteLog("publisher_" + file_name + ".csv")
                            self.writers[file_name] = writer
                            writer.write(str(sent_value))
                        else:
                            writer = self.writers[file_name]
                            writer.write(str(sent_value))
                    print("sent: {0}/{1}/{2}".format(self.name, i, sent_value))
