"""
Created  by rtasooji on 6/19/2018
"""


class Messages(object):

    @staticmethod
    def create_type():
        """
        If the structure changed change queueParser.py to get correct number
        of payloads and on receiver change validate_message for making correct
        format.
        :return:
        """
        msg_body = [
                {
                    "id": "FirstStaticType",
                    "name": "First static type",
                    "classification": "static",
                    "type": "object",
                    "description": "First static asset type",
                    "properties":
                        {
                            "index": {
                                "type": "string",
                                "isindex": True,
                                "name": "not in use",
                                "description": "not in use"
                            },
                            "name": {
                                "type": "string",
                                "isname": True,
                                "name": "not in use",
                                "description": "not in use"
                            }
                         }
                },
                {
                    "id": "SecondDynamicType",
                    "name": "Second dynamic type",
                    "classification": "dynamic",
                    "type": "object",
                    "description": "not in use",
                    "properties":
                        {
                        "timestamp":
                            {
                            "format": "date-time",
                            "type": "string",
                            "isindex": True,
                            "name": "not in use",
                            "description": "not in use"
                              },
                        "NumberProperty1":
                            {
                            "type": "number",
                            "format": "float16",
                            "name": "Value",
                            "description": "PI point data value sent"
                                           " by device/sensor"
                             },
                        "Property2":
                            {
                            "type": "string",
                            "name": "Timestamp",
                            "description": "Timestamp sent by the sensor/device",
                            "format": "date-time"
                             },
                        "StringEnum":
                            {
                            "type": "string",
                            "enum": ["False", "True", "Open",
                                     "Close", "On", "Off"],
                            "name": "Status",
                            "description": "String enumeration to describe"
                                           " status of the device"
                             },
                        "Description":
                            {
                            "type": "string",
                            "name": "Description",
                            "description": "Dynamic asset's attribute description"
                             }
                        }
                }
            ]
        return msg_body

    @staticmethod
    def create_data_for_dynamic(containerid,
                                value, timestamp,
                                status, description):
        """
        This needs to be changed based on the received message
        :param containerid:
        :param value:
        :param timestamp:
        :param status:
        :param description:
        :return:
        """
        container = [
            {
                "containerid": containerid,
                "values": [
                    {
                        "timestamp": timestamp,
                        "NumberProperty1": value,
                        "Property2": timestamp,
                        "StringEnum": status,
                        "Description": description
                    }
                ]
            }
        ]
        return container

    @staticmethod
    def call_method(method_number, *args):
        """

        :param method_number: 0: create_container\n
        1: create_asset_data\n
        2: create_asset_link\n
        3: create_container_link\n
        4: create data
        5: create MATE3s
        6: create data for MATE3s
        :param args:
        :return:
        """
        try:
            if method_number == 0:
                return Messages.create_container(args[0][0])
            elif method_number == 1:
                return Messages.create_asset_data(args[0][0], args[0][1])
            elif method_number == 2:
                return Messages.create_asset_link(args[0][0], args[0][1])
            elif method_number == 3:
                return Messages.create_container_link(args[0][0], args[0][1])
            elif method_number == 4:
                return Messages.create_data_for_dynamic(args[0][0], args[0][1],
                                                        args[0][2], args[0][3],
                                                        args[0][4])

            else:
                raise IndexError
        except IndexError as e:
            print("method number not in the list")

    @staticmethod
    def create_container(containerid):
        container = [{
            "id": containerid,
            "typeid": "SecondDynamicType"
        }]
        return container

    @staticmethod
    def create_asset_data(index, assetName):
        assetDataMessage = [{
            "typeid": "FirstStaticType",
            "values": [{
                "index": index,
                "name": assetName
            }]
        }]
        return assetDataMessage

    @staticmethod
    def create_asset_link(sourceIndex, targetIndex):
        assetLinkMessage = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": sourceIndex
                },
                "target": {
                    "typeid": "FirstStaticType",
                    "index": targetIndex
                }
            }]
        }]
        return assetLinkMessage

    @staticmethod
    def create_container_link(containerSourceIndex, containerid):
        containerLinkMessage = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": containerSourceIndex
                },
                "target": {
                    "containerid": containerid
                }
            }]
        }]
        return containerLinkMessage







