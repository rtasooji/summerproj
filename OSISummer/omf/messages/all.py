"""
Created  by rtasooji on 6/19/2018
"""


class Messages(object):

    @staticmethod
    def create_type():
        """
        If the structure changed change queueParser.py to get correct number
        of payloads and on receiver change validate_message for making correct
        format.
        :return:
        """
        msg_body = [
                {
                    "id": "FirstStaticType",
                    "name": "First static type",
                    "classification": "static",
                    "type": "object",
                    "properties":
                        {
                            "index": {
                                "type": "string",
                                "isindex": True
                            },
                            "name": {
                                "type": "string",
                                "isname": True
                            }
                         }
                },
                {
                    "id": "NumberStaticType",
                    "name": "Number static type",
                    "classification": "static",
                    "type": "object",
                    "properties":
                        {
                            "index": {
                                "type": "string",
                                "isindex": True
                            },
                            "name": {
                                "type": "string",
                                "isname": True
                            }
                        }
                },
                {
                    "id": "StringStaticType",
                    "name": "String static type",
                    "classification": "static",
                    "type": "object",
                    "properties":
                        {
                            "index": {
                                "type": "string",
                                "isindex": True
                            },
                            "name": {
                                "type": "string",
                                "isname": True
                            }
                        }
                },
                {
                    "id": "NumberDynamicType",
                    "name": "Number dynamic type",
                    "classification": "dynamic",
                    "type": "object",
                    "properties":
                        {
                            "timestamp":
                                {
                                    "format": "date-time",
                                    "type": "string",
                                    "isindex": True
                                },
                            "Property":
                                {
                                    "type": "number",
                                    "format": "float16",
                                    "name": "Value"
                                }
                        }
                },   {
                    "id": "StringDynamicType",
                    "name": "String dynamic type",
                    "classification": "dynamic",
                    "type": "object",
                    "properties":
                        {
                            "timestamp":
                                {
                                    "format": "date-time",
                                    "type": "string",
                                    "isindex": True
                                },
                            "Property":
                                {
                                    "type": "string",
                                    "name": "Value"
                                }
                        }
                }
            ]
        return msg_body

    @staticmethod
    def create_data_for_dynamic(containerid,
                                value, timestamp):
        """
        This needs to be changed based on the received message
        :param containerid:
        :param value:
        :param timestamp:
        :return:
        """

        container = [
            {
                "containerid": containerid,
                "values": [
                    {
                        "timestamp": timestamp,
                        "Property": value
                    }
                ]
            }
        ]

        print("in create dyn type: " + str(container))
        return container

    @staticmethod
    def call_method(method_number, *args):
        """
        :param method_number:
        0: create_container\n
        1: create_asset_data\n
        2: create_asset_link\n
        3: create_container_link\n
        4: create data
        :param args:
        :return:
        """
        try:
            if method_number == 0:
                return Messages.create_container(args[0][0], args[0][1])
            elif method_number == 1:
                return Messages.create_asset_data(args[0][0], args[0][1], args[0][2])
            elif method_number == 2:
                return Messages.create_asset_link(args[0][0], args[0][1], args[0][2])
            elif method_number == 3:
                return Messages.create_container_link(args[0][0], args[0][1], args[0][2])
            elif method_number == 4:
                return Messages.create_data_for_dynamic(args[0][0], args[0][1],
                                                        args[0][2])
            else:
                raise IndexError
        except IndexError as e:
            print("method number not in the list")

    @staticmethod
    def create_container(containerid, container_type):
        print(container_type)
        if container_type == 0:
            container = [{
                "id": containerid,
                "typeid": "NumberDynamicType"
            }]
        else:
            container = [{
                "id": containerid,
                "typeid": "StringDynamicType"
            }]
        print("in container type:  " + str(container))
        return container

    @staticmethod
    def create_asset_data(index, assetName, type_id):
        if type_id == 0:
            target = "FirstStaticType"
        if type_id == 1:
            target = "NumberStaticType"
        if type_id == 2:
            target = "StringStaticType"

        assetDataMessage = [{
            "typeid": target,
            "values": [{
                "index": index,
                "name": assetName
            }]
        }]
        return assetDataMessage

    @staticmethod
    def create_asset_link(sourceIndex, targetIndex, type_id):
        if type_id == 0:
            target = "FirstStaticType"
        if type_id == 1:
            target = "NumberStaticType"
        if type_id == 2:
            target = "StringStaticType"

        assetLinkMessage = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": sourceIndex
                },
                "target": {
                    "typeid": target,
                    "index": targetIndex
                }
            }]
        }]
        return assetLinkMessage

    @staticmethod
    def create_container_link(containerSourceIndex, containerid, source_id):
        if source_id == 1:
            target = "NumberStaticType"
        else:
            target = "StringStaticType"

        containerLinkMessage = [{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": target,
                    "index": containerSourceIndex
                },
                "target": {
                    "containerid": containerid
                }
            }]
        }]
        return containerLinkMessage







