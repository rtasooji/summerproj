"""
Created  by rtasooji on 6/19/2018
"""
import json
import gzip
import datetime
from requests import post, packages, Response


class OMFSender(object):

    def __init__(self, producer_token: str, ingress_url: str, compress=True,
                 verify_ssl=False, timeout=30, version="1.0"):
        """

        :param producer_token: from relay
        :param ingress_url: from relay
        :param compress: True by default
        :param verify_ssl: False by default
        :param timeout: 30 seconds by default
        :param version: 1.0 input string
        """
        self._PRODUCER_TOKEN = producer_token
        self._INGRESS_URL = ingress_url
        self.compress = compress
        self.ssl = verify_ssl
        self.timeout = timeout
        self.version = version

    def _make_message(self, type, body, action):
        """
        Make message to be sent to PI system
        :param type: type of the messages
        :param body: Message OMF in JSON format
        :return: header and body ready to be sent
        """
        if self.compress:
            msg_body = gzip.compress(bytes(json.dumps(body), 'utf-8'))
            self.compress = 'gzip'
        else:
            msg_body = json.dumps(body)
            self.compress = 'none'
        header = {
            'producertoken': self._PRODUCER_TOKEN,
            'messagetype': type,
            'action': action,
            'messageformat': 'JSON',
            'omfversion': self.version,
            'compression': self.compress
        }

        return header, msg_body

    def postMessage(self, type: str, body, action='create') -> Response:
        """
        post message using PI Relay
        :param type: "type", "container", "data"
        :param body: json format
        :param action: currently only "create" is supported
        :return: requests.Response
        """
        msg_header, msg_body = self._make_message(type, body, action)
        response = Response()
        try:
            response = post(self._INGRESS_URL,
                            headers=msg_header,
                            data=msg_body,
                            verify=self.ssl,
                            timeout=self.timeout)

            if not self.ssl:
                packages.urllib3.disable_warnings()
            print('\nResponse from relay from the initial "{0}" message:'
                  '{1} {2} {3}'.format(type, response.status_code, response.text,
                                       str(datetime.datetime.now())))
            return response

        except Exception as e:
            print(str(datetime.datetime.now()) + " An error ocurred during"
                                              " web request: " + str(e))
        finally:
            return response
