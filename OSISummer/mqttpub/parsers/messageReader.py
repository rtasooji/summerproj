# -*- coding: utf-8 -*-
"""
Created on Wed May 30 08:38:07 2018

@author: rtasooji
"""
#%%Modulers
import pandas as pd
import numpy as np
import datetime
import json
import OSISummer.mqttpub.parsers.status as st


#%%
class GSRReader(object):
    """Read the file and provide simple parser
        Attribute:
            fileName: the name of the csv file to read
   """

    def __init__(self, fileName):
        try:
            self.filename = fileName
            self.data = pd.read_csv(fileName, names=["Device",
                                                     "UnixTime",
                                                     "ConvertedTime",
                                                     "RawData"])
            self.numRow, self.numCol = self.data.shape
            self.empUnixTime = self.data.iloc[:, 2]
            self.rawData = self.data.iloc[:, 3]
            self.timeStamp = np.empty([self.numRow, ],
                                      dtype='datetime64[ms]')
            self._set_timeStamp(self)
            self.status = st.EmpaticaStatus
        except FileNotFoundError:
            print("Wrong file or file path")

#%%
    def copyData(self):
        return pd.DataFrame.copy(self.data)

#%%
    @staticmethod
    def _set_timeStamp(self):
        for i in range(self.numRow):

            currUnix = self.empUnixTime.iloc[i, ]
            time = datetime.datetime.fromtimestamp(currUnix).strftime(
                    '%Y-%m-%d %H:%M:%S.%f')
            self.timeStamp[i, ] = time
#%%

    @staticmethod
    def json_creator(timeStamp, value, disc, status):
        return json.dumps({'Timestamp': str(timeStamp),
                           'Value': value,
                           'Description': disc,
                           'Status': status
                           })
#################



# datetime.datetime.utcnow().isoformat() + 'Z'