# Created by dark at 6/4/2018, 3:44 PM
from enum import Enum


class EmpaticaStatus(Enum):
    off = "Off"
    on = "On"


class DoorStatus(Enum):
    off = "Close"
    on = "Open"


class DefaultStatus(Enum):
    off = "False"
    on = "True'"