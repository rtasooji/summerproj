# -*- coding: utf-8 -*-
"""
Created on Thu May 31 08:13:27 2018

@author: rtasooji
"""
#%%Modules
import paho.mqtt.client as mqtt
#%%


class Mqttsender(object):
    """Stablish mqtt event and send topic and payload to broker.\n
    Args:
        brokderAddress (str): ip address to the broker.\n
        port (int)\n
        client (str): name of the mqtt client.\n
        user (str): default empty\n
        password (str): default empty\n
    """

    def __init__(self, broker_address: str, portNum: str, client: str,
                 user=None, password=None, clr_ses=False):
        if user is None:
            self.user = ""
        else:
            self.user = user
        if password is None:
            self.password = ""
        else:
            self.password = password
        # clean_session = False, uses persistent session
        self.client = mqtt.Client(client, clean_session=clr_ses)
        self.brokerAddress = broker_address
        self.port = int(portNum)
        self.connected = False
        self.client.username_pw_set(self.user, self.password)
        self.client.on_connect = self._on_connect
        self.establish_connection()

    def __enter__(self):
        self.client.loop_start()
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stopLoop()
        self.disconnect()

    def establish_connection(self):
        self.client.connect(self.brokerAddress, self.port)

    def _on_connect(self, client, userdata, flags, rc):
        print("Connected with result code {0}".format(rc))
        if rc == 0:
            print("Connected to the broker")
            self.connected = True
        else:
            print("Connected failed")
            client.loop_stop()

    def publishMessage(self, topic, message, qos):
        if type(qos) is not int:
            raise TypeError
        if self.connected:
            self.client.publish(topic, message, qos=qos)
            
    def stopLoop(self):
        self.client.loop_stop()
        
    def disconnect(self):
        self.client.disconnect()
        
    def set_user(self, username):
        self.user = username
        
    def set_password(self, password):
        self.password = password
        
    def set_broker(self, brokerAddress):
        self.brokerAddress = brokerAddress
        
    def set_port(self, port):
        self.port = port
#%%






