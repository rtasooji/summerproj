# -*- coding: utf-8 -*-
"""
Created on Tue May 29 08:04:57 2018

@author: rtasooji
"""
#%%
#Moduels
import abc
from time import time
from datetime import datetime
#%%


class WriteFile(object):
    """Write information on file.\n
        Attrribute:
            file_Name: name of the file to store information. If the file 
            exists, the line will be added at the end of the file
        Beware this is very bad implementation to write to file and it is slow.
        The format works classes in analysis but be aware of this performance.
    """
    __metaclass__ = abc.ABCMeta
    
    def __init__(self, file_name):
        self.fileName = file_name
    
    @abc.abstractmethod
    def write(self, input_value):
        return

    def write_line(self, str_value):
        with open(self.fileName, 'a') as f:  # CHECK:
                                                    # expensive method, opens the
                                                    # file every time and close
                                                    # it for just one output.
            f.write(str_value + "\n")
#%%


class WriteLog(WriteFile):
    """Write log info on the file.\n
    The Log Info will be pre-defined with three headers.\n
    UnixTime,Current Time and output message.
    Attributes:
        file_name: name of the file.\n
        delim: the delimiter for the file format, the defautl is ','.\n
    """
    def __init__(self, file_name, delim = None):
        if delim is None:
            self.delimiter = ','
        else:
            self.delimiter = delim
        super(WriteLog, self).__init__(file_name)

    def write(self, input_value):
        unixTime = str(time())
        currTime = datetime.now().strftime("%Y_%m_%d %H:%M:%S.%f")
        result = self.makeDelimiter(self, [unixTime, currTime, input_value])
        self.write_line(result)
        
    @staticmethod
    def makeDelimiter(self, list_input):
        strValue = self.delimiter.join(list_input)
        return strValue

        
#%%
class WriteLine(WriteFile):
    def write(self, input_value):
        self.write_line(input_value)
        
        
        
        