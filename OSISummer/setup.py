from setuptools import setup

setup(
    name="OSISummer",
    install_requires=[
        'paho-mqtt',
        'numpy',
        'pandas',
        'requests',
        'rx',
        'PySide2',
        'fbprophet',
        'scikit-learn',
        'scipy',
        'pythonnet',
        'matplotlib'
    ]
)