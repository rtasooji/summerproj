# Created by dark at 6/22/2018, 5:48 PM
"""
This script will create a listener for receiving payloads from mqtt borker.
Before running the code check "TO DO" lists. some of the time stamps might
commented out or changed for testing purposes.
"""
import time
import json
from mqttReceiver.rx_receiver import MqttClient
from omf.omfSender import OMFSender
from omf.messages.all import Messages
import mqttReceiver.resources as rs
import os
from mqttReceiver.exception.response_code import RESPONSEERROR
import uuid

# The OMFSender is needed to send messages to the PI system.
sender = None


def get_values() -> dict:
    """
    Get values from the context.json file in the resources folder in
     mqttReceive package
    :return: relay and broker inputs
    """
    usr_input = dict()
    res_path = os.path.dirname(rs.__file__)
    try:
        with open(os.path.join(res_path, "context.json")) as f:
            usr_input = json.load(f)
    except FileExistsError:
        print("file not found!")
    return usr_input


def make_sender(user_inputs: dict):
    """
    Creates a sender object that will be used to send the bulk of data inside
    mqtt receiver
    :param user_inputs: producer token and ingress url generated using PI data
    collection manager
    :return:
    """
    global sender
    producer_token = user_inputs['relay']['producer_token']
    ingress_url = user_inputs['relay']['Ingress_url']
    sender = OMFSender(ingress_url=ingress_url, producer_token=producer_token)


def send_type():
    """
    Send the type omf body to pi system.
    This is the starting point of the receiving and sending.
    If there are any chagnes in the format this message needs to be changed.
    To see the format check create_type method in Messages package
    :return:
    """
    # make sender
    global sender
    # send the type body
    respond = sender.postMessage("type", Messages.create_type())
    # this code is received if PI system received the message.
    # It will raise exception if it is not.
    if respond.status_code != 204:
        print("something is wrong!")
        raise RESPONSEERROR("message didn't received by relay, received code: {}"
                            .format(respond.status_code))


def start_listener(user_inputs: dict):
    """
    Creates mqtt subscriber and send messages to the PI system
    :param user_inputs: **kwargs
    :return:
    """
    # the sender is required by the receiver to send the queue
    global sender
    # set the values for starting mqtt subscriber
    client_name = str(uuid.uuid4())
    ip_address = user_inputs['broker']['ip_address']
    port_num = user_inputs['broker']['port_num']
    topic_name = user_inputs['broker']['topic_name']
    user_name = user_inputs['broker']['user_name']
    password = user_inputs['broker']['password']
    waiting_time = user_inputs['broker']['waiting_time']
    # start subscriber
    with MqttClient(sender,
                    client_name,
                    ip_address,
                    port_num,
                    topic_name,
                    user=user_name,
                    password=password,
                    qos=1,
                    clean_ses=False) as f:

        if f.connected is False:
            time.sleep(0.5)
        curr_time = time.time()
        try:
            # CHECK:
            # this will check if the subscriber didn't received any message
            # longer than waiting_time it will close the subscriber
            # this might be commented out.
            #while curr_time - f.timer < waiting_time:
            while True:
                time.sleep(0.000005)
            #     curr_time = time.time()
            # raise TimeoutError
        except KeyboardInterrupt:
            print("No message received in %s second. Closing the "
                  "subscriber." % waiting_time)


def run():
    inputs = get_values()
    make_sender(inputs)
    send_type()
    start_listener(inputs)


if __name__ == '__main__':
    run()

