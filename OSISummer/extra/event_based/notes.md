##  File descriptor
*   Used to access a file or other I/O resources, such as pipe or network sockets.
[link](https://en.wikipedia.org/wiki/File_descriptor)


##  Sockets

*   [more about sockets](https://www.tutorialspoint.com/unix_sockets/what_is_socket.htm)

##  coroutine in python

*   result = await future or result = yield from future – suspends the coroutine
 until the future is done, then returns the future’s result, or raises an
 exception, which will be propagated. (If the future is cancelled, it will raise
  a CancelledError exception.) Note that tasks are futures, and everything said
   about futures also applies to tasks.
*   check [link](https://docs.python.org/3/library/asyncio-task.html)


