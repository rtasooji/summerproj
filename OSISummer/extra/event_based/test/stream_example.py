# Created by dark at 6/20/2018, 8:25 PM
import asyncio

async def handle_echo(reader, writer):
    data = yield from reader.read(100)


loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_echo, '127.0.0.1', )