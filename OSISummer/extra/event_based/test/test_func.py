"""
Created  by rtasooji on 6/20/2018
"""
import asyncio
import datetime
import sys

def display_date(endtime, loop):
    """
    Callback function
    :param endtime:
    :param loop:
    :return:
    """
    print(endtime, datetime.datetime.now())
    if (loop.time() + 1.0) < endtime:
        loop.call_later(1, display_date, endtime, loop)
    else:
        loop.stop()

async def display_date_coro(loop):
    """
    Using coroutines
    :param loop:
    :return:
    """
    end_time = loop.time() + 5  # this is going to be fixed in the function at the start
    while True:
        print(datetime.datetime.now())
        if (loop.time() + 1) >= end_time:
            break
        await asyncio.sleep(1)

def main_coro():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(display_date_coro(loop))
    loop.close()

def main():
    loop = asyncio.get_event_loop()
    loop.set_debug(enabled=True)
    end_time = loop.time() + 5
    loop.call_soon(display_date, end_time, loop)
    loop.run_forever()
    loop.close()

try:
    from socket import socketpair
except:
    from asyncio.windows_utils import socketpair


def reader():
    data = rsock.recv(100)
    print("received size: ", sys.getsizeof(data))
    print("Received: ", data.decode())
    #  Done, unregister the file descriptor
    loop.remove_reader(rsock)
    #  stop loop
    loop.stop()


if __name__ == "__main__":
    rsock, wsock = socketpair()
    loop = asyncio.get_event_loop()
    #  register the file descriptor for read event
    loop.add_reader(rsock, reader)

    #  simulate the reception of data from the network
    print("abc size: ", sys.getsizeof('abcdegssdf'))
    loop.call_soon(wsock.send, 'abcdegssdf'.encode())
    loop.run_forever()
    rsock.close()
    wsock.close()
    loop.close()