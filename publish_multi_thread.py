# Created by dark at 6/17/2018, 7:28 PM
from OSISummer.publisher_sim.threadPublisher import ThreadPublisher
import getopt
import sys


num_publishers = 1
log = False
frequency = 1

def main(argv):
    global num_publishers
    global log
    global frequency
    try:
        opts, args = getopt.getopt(argv, "n:l:f:h", ["number=", "logging=",
                                                     "freq=", "help"])
    except getopt.GetoptError:
        print("Invalid input!\n"
              "Options:\n"
              "-n -number\t Total number of publisher\n"
              "-l -logging\t set to 1 to enable writing log info")
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-n":
            num_publishers = arg
        elif opt == '-l':
            if arg == "1":
                log = True
        elif opt == '-f':
            frequency = int(arg)
        elif opt == 'h':
            print("Options:\n"
                  "-n -number\t Total number of publisher\n"
                  "-l -logging\t set to 10 to enable writing log info\n"
                  "-f -freq\t set the number of data generated per seconds")

    make_publisher(int(num_publishers), log, frequency)


def make_publisher(numbers, logging, freq):
    try:
        publishers = []
        for i in range(numbers):
            pub = ThreadPublisher(name="thread"+str(i), args=(str(i),
                                                              logging,
                                                              0,
                                                              freq))
            """
            args[0]: name of the publisher
            args[1]: write to file set to True 
            args[2]: iterator starting number will be next value
            arg[3]: iterator frequency
            """
            publishers.append(pub)
            pub.start()
    except KeyboardInterrupt:
        for i in publishers:
            i.join()
        raise()


if __name__ == "__main__":
    main(sys.argv[1:])  # [0] is name of the script
