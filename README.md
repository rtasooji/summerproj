#   FutureHAUS
## 

---

##  MQTT to OMF Converter

There are two scripts that can be used to  convert MQTT to OMF. 

One includes GUI that can provides periodic Machine Learning(ML)/Deep Learning(DL) analytics. Different methods can be
run on different threads. It uses  pythonnet package to run methods from AFSDK library in C# to retrieve data from PI system.
Also it provides a basic set of four plots to explore values from different topics. This information
can be useful to debug and check if value for different topics are stored correctly inside PI System.

Another way is to use a simple script without GUI. The script simply, convert MQTT messages to OMF and send the message
to PI System.

---

### How to run the code:

Before covering the whole process of re-installing everything if it is needed (Hopefully not!!!). I go through easy setup:

#### Script with GUI:

I made a batch file with the name _start_omf_converter.bat_ on the desktop folder that runs when windows starts. 
The lines in batch file is as follow:

```commandline
@echo off
cmd /k "cd /d C:\Users\Administrator\Desktop\python\venv\Scripts & activate & cd /d    
C:\Users\Administrator\Desktop\python\futurehaus\OSISummer\gui & python main_multi_topic.py"
```

The location of the files might change but what it does it first activate the virtual python and then  runs the GUI script.

You should see something like this:

![](Images/GUIScreen.PNG)

The Relay info, which includes Producer Token and Ingress URL are the parameters that are needed for OMF message to send
payload and store it in PI system.

You can get these information from PI data collection manager. For more information about this, go to osisoft_doc
 repository and see installation.md file. 
 
 The broker info, includes information about MQTT subscriber. 

 
 __All the topics from publisher needs to follow +/+/+/+ format__.

** Hierarchical Structure for Topics:**

* Topic: FutureHAUS/[Room]/[Thing]/[Device]

* Payload:  'Value' : 5

** Example: **

* Topic: FutureHAUS/Lobby/Door/Mesh

* 'Value' : [‘Open’/ ‘Close’/’Stop’]


** this [link](https://docs.google.com/document/d/1HIjwraQCne3SLiWzQO6IPSO4pmODAfiXj647VM4amHA/edit) has the list of all the rooms, smart devices and topics for FutureHAUS: **


The payload for each topic should be single value, either string (on/off, open/close or up/down) or numeric value.

If at some point some topics with different format are going to be published, _queueParser.py_ inside 
OSISummer/mqttReceiver directory is the script to parse payload based on the topic name. Add the parsing method and
call it from _parse_input_ method

When you run this script, be default,  is not subscribed to the broker and it needs to be run.
To run the application, go to File and hit __RUN__ button.

![](Images/GUIScreen_run.PNG)

That's it. Now all values from publisher is going to be stored inside PI system.

In case you want to see the values from different topics, navigate to _Analysis_ and _Show plot_

![](Images/showPlot.PNG)

You should be able to select different topics and see 4 plots as follow:

![](Images/plots.PNG)

This information are stored on temprory buffer and is going to be removed every 10 minutes.


---


#### Script without GUI:

The batch file on desktop with name __start_no_gui_mqtt_to_omf.bat__ is a simple version of mqtt to omf without gui.
Use this if there is not need for periodic analytics. 
The script only converts the mqtt message and send it to OMF.
 In case you want to change the parameters navigate to 
__context.json__ located at OSISummer/mqttReceiver/resources directory.


---

### In detail instruction:

The first step is to make sure python is installed and can be called from command prompt. 
For this project, we used __Python 3.6__, it is recommended to use the same version.

Run command prompt and type 'python'. You can see the version of the python that is being stored in PATH variable. 
You can have different version of Python with different packages for separate project. 
For this reason it is highly recommended to create [python virtual environment](https://docs.python.org/3/tutorial/venv.html) 
that only works with this project, and as for IDE It is recommended to use pycharm.

The _setup.py_ script file inside OSISummer folder includes all the packages that needs to be added for this project.
PyCharm by default pops up a notification and ask you to install all the dependencies.

Make sure your python uses OSISummer package as the source for finding the modules.

In mqttReceiver package under resources, you can define different information in
context.json file such as relay and mqtt broker information.

After that, run_listern.py script, in OSISummer module will run the listener.

Make sure you run the python from command prompt not the IDE, the IDE will 
require extra resources for no reason.
It is very light application that uses 30 MB memory and 2 percent of the 
cpu @ 3.6 GHz


---

### Class hierarchy

*   FutureHAUS
    *   OSISummer
        *   __mqttReceiver__  --> deals with received messages
        *   __omf__  --> deals with sending OMF messages to PI
        *   analysis --> includes methods for ML/DL analysis
        *   gui --> provides graphical user interface
        *   pi --> retrieve information from PI system
        *   test --> includes some unit test scripts
        *   extra -->  for testing
        *   mqttpub --> for testing, makes dummy mqtt publisher
        *   publisher_sim --> for testing makes pre defined multi publishers


---

##   How to work with omf message format:

This description is my understanding from OMF and how I implemented in the software.
Before any change make sure you read this, run the omfSender_test.py folder 
in omf/messages/test to fully understand how PI system receives the OMF message.


The OMF format needs to be sent using POST request, and post requires header and 
body. the Messages class in omf/messages/all.py will make the body of the messages
and based on the type of the message you need to define the header.

There is an order for making message in OSIsoft and you need to make sure that
you provide this order in correct format.

First for OMF to work, the PI system needs to know what it will going to receive.

In Messages class the method _create_type()_ is going to provide this information.

To send this message, from omf.omfSender create OMFSender class. The sender object
requires information from Relay, which is the producer token and ingress url.
 
 From PI Data Collection Manager, you can get these information and instantiate
 you omf sender object.
 
 To post a message form the omfSender object use the 
 _postMessage(type, body, action='create')_ method to send this to PI system.

Example Code: 

```Python
omf_sender = OMFSender(ingress_url="your ingress_url here",
                       producer_token="your producer_token here")
respond = omf_sender.postMessage("type", Messages.create_type())
print("Response code: {}".fromat(respond.status_code))                     
```

```bash
Response code: 204
```

The post will return the response, make sure the response state code is 204.

This type body will create what static type will look like.
 (types that wont receive any values
 and are assets for example in topic F_HAUS/room1/lamp1, F_HAUS and room1 are
 considered as static because they wont store any value while lamp1 might 
 store information such as energy, on/off state and other things)
and what dynamic types will look like, if you have more types you need 
to provide different id for them to let that body know what type it will be 
in the PISystem.

Example for OMF type message:
```json
  [
    {"id": "FirstStaticType",
    "name": "First static type",
    "classification": "static",
    "type": "object",
    "description": "First static asset type",
    "properties":
        {
            "index": {
                "type": "string",
                "isindex": True,
                "name": "not in use",
                "description": "not in use"
            },
            "name": {
                "type": "string",
                "isname": True,
                "name": "not in use",
                "description": "not in use"
            }
         }
    },
    {
    "id": "SecondDynamicType",
    "name": "Second dynamic type",
    "classification": "dynamic",
    "type": "object",
    "description": "not in use",
    "properties":
        {
        "timestamp":
            {
            "format": "date-time",
            "type": "string",
            "isindex": True,
            "name": "not in use",
            "description": "not in use"
              },
        "NumberProperty1":
            {
            "type": "number",
            "format": "float16",
            "name": "Value",
            "description": "PI point data value sent"
                           " by device/sensor"
             },
        "Property2":
            {
            "type": "string",
            "name": "Timestamp",
            "description": "Timestamp sent by the sensor/device",
            "format": "date-time"
             },
        "StringEnum":
            {
            "type": "string",
            "enum": ["False", "True", "Open",
                     "Close", "On", "Off"],
            "name": "Status",
            "description": "String enumeration to describe"
                           " status of the device"
             },
        "Description":
            {
            "type": "string",
            "name": "Description",
            "description": "Dynamic asset's attribute description"
             }
        }
    }
 ]             
```
The next body message that needs to be sent to PISystem are the containers.
Containers are the messages that lets PI system knows that they will
 receive values and they need to store the up-coming values.

Json Example for container message:
```json
{
            "id": "F_HAUS/room1/light1_container",
            "typeid": "SecondDynamicType"
        }
```
Example for sending container message to PI system in Python:
```python
containerid = "F_HAUS/room1/light1_container"
value_holder = Messages.call_method(0, containerid)
omf_sender.postMessage("container", value_holder, action='create')
```

This will be your container and you need to track the containerid to connect it 
the the asset. The tricky part is to understand the container will only hold
values and although you think you have "F_HAUS/room1/light1" but this 
is your asset and you need to link the container to this asset.

So you need to create an asset too. here is the json format to create an asset:
```json
{
            "typeid": "FirstStaticType",
            "values": [{
                "index": "F_HAUS/room1/light1", 
                "name": "The name that will be shown up in PI"
            }]
        }
```

and then you link the container to the asset.
here the json example for linking:
```json
{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": "F_HAUS/room1/light1"
                },
                "target": {
                    "containerid": "F_HAUS/room1/light1_container"
                }
            }]
        }
```

So although there is just one type of link but in our code we divided into two.

One just link different static types and the other one connects the container 
to the static one.

The example for linking different static assets:
```json
{
            "typeid": "__Link",
            "values": [{
                "source": {
                    "typeid": "FirstStaticType",
                    "index": "F_HAUS/room1"
                },
                "target": {
                    "typeid": "FirstStaticType",
                    "index": "F_HAUS/room1/light1"
                }
            }]
        }
```

In the last example make sure that you have built the assets before you linking
 them. So you need to create asset data with index "F_HAUS/room1"
to be able to link two data together. In this example light1 will be the child
for room1.

After doing all these steps then you can send data value to the container 
to store the data in PI system.
```json
"containerid": "F_HAUS/room1/light1_container",
                "values": [
                    {
                        "timestamp": timestamp,  
                        "NumberProperty1": value,
                        "Property2": timestamp,
                        "StringEnum": status,
                        "Description": description
                    }
                ]
            }
```

Note that you can not simply add things here, you need to make sure these schema
follows exactly the dynamic schema you described in your first message you sent
which was your type message.

the Message class has a function _call_method(method number)_ that based on the
method number it will creates the body.
The numbers are as the way that I described:

*   0: creates container
*   1: creates assets
*   2: create link between assets
*   3: creates link between asset and container
*   4: creates values in the container 

At any stage if you want to change things you need to clean cache file on the 
relay connector. 

For example if you want to add "property3", first you need to add that in 
your type message body. add the information to the container data value and 
finally you need to first stop the PI connector relay from the service. 
 using  win  + R, run the "run" application and type :
 
 %ProgramData%\OSIsoft\Tau\
 
 and delete the "Relay Connector Host" folder.
 
 This stores cache files, the moment you restart the relay, it will create a 
 new one. 

Follow different test cases in omf.test module to test and understand the 
OMF message.

---
##  Bulking result:
### using local network socket for relay:

message type used for testing:

```json
[
            {
                "containerid": containerid,
                "values": [
                    {
                        "timestamp": timestamp,  # needs to be in datetime#
                        "NumberProperty1": value,
                        "Property2": timestamp,
                        "StringEnum": status,
                        "Description": description
                    }
                ]
            }
        ]
```

*   message size: 104 byte using __sys.getsizeof()__ 

*   After sending type and container bodies, 18885 data body type
    messages can be sent to the data without receiving payload size error 
    for OMF messages.
    
*   The time interval between receiving and sending is 3.20 seconds, this can
    varies between 1 seconds depends on the network situations. Best case was 
    2.19 seconds.

*   use compression for big data, the differences between with and without
    using compression is significant.
    *   18000 point without gzip takes 10.51 seconds to send and it led to 
        payload size error.
    *   18000 points with gzip takes 3.56 seconds to send and there was no 
        payload error issues.
    *   18885 points with gzip without network takes 1.09 seconds to send.


---
## 24 hours test results:
*   __June 28, 2018__: The current version based on observable concept worked
    for 24 hours without any issue receiving 10 publisher, 10 topic @ 5hz, using
    publisher_sim module.

*   __Current Summery__: Better avoid multi-treaded approach at this stage. Implement
    event-driven approach and see if it is stable or not.

    I think by the end of these tests this laptop is going to explode.

*   __June 21, 2018__: Publisher and subscriber on same machine, not using network
    connections. Somehow for some reason PyCharm (IDE) stopped sending data to
    relay at 8:30 PM, 2 hours after starting, but checking the computer in the
    morning at 8:30 AM, resumed the pause and it is sending the past 12 hours 
    points stored in the queue. NO error received.

    The computer always goes to sleep mode even though the sleep mode is off on
    the windows setting. Maybe the sleep mode caused this issue. This can't be
    the issue because I left the computer un touched for 3 hours and it still
    sending data to PI system.

    Used reactive programming concept
    The previews analysis from this approach shows that this does not use any
    multithreaded concept, even though it is attempted.

    Used 10 publisher as threads with 10 topics, sending values with
     __random.randint(1, 10) X 0.001__ sleep between each sending to avoid
     publishing same value exactly at the same time with other threads.

*   __June 20, 2018__: Relay stopped receiving the data, although in services it was working
    Time our error. Used 5 multi-threaded approach.

    Used 200 publishers as thread with one topic sending values.

*   __June 19, 2018__: Forgot to store log info on the publisher, received 10054
    error.

    Used 200 publishers as thread with one topic sending values.

---
## Current issues:
__These issues caused by multi-tread approach__. There is no need to have this 
approaches.
*   The MQTT broker stops immediately, when there are more than 500 publishers connecting.
    It needs to start again under windows **services**
    *   To avoid this issue, we need to test the situation by provide extra broker.
*   The cheap multithread approach didn't work during long time simulation.
    *   first day error:  ["10054"](https://support.microsoft.com/en-us/help/981344/an-application-may-receive-the-10054-error-when-the-application-receiv) error
    *   second day error: time out error.
        *   Unclear why this happened the connector relay in services was not
            stopped, but the relay was not able to receive any data.
            *   possible to0 much data flow at the same time? need more testing.
    *   Is there I/O multiplexing on Relay part? [link1](https://notes.shichao.io/unp/ch6/), [link2](https://docs.python.org/3/library/selectors.html#module-selectors)


##  Questions:

*   Why I cant connect from OSI laptop to my personal laptop using my ip? both are connected to VPN.
*   How to change the type of the pi points, removing extra attributes, after making
elements, with previews type.

*   How to delete pi points from AF
*   [link to the clean up](https://github.com/osisoft/OMF-Developer-Companion-Guide/blob/master/docs/OMF_Quick_Start.rst)
*   how to use other PI tools to make changes or make the changes manually?

---

#   June 22, 2018:
*   The reactive implementation for receiver now works without any delay.
    There is no i/o bottleneck.
*   More test with bigger files are required to test at what point relay
    might breaks.
    *   This will help us to know how much data we can analyze in one second
        before sending to the PI system.
*   The current implementation has no problem receiving 100 points per seconds.
---

#   June 21, 2018:
*   Implemented event driven approach using asyncio in python.
    *   current result does not show any performance improvement
     but this design has potential to increase performance.
     *  There is no drops and all points are received correctly.

---

# June 20, 2018:

TODO:
*   Test new design with big data and find the issues.
*   Try sending and analyze by performing different sizes and see the result
*   Make Reactive programming multithreaded and test the performance
*   Use python profiler to get what method takes the highest processing time.
*   Find best practice for bulking the data

---

# June 19, 2018:

Rewrote the receiver to have inherent queue class to track the size of the queue
 for each topic.

Each topic will have its own thread to send the data.

We have control over the messages that comes in to
make bigger buffer size to send to the PI system.



---
#   June 15, 2018:
##  TODO:
*   ~~Try cutting the network between connection and see which cause the delay.~~


*   Run the analysis on C# and compare it to the python.
    *   ~~Implement subscriber $\to$ Done but without output text file.~~ Mohamed shared his result.
    *   ~~Implement publisher~~ no need the publisher is fast.

---
# June 14, 2018
## Introduced Reactive Programming:

Why this concept is good to be implemented in the data stream: (From Learning Concurrency in Python by Ellio Forbes, Packt Publishing)

*   Representing our data as a stream of events.
    *   subscribe to subsequent streams and take action upon receiving the events.
    *   This will simplify system flow that could quickly become unmaintainable.
    *   we can fashion program that will run infinitely against an infinite stream
    of events.
*   Uses Push (keeps it constantly active) mechanism in order to notify as opposed to a Pull mechanism.

The data flow work as the events now.
>   This paradigm can be used when we have streams of statistical data or sensory data coming in, which they have to be analyzed and make decisions.

---
#   June 13, 2018:
*   Implemented multithread approach for sending the post messages. The current analysis shows improvement compare to single threaded approach. The test was sending to broker on another machine, receiving back and sending to relay on another machine, the result is promising, but it shows some point drops.
*   Moved repository to bitbucket

---
#   June 12, 2018:
*   Analyzed the result from yesterday and ran more tests.
*   Found the cause of latency, the on-message works sync, the data need to be sent to PI system with another approach.
    *   Implemented  a queue of received data with on_message call back and parse and send the value outside the on_message call back.
        *   The on_messaged received very fast but the process for sending to PI System was around 0.9 sec for each message.
##  TODO:
*   ~~~Fix the venv on the gitlab, if needed make new repository~~~

---
#   June 11, 2018:
## TODO:
*   ~~~Run publisher and subscriber on my laptop connecting to broker on OSI Soft. Calculate time between sending and receiving data.~~~


---
---
#   June 8, 2018:
## TODO:

*   ~~Talk about the git repository, where to locate.~~
*   ~~Email mohamad~~
*   ~~make repository~~



---
#   June 7, 2018:
##  TODO:
*   ~~Try paho mqtt on C# and compare the duration of gathering data to python.~~
##  Done:
*   Made paho mqtt subscriber in C#. Had problem dealing with writing data to text file caused by threading issue in C#.
    *   Suggested method use lock to synchrozing access to the streamwriter.  [Link](https://stackoverflow.com/questions/29099980/stringtemplate-probable-i-o-race-condition-detected-while-copying-memory).

##  Questions:
*   ~~Tried PI WEB API but how to post authentication, what is username and password?~~ solved
    *   Follow [this link](https://livelibrary.osisoft.com/LiveLibrary/content/en/web-api-v8/GUID-DA5C6E2D-46FE-4961-AF95-642E7B309D9F#addHistory=true&filename=GUID-795C947E-C220-4521-8827-F7840CFE0DEA.xml&docid=GUID-5EB0EE2A-B2D0-44D5-935A-422E9FA574A8&inner_id=&tid=&query=&scope=&resource=&toc=false&eventType=lcContent.loadDocGUID-5EB0EE2A-B2D0-44D5-935A-422E9FA574A8) to locate the configuration for PI Web API and change the authentication to Basic. Make sure you check-in the new variables and stop and start the PI Web API from services.

    *   Using postman you can verify if you are able to receives the json fromat or not. https://serverName/piwebapi/assetservers.

[Link to OSI SOFT Live Library](https://livelibrary.osisoft.com/LiveLibrary/content/en/web-api-v8/GUID-DA5C6E2D-46FE-4961-AF95-642E7B309D9F#addHistory=true&filename=GUID-795C947E-C220-4521-8827-F7840CFE0DEA.xml&docid=GUID-DA5C6E2D-46FE-4961-AF95-642E7B309D9F&inner_id=&tid=&query=&scope=&resource=&toc=false&eventType=lcContent.loadDocGUID-DA5C6E2D-46FE-4961-AF95-642E7B309D9F)

---
#   June 6, 2018:
##  Done:
*   Different analytics have been done to find which part of communication, either MQTT or post request causes delay.

*   The result shows, MQTT takes about 4 minute to deal 7000 points with qos=0 and  persistent session to False.

---
#   June 5, 2018:
##  Done:
*   Installed PI AF service and PI WEB API, and started working on receiving data from AF archive

*  Found that we have performance issue with multi IoT subscribers.
    28:45 minute to transfer all the files to pi system.
---
# June 4, 2018:
*   Time input problem solved.
    *   By making pi archive in pi management tool, we can add data in the past.\
    The default value is about 13 days.
## notes:
*   Make sure qos=1 for publishisng MQTT, and clean_session=False, for both publisher and subscribers.
